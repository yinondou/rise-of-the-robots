#include <iostream>
#include <string>
 
// roscpp
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
 
// libstage
#include <libplayerc++/playerc++.h>
 
#include <assert.h>

class PlayerRosNode {
  public:
    PlayerRosNode(std::string host);
 
  private:
    //  Player objects
    PlayerCc::PlayerClient *client;
    PlayerCc::Position2dProxy *posProx;
    PlayerCc::LaserProxy *lasProx;
	PlayerCc::IrProxy *irProx;
    pthread_mutex_t mutex;
 
    // ROS topics
    ros::NodeHandle n;
    ros::Publisher laser_pub;
    ros::Publisher odom_pub;
    ros::Subscriber vel_sub;
    tf::TransformBroadcaster tfb;
 
    void odomReceived(void);
    void laserReceived(void);
    void irReceived(void);
    void cmdvelReceived(const boost::shared_ptr<geometry_msgs::Twist const>& msg);
};

int main(int argc, char **argv)
{
  // Init ROS
  ros::init(argc, argv, "playerros");
 
  // Create the PlayerROS node
  PlayerRosNode p("localhost");
 
  // Start the ROS message loop
  ros::spin();
 
  return 0;
}

/// Connects to Player on the specified host and create the bridge between
/// Player and ROS.
PlayerRosNode::PlayerRosNode(std::string host)
{
  // Connect to player and get the proxies for position and laser
  try {
    client = new PlayerCc::PlayerClient(host);
    posProx = new PlayerCc::Position2dProxy(client);
    posProx->SetMotorEnable(true);
	//ROS_INFO("Initializing Player laser proxy");
    //lasProx = new PlayerCc::LaserProxy(client);
	//ROS_INFO("Initialized Player laser proxy");
	//irProx = new PlayerCc::IrProxy(client);
  }
  catch( PlayerCc::PlayerError e) {
    std::cerr << e << std::endl;
    abort();
  }
 
  // A mutex to protect concurrent access to posProx, because it is accessed in
  // 2 different threads (odomReceived and cmdvelReceived)
  pthread_mutex_init(&mutex,NULL);
 
  // Connect the handlers to treat player updates
  posProx->ConnectReadSignal(boost::bind(&PlayerRosNode::odomReceived,this));
  //lasProx->ConnectReadSignal(boost::bind(&PlayerRosNode::laserReceived,this)); 
  //irProx->ConnectReadSignal(boost::bind(&PlayerRosNode::irReceived, this));
 
  // Advertize the ROS topics for odometry and laser data
  odom_pub = n.advertise<nav_msgs::Odometry>("/odom", 10);
  //laser_pub = n.advertise<sensor_msgs::LaserScan>("/base_scan", 10);
 
  // Subscribe to the topic to receive velocity commands
  vel_sub = n.subscribe<geometry_msgs::Twist>("/cmd_vel", 10,
                        boost::bind(&PlayerRosNode::cmdvelReceived,this,_1));
  
  ROS_INFO("Starting thread");
  // Start the player client
  client->StartThread();
  ROS_INFO("Started thread");
}

/// A callback function called by player when an odometry packet is received.
/// Read the packet and create the corresponding ROS message, then publish it.
void PlayerRosNode::odomReceived() {
  // safety net
  assert(posProx);
 
  // lock access to posProx
  pthread_mutex_lock( &mutex );
 
  // Retrieve odo info from Player
  float x = posProx->GetXPos();
  float y = posProx->GetYPos();
  float th = posProx->GetYaw(); //theta
  float vx = posProx->GetXSpeed();
  float vth = posProx->GetYawSpeed();

  // log
  //ROS_INFO("Got odometry %f %f %f %f %f", x, y, th, vx, vth);
 
  // release access to posProx
  pthread_mutex_unlock( &mutex );
 
  // current time
  ros::Time current_time = ros::Time::now();
 
  //first, we'll publish the transform over tf
  //position and velocity of frame base_link w.r.t. frame odom (fixed frame)
  geometry_msgs::TransformStamped odom_trans;
  odom_trans.header.stamp = current_time;
  odom_trans.header.frame_id = "odom";
  odom_trans.child_frame_id = "base_link";
  odom_trans.transform.translation.x = x;
  odom_trans.transform.translation.y = y;
  odom_trans.transform.translation.z = 0.0;
  odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(th);
  tfb.sendTransform(odom_trans);
 
  //next, we'll publish the odometry message over ROS
  nav_msgs::Odometry odom;
  odom.header.stamp = current_time;
  odom.header.frame_id = "odom";
  odom.child_frame_id = "base_link";
  odom.pose.pose.position.x = x;
  odom.pose.pose.position.y = y;
  odom.pose.pose.position.z = 0.0;
  odom.pose.pose.orientation = tf::createQuaternionMsgFromYaw(th);
  odom.twist.twist.linear.x = vx;
  odom.twist.twist.linear.y = 0;
  odom.twist.twist.angular.z = vth;
  odom_pub.publish(odom);
}

/// A callback function called by player when a laser packet is received.
/// Read the packet and create the corresponding ROS message, then publish it.
void PlayerRosNode::laserReceived() {
  //safety net
  assert(lasProx);
  
  // log
  //ROS_INFO("Got laser");
 
  // retrieve laser data
  unsigned nPoints = lasProx->GetCount();
  float resolution = lasProx->GetScanRes();
  float fov = (nPoints-1)*resolution;
 
  // current time
  ros::Time now = ros::Time::now();
 
  // first publish the transform over tf. In our case, the position of the laser
  // is fixed.
  tfb.sendTransform( tf::StampedTransform(
      tf::Transform( tf::createIdentityQuaternion(), //orientation of the laser on the robot
                     tf::Vector3(0.0, 0.0, 0.0) ),   //Position of the laser on the robot
      now, "base_link", "base_laser"));
 
  // then send the laser message
  sensor_msgs::LaserScan laserMsg;
  laserMsg.header.frame_id = "/base_laser";
  laserMsg.header.stamp = now;
  laserMsg.angle_min = -fov/2.0;
  laserMsg.angle_max = +fov/2.0;
  laserMsg.angle_increment = resolution;
  laserMsg.range_min = 0.0;
  laserMsg.range_max = 20; //arbitrary value (can we get the real value from Player?)
  laserMsg.ranges.resize(nPoints);
  laserMsg.intensities.resize(nPoints);
  for(unsigned int i=0;i<nPoints;i++) {
    laserMsg.ranges[i] = lasProx->GetRange(i);
    laserMsg.intensities[i] = lasProx->GetIntensity(i);
  }
  laser_pub.publish(laserMsg);
}

/// A callback function called by player when an IR packet is received.
/// Read the packet and create the corresponding ROS message, then publish it.
void PlayerRosNode::irReceived() {
	uint32_t ir_count = irProx->GetCount();
	uint32_t range0 = irProx->GetRange(0);
	uint32_t range1 = irProx->GetRange(1);
	uint32_t range2 = irProx->GetRange(2);
	uint32_t range3 = irProx->GetRange(3);
	uint32_t range4 = irProx->GetRange(4);
	uint32_t range5 = irProx->GetRange(5);
	uint32_t range6 = irProx->GetRange(6);
	uint32_t range7 = irProx->GetRange(7);
	ROS_INFO("Got IR. Count: %d, %d %d %d %d %d %d %d %d", ir_count, range0, range1, range2,
			range3, range4, range5, range6, range7);
}

/// A callback function called by ROS when a velocity command message is received.
/// Read the message and send the command to player.
void PlayerRosNode::cmdvelReceived(const boost::shared_ptr<geometry_msgs::Twist const>& msg) {
  // safety net
  assert(posProx);

  // log
  //ROS_INFO("Got cmd_vel");
 
  // lock access to posProx
  pthread_mutex_lock( &mutex );
 
  // Pass the velocity command to Player
  posProx->SetSpeed(msg->linear.x, msg->angular.z);
 
  // release access to posProx
  pthread_mutex_unlock( &mutex );
}

