# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yinon/dev/ros_player/ros_playground/playerros1node/src/playerros_node.cpp" "/home/yinon/dev/ros_player/ros_playground/playerros1node/build/CMakeFiles/playerros_node.dir/src/playerros_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_CB_DISABLE_DEBUG"
  "ROS_PACKAGE_NAME=\"playerros1node\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/opt/ros/kinetic/include"
  "/usr/local/include/player-3.0"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
