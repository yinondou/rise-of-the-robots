/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef RV400_COMMANDS_H
#define RV400_COMMANDS_H

#include <inttypes.h>
#include <cstdio>

#define START_BYTE 170  
#define R_KIT_CODE 11

#define MIN_NUM_OF_BUZZES 1
#define MAX_NUM_OF_BUZZES 29
#define MIN_BUZZ_FREQUENCY 1
#define MAX_BUZZ_FREQUENCY 255

#define MIN_TURN_RIGHT 15
#define MAX_TURN_RIGHT 60
#define MIN_TURN_LEFT 75
#define MAX_TURN_LEFT 120
#define MIN_TURN_DEGREE 0
#define MAX_TURN_DEGREE 180
#define MIN_PWM     0
#define MAX_PWM     100
#define MIN_CURVE_VELOCITY  0
#define MAX_CURVE_VELOCITY  120
#define MIN_HEADING     0
#define MAX_HEADING     259
#define FORWARD_KEEP_HEADING 360
#define MIN_ULTRASOUND_THRESHOLD 1
#define MAX_ULTRASOUND_THRESHOLD 1000
#define MIN_FORWARD_VELOCITY    0
#define MAX_FORWARD_VELOCITY    19
#define MIN_VAC_MOTOR_COMMAND_PWM   0
#define MAX_VAC_MOTOR_COMMAND_PWM   98
#define MIN_BRUSH_MOTOR_COMMAND_PWM   0
#define MAX_BRUSH_MOTOR_COMMAND_PWM   98
#define MIN_BRUSH_ELEVATION_HEIGHT  0
#define MAX_BRUSH_ELEVATION_HEIGHT  175

uint8_t checksum(uint8_t* packet, uint8_t length);

/* interface for all rv400 commands */
class rv400_command
{ 
public:
    virtual ~rv400_command() {};

    /* cast command to a uint8 array for sending in the network */ 
    virtual operator uint8_t* ()=0;    

    /* command size when casted to uint8 array */
    virtual int size()=0;
  
    /* debug information */
    void debug() 
    { 
        for (int i=0;i<size();i++) 
            printf("%x ",((uint8_t*)(*this))[i]); printf("\n"); 
            fflush(stdout); 
    }
};

/* class representing a drive motor command */
class rv400_drive_motors_command : public rv400_command
{
private:
    /* most drive commands fits this structure */
    struct  {
        uint8_t start_byte;
        uint8_t packet_length;  
        uint8_t r_kit_code;
        uint8_t command;
        uint8_t data;
        uint8_t movement_type;
        uint8_t data1;
        uint8_t data2;
        uint8_t checksum;
    } __attribute__ ((__packed__)) command;  

    /* structure for "turn to" command */
    struct {
        uint8_t start_byte;
        uint8_t packet_length; 
        uint8_t r_kit_code;
        uint8_t command;
        uint8_t data;
        uint8_t movement_type;
        uint8_t velocity;
        uint16_t heading;
        uint8_t checksum;
    } __attribute__ ((__packed__)) command_turn_to;

    /* structure for "forward" command */
    struct {
        uint8_t start_byte;
        uint8_t packet_length; 
        uint8_t r_kit_code;
        uint8_t command;
        uint8_t data;
        uint8_t movement_type;
        uint8_t velocity;
        uint16_t heading;
        uint16_t distance;
        uint16_t ultrasound_threshold;
        uint8_t checksum;
    } __attribute__ ((__packed__)) command_forward;
    
    rv400_drive_motors_command(); 
    
    /* command movement type */
    enum {STOP=1,FORWARD=2,BACKWARD=3,TURN_TO=4,TURN=5,CONTOUR=6,
          DOCKING_HOMING=7, MOTORS_PWM=8,MOTORS_DIRECTION=9,CURVE=10} move_type;

public:
    /* motor direction */
    enum motor_direction_t { MOTOR_FORWARD = 0 , MOTOR_BACKWARD = 1 };

    /* commands factory methods */

    static rv400_drive_motors_command* stop();

    static rv400_drive_motors_command* backward();

    static rv400_drive_motors_command* turn(uint8_t velocity,uint8_t degrees);

    static rv400_drive_motors_command* countor();

    static rv400_drive_motors_command* docking_homing();

    static rv400_drive_motors_command* motors_PWM(uint8_t left, 
                                                  uint8_t right);

    static rv400_drive_motors_command* motor_direction(motor_direction_t left,
                                                       motor_direction_t right);

    static rv400_drive_motors_command* curve(uint8_t velocity, uint8_t radius);

    static rv400_drive_motors_command* turn_to(uint16_t heading);

    static rv400_drive_motors_command* forward(uint8_t velocity, 
                                               uint16_t heading, 
                                               uint16_t distance, 
                                               uint32_t ultrasound_threshold);

    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_drive_motors_command() {}

};

/* class representing end programming mode command */
class rv400_end_programming_mode : public rv400_command
{
private:
    struct{
        uint8_t start_byte;
        uint8_t packet_length;
        uint8_t r_kit_code;
        uint8_t command;
        uint8_t checksum;
    }__attribute__ ((__packed__)) command;

    rv400_end_programming_mode();

public:
    /* create an end programming mode command */
    static rv400_end_programming_mode* end_program();

    virtual operator uint8_t*();
    virtual int size();
    virtual ~rv400_end_programming_mode() {}
};

/* class representing buzzer command */
class rv400_buzzer_command : public rv400_command
{
private:
    struct {
        uint8_t start_byte;
        uint8_t packet_length;
        uint8_t r_kit_code;
        uint8_t command;
        uint8_t num_of_buzzes;
        uint8_t frequency;
        uint8_t checksum;
    } __attribute__ ((__packed__)) command;

    rv400_buzzer_command(uint8_t num_of_buzzes,uint8_t frequency);
public:
    /* create a buzzer command */
    static rv400_buzzer_command* buzzer(uint8_t num_of_buzzes,
                                        uint8_t frequency);

    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_buzzer_command() {}
};

/* class representing dummy command */
class rv400_dummy_command : public rv400_command
{
private:
    struct {
        uint8_t start_byte;
        uint8_t packet_length;
        uint8_t r_kit_code;
        uint8_t command;
        uint8_t checksum;
    } __attribute__ ((__packed__)) command;
    
    rv400_dummy_command();
public:
    /* create a dummy command */
    static rv400_dummy_command* dummy();

    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_dummy_command() {}
};

/* class representing timeout threshold command */
class rv400_timeout_threshold_command : public rv400_command
{
private:
    struct {
        uint8_t start_byte;
        uint8_t packet_length;
        uint8_t r_kit_code;
        uint8_t command;
        uint8_t data;
        uint8_t checksum;
    } __attribute__ ((__packed__)) command;
    
    rv400_timeout_threshold_command();
public:
    /* create a timeout threshold command */
    static rv400_timeout_threshold_command* timeout_threshold(uint8_t threshold);

    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_timeout_threshold_command() {}
};

/////////////////////////////////////////////////////////////////////////////

class rv400_vac_motor_command : public rv400_command
{
private:
    struct{
        uint8_t start_byte;
        uint8_t packet_length;
        uint8_t r_kit_code;
        uint8_t command;
        
        uint8_t dummy_data;
        uint8_t data;

        uint8_t checksum;

    } __attribute__ ((__packed__)) command;

    rv400_vac_motor_command();

public:

    static rv400_vac_motor_command* vac_motor(uint8_t pwm);
    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_vac_motor_command() {}

};

/////////////////////////////////////////////////////////////////////////////

class rv400_brush_motor_command : public rv400_command
{
private:
    struct{
        uint8_t start_byte;
        uint8_t packet_length;
        uint8_t r_kit_code;
        uint8_t command;
        
        uint8_t dummy_data;
        uint8_t pwm;
        uint8_t direction;

        uint8_t checksum;

    } __attribute__ ((__packed__)) command;

    rv400_brush_motor_command();

public:
    enum motor_direction_t { BRUSH_FORWARD = 0 , BRUSH_BACKWARD = 1 };

    static rv400_brush_motor_command* brush_motor(uint8_t pwm, motor_direction_t direction);
    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_brush_motor_command() {}

};

//////////////////////////////////////////////////////////////////////////////

class rv400_brush_elevation_motor_command : public rv400_command
{
private:
    struct{
        uint8_t start_byte;
        uint8_t packet_length;
        uint8_t r_kit_code;
        uint8_t command;
        
        uint8_t dummy_data;
        uint8_t height;

        uint8_t checksum;

    } __attribute__ ((__packed__)) command;

    rv400_brush_elevation_motor_command();

public:
    static rv400_brush_elevation_motor_command* brush_height(uint8_t height);
    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_brush_elevation_motor_command() {}

};

///////////////////////////////////////////////////////////////////////////////

/* class representing telemetry command */
class rv400_telemetry_command : public rv400_command
{
private:
    struct {
        uint8_t start_byte;
        uint8_t length;
        uint8_t r_kit_code;
        uint8_t command;
        
        uint8_t telemetry_messages_rate;

        uint8_t telemetry_format_A;
        uint8_t telemetry_format_B;
        uint8_t telemetry_format_C;    
        uint8_t telemetry_format_D;

        uint8_t checksum;
    } __attribute__ ((__packed__)) command;

    /* create a byte according to each bit state */
    static uint8_t create_byte(bool lsb, 
                               bool b,
                               bool c, 
                               bool d, 
                               bool e, 
                               bool f, 
                               bool g, 
                               bool msb);
public:
    rv400_telemetry_command();

    /* create telemetry format A byte */
    static uint8_t create_telemetry_format_A(
                                bool ask_for_ultrasound_proximity_sensors, 
                                bool ask_for_odometer,                     
                                bool ask_for_left_bumper,                 
                                bool ask_for_right_bumper,                
                                bool ask_for_left_nozzle_bumper,            
                                bool ask_for_right_nozzle_bumper,          
                                bool ask_for_nozlle_height,                
                                bool ask_for_charging_voltage);        

    /* create telemetry format B byte */
    static uint8_t create_telemetry_format_B(
                                bool ask_for_vcc_voltage,              
                                bool ask_for_ambient_temperature,
                                bool ask_for_brush_elevation_motor_current,
                                bool ask_for_right_drive_motor_current,
                                bool ask_for_left_drive_motor_current,
                                bool ask_for_selector_position,
                                bool ask_for_front_stairs_PSD,
                                bool ask_for_humidity);                             
    
    /* create telemetry format C byte */
    static uint8_t create_telemetry_format_C(
                                bool ask_for_battery_voltage,            
                                bool ask_for_stairs,                    
                                bool ask_for_buttons_bitmap,
                                bool ask_for_leash,
                                bool ask_for_door,
                                bool ask_for_IR_receivers,
                                bool ask_for_charging_plug,
                                bool ask_for_charging_temperature);

    /* create telemetry format D byte */
    static uint8_t create_telemetry_format_D(
                                bool ask_for_left_battery_thermistor,    
                                bool ask_for_right_battery_thermistor);
                               
    /* create a telemetry command */                           
    static rv400_telemetry_command* telemetry_setting(
                                uint8_t messages_rate, 
                                uint8_t telemetry_A, 
                                uint8_t telemetry_B, 
                                uint8_t telemetry_C, 
                                uint8_t telemetry_D);

    virtual operator uint8_t* ();
    virtual int size();
    virtual ~rv400_telemetry_command() {}
};


#endif // RV400_COMMANDS_H
