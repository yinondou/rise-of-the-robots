#ifndef RV400ROS_H
#define RV400ROS_H

#include "rv400Commands.h"
#include "rv400Response.h"
#include "Timer.h"
#include <pthread.h>
#include <memory>
#include <linux/serial.h>

#define DEFAULT_PORT "/dev/ttyS0"

/* number of connection retries */
#define CONNECT_RETRY_TIMES 5

class rv400Ros
{
public:
	rv400Ros();
	virtual ~rv400Ros();
	int setup();
	void test_rv400();
	int main_loop(int argc, char **argv);
private:
    /* serial port descriptor */
    int m_rv400_fd;

	/* read mutex */
    pthread_mutex_t m_read_mutex;

    /* supported interfaces flag */
    bool m_position;
    bool m_sonar;
    bool m_bumper;
    bool m_rv400;

	/* Timers */
    Timer m_timer;
    Timer timer;

    /* rv400 responses */
	// TODO - change auto_ptr to unique_ptr?
    std::auto_ptr<rv400_data_response> m_data_response;
    std::auto_ptr<rv400_ack_response> m_ack_response;

	void ReadData();
	bool connect();
	int read_buff(int fd,uint8_t* buff,uint8_t len);
	void send_command(rv400_command* command);
	void send_command(const std::auto_ptr<rv400_command>& command);
};

#endif  // RV400ROS_H
