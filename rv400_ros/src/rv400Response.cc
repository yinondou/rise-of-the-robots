/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "rv400Response.h"
#include <cmath>
#include <iostream>
#include <netinet/in.h>

#define TELEMETRY_DATA_A 1
#define TELEMETRY_DATA_B 2
#define TELEMETRY_DATA_C 3
#define TELEMETRY_DATA_D 4

using namespace std;

rv400_response::rv400_response(types type)
{
    m_type=type;
}

rv400_response::types rv400_response::type() const { return m_type; }

rv400_response* rv400_response::parseResponse(uint8_t* response,uint8_t len)
{    
    if (len<5)
        throw "rv400_response: length error";
    
    if (response[0]!=START_BYTE || response[2]!=R_KIT_CODE)
        throw "rv400_respose: invalid response";

    if (response[1]!=len)
        throw "rv400_respone: length error";

    if (checksum(response,len-1)!=response[len-1])
        throw "rv400_response: checksum mismatch";
    
    switch (response[3]) {
        case STOP:
            return new rv400_response(STOP);
        case ACK:
            return new rv400_ack_response(response[4],response[5]);
        case DATA:{         
           return new rv400_data_response(response+4,len-4);
        }

        default: throw "rv400_response: unknown command";
    };

}

/* IMPORTANT: reponse is expected to start at the transmition rate field */
rv400_data_response::rv400_data_response(uint8_t *response, uint8_t len) 
                                        : rv400_response(DATA)
{
    uint8_t* current = response+5;
    
    /* TELEMETERY DATA A */

    if (fromByteToBool(response[TELEMETRY_DATA_A], 0)) {
        for (int i=0; i<NUMBER_OF_SONARS; i++){
            sonars[i] = ntohs(*((uint16_t*)current));            
            current+=2;
        }
    }

    if (fromByteToBool(response[TELEMETRY_DATA_A], 1)){
        odometery_heading = ntohs(*(uint16_t*)(current));     
        current+=2;
        odometery_acc_distance = ntohl(*(uint32_t*)(current));      
        current+=4;
        odometery_left_tics = ntohl(*(uint32_t*)(current));   
        current+=4;
        odometery_right_tics = ntohl(*(uint32_t*)(current));    
        current+=4;
        odometery_left_backward_tics = ntohl(*(uint32_t*)(current));
        current+=4;
        odometery_right_backward_tics = ntohl(*(uint32_t*)(current));
        current+=4;
        odometery_left_direction = (uint8_t)(*current);
        current+=1;
        odometery_right_direction = (uint8_t)(*current);
        current+=1;
    }

    
    if (fromByteToBool(response[TELEMETRY_DATA_A],2)){
        bumper_left = current[0];       
        current++;
    }
    
    if (fromByteToBool(response[TELEMETRY_DATA_A],3)){
        bumper_right = current[0];
        current++;
    }
    if (fromByteToBool(response[TELEMETRY_DATA_A],4)){
        bumper_left_nozzle = current[0];
        current++;
    }
    if (fromByteToBool(response[TELEMETRY_DATA_A],5)){
        bumper_right_nozzle = current[0];
        current++;
    }
    if (fromByteToBool(response[TELEMETRY_DATA_A],6)){
        nozzle_height = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_A],7)){
        charging_voltage = current[0];
        current++;
    }

    /* TELEMETERY DATA B */

    if (fromByteToBool(response[TELEMETRY_DATA_B],0)){
        vcc_voltage = current[0];
        //cout << "vcc voltage: " << (uint32_t)vcc_voltage << endl;
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_B],1)){
        ambient_temperature = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_B],2)){
        brush_elevation_motor_current = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_B],3)){
        right_drive_motor_current = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_B],4)){
        left_drive_motor_current = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_B],5)){
        selector_position = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_B],6)){
        front_stairs_psd = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_B],7)){
        humidity = current[0];
        current++;
    }

    /* TELEMETERY DATA C */

    if (fromByteToBool(response[TELEMETRY_DATA_C],0)){
        battery_voltage = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_C],1)){
        stairs = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_C],2)){
        buttons_bitmap = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_C],3)){
        leash = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_C],4)){
        door = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_C],5)){
        battery_voltage = current[0];
        ir_receivers_front_right_sensor = current[0];
        ir_receivers_front_left_sensor = current[1];
        ir_receivers_right_sensor = current[2];
        ir_receivers_left_sensor = current[3];
        ir_receivers_rear_sensor = current[4];
        current+=5;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_C],6)){
        charging_plug = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_C],7)){
        charging_temperature = current[0];
        current++;
    }

    /* TELEMETERY DATA D */

    if (fromByteToBool(response[TELEMETRY_DATA_D],0)){
        left_battery_thermistor = current[0];
        current++;
    }

    if (fromByteToBool(response[TELEMETRY_DATA_D],1)){
        right_battery_thermistor = current[0];
        current++;
    }
                  
}

rv400_ack_response::rv400_ack_response(uint8_t ackType, uint8_t reason) : rv400_response(ACK)
{
    if (ackType != ACK_DRIVE_RECEPTION && ackType != ACK_DRIVE_COMPLETION
        && ackType!= ACK_GLOBAL)
        throw "rv400_ack_response: unknown acknowledge type";
   
    if (reason != UNRECOGNIZED_EVENT && reason != BUMPER_EVENT && reason != STAIR_EVENT 
        && reason != DISTANCE_REACHED && reason != ULTRASOUND_EVENT 
        && reason != HEADING_REACHED)
        throw "rv400_ack_response: unknown acknowledge reason";
    

    acknowledgeType = ackType;
    driveCommandTerminationReason = reason;
}

uint8_t rv400_ack_response::ack_type() const { return acknowledgeType; }

uint8_t rv400_ack_response::ack_reason() const { return driveCommandTerminationReason; }
