#include <iostream>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <memory.h>
#include <errno.h>
#include <string.h>
#include "rv400Ros.h"

using namespace std;

rv400Ros::rv400Ros()
{
	setup();
}

rv400Ros::~rv400Ros()
{
	close(m_rv400_fd);
}

int
set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                perror("error from tcgetattr");
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                perror("error from tcsetattr");
                return -1;
        }
        return 0;
}

int rv400Ros::setup()
{
	int baud=B38400; // rv400 serial speed  
	struct termios term;
	int flags;
	
	if((m_rv400_fd = open(DEFAULT_PORT, 
                     O_RDWR|O_NONBLOCK|O_NOCTTY|O_LARGEFILE)) < 0 )
    {
    	perror("RV400::Setup():open():");
    	return(1);
  	}

	if(tcgetattr(m_rv400_fd, &term ) < 0 )
  	{
    	perror("RV400::Setup():tcgetattr():");
    	close(m_rv400_fd);
    	m_rv400_fd = -1;
    	return(1);
  	}

    /* set serial speed */
  	cfmakeraw( &term );
  	cfsetispeed(&term, baud);
  	cfsetospeed(&term, baud);

    term.c_cflag = (term.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    term.c_iflag &= ~IGNBRK;         // disable break processing
    term.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    term.c_oflag = 0;                // no remapping, no delays
    term.c_cc[VMIN]  = 0;            // read doesn't block
    term.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    term.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    term.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    term.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    term.c_cflag |= 0;
    term.c_cflag &= ~CSTOPB;
    term.c_cflag &= ~CRTSCTS;

    if (tcsetattr (m_rv400_fd, TCSANOW, &term) != 0)
    {
            perror("error from tcsetattr");
            return -1;
    }

    /* flush serial queues */  
    if(tcflush(m_rv400_fd, TCIOFLUSH ) < 0)
  	{
    	perror("RV400::Setup():tcflush():");
    	close(m_rv400_fd);
    	this->m_rv400_fd = -1;
    	return(1);
    }

    if(tcsetattr(m_rv400_fd, TCSAFLUSH, &term ) < 0)
  	{
    	perror("RV400::Setup():tcsetattr():");
    	close(m_rv400_fd);
    	this->m_rv400_fd = -1;
    	return(1);
  	}

  	if((flags = fcntl(m_rv400_fd, F_GETFL)) < 0)
  	{
        perror("RV400::Setup():fcntl()");
        close(m_rv400_fd);
        m_rv400_fd = -1;
        return(1);
    }

    if (!connect())
        return -1;
	
	return 0;
}


bool rv400Ros::connect()
{
    bool connected=false;

	rv400_command *dummy = rv400_dummy_command::dummy();
	send_command(dummy);

    /* init rick's telemetry */

    uint8_t tele_dataA = rv400_telemetry_command::create_telemetry_format_A(
                                m_sonar,m_position,m_bumper,m_bumper,m_bumper,
                                m_bumper,m_rv400,m_rv400);

    uint8_t tele_dataB = m_rv400 ? 0xFF : 0;
    uint8_t tele_dataC = m_rv400 ? 0xCF : 0;
    uint8_t tele_dataD = m_rv400 ? 0x03 : 0;

                                
    auto_ptr<rv400_command> telemetry(rv400_telemetry_command::telemetry_setting
                                      (5,tele_dataA,tele_dataB,tele_dataC,
                                       tele_dataD));
                                      
    /* wake up rick */
    /*for (int i=0;i<CONNECT_RETRY_TIMES && !connected;i++) {
        cout<<"wake up (attempt " << i << ")" << endl;
        for (int j=0;j<50;j++) {
            send_command(telemetry);ssscfdsfds
            usleep(1000000); 
            ReadData();
            if (m_data_response.get()) {
                cout<<"connected"<<endl;                         
                connected=true;
                break;
            }
        }                
    }*/

	delete dummy;
    
    return connected;    
}

void rv400Ros::ReadData()
{
    uint8_t ch;
    int s;
    
    do {
        if (read(m_rv400_fd,&ch,1)==-1) {
            perror(NULL);
            return;
        }
		if (ch != 0)
			cout << "ch :" << (int)ch << endl;
    }
    while (ch != START_BYTE);
         
    if (read(m_rv400_fd,&ch,1)<1) {
        return;
    }
    
    unsigned length = ch;
    
    if (length==0) {
        return;
    }
   
    uint8_t* buffer=new uint8_t[length+2];
    
    if (buffer==NULL) 
        return;
 
    buffer[0] = START_BYTE;
    buffer[1] = length;

    if ((s=read_buff(m_rv400_fd, buffer+2, length-2)) != (int)length-2) {
        delete[] buffer;
        return;
    }

    auto_ptr<rv400_data_response> data_response;
    auto_ptr<rv400_ack_response> ack;
    auto_ptr<rv400_response> response;
    
    try {
        response.reset(rv400_response::parseResponse(buffer,length));
        
        switch (response->type()) {

            /* there is no need to use mutex on DATA response as ReadData and
             * UpdateData are called from the same thread
             */
            case rv400_response::DATA:
				cout << "DATA";                
                timer.startTimer();
                data_response.reset(
                    dynamic_cast<rv400_data_response*>(response.release()));
                m_data_response = data_response;      
                break;

            /* ack response should be synchronized as it is accessed from the
             * main thread as well as the read thread
             */
            case rv400_response::ACK:
				cout << "ACK";
                ack.reset(dynamic_cast<rv400_ack_response*>(response.release()));
                if ( ack->ack_type()==rv400_ack_response::ACK_DRIVE_RECEPTION) {
                    pthread_mutex_lock(&m_read_mutex);                                     
                    m_ack_response = ack;
                    pthread_mutex_unlock(&m_read_mutex);                    
                }
                break;
            default: break;  
        };
        
    }
    
    catch (const char* str) {}
   
    delete[] buffer;
}

int rv400Ros::read_buff(int fd,uint8_t* buff,uint8_t len)
{    
    time_t t = time(NULL);

    int r;
    int l = len; 
    
    while (len>0)
    {
        r = read(fd,buff,len);
        if (r==-1 || (r==0 && time(NULL)-t>1)) {
            perror(NULL);
            return l-len;
        }
        buff+=r;
        len-=r;
            
    }        
    
    return l-len;
}

void rv400Ros::send_command(rv400_command* command)
{
    write(m_rv400_fd,(uint8_t*)(*command),command->size());
}

void rv400Ros::send_command(const auto_ptr<rv400_command>& command)
{
    send_command(command.get());
}

void rv400Ros::test_rv400()
{
    //makin' a sound to show that the robot is ready (player workin') 
    rv400_command* buzz = rv400_buzzer_command::buzzer(2,1);
    send_command(buzz);
    usleep(1000000);
    delete buzz;
	//ReadData();
	
//	for (int i = 0; i < 10; i++) 
//	{
		rv400_command* move = rv400_drive_motors_command::motors_PWM(100, 100);
		send_command(move);
		usleep(2000000);
		delete move;
//	}
	
	/*rv400_command* curve = rv400_drive_motors_command::curve(100, 50);
	send_command(curve);
	usleep(2000000);
	delete curve;*/

	/*rv400_command* forward = rv400_drive_motors_command::forward(19, 180, 10000, 100);
	send_command(forward);
	usleep(2000000);
	delete forward;*/

	/*rv400_command* stop = rv400_drive_motors_command::stop();
	send_command(stop);
	usleep(1000000);
	delete stop;*/

	/*rv400_command* back = rv400_drive_motors_command::backward();
	send_command(back);
	usleep(1000000);
	delete back;*/
}

byte calc_checksum(byte* buffer, int buffer_len)
{
	byte i=0;
	long sum=0;
	for(i=0;i<buffer_len;i++)
		sum += buffer[i];
	return ((byte)(~sum));
}
int main()
{
	cout << calc_checksum("\xAA\x09\x11\x04\x02\x08\x64\x64");
	//rv400Ros* rv400RosInst = new rv400Ros();
	//rv400RosInst->test_rv400();

	//delete rv400RosInst;
}
