#include "ros/ros.h"
#include "rv400_ros/telemetry.h"

#include <sstream>

void telemetryCallback(const rv400_ros::telemetry::ConstPtr& msg)
{
  ROS_INFO("I heard: [%d]", msg->sonars[0]);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "RosRv400Telemetry");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("rv400_telemetry", 1000, telemetryCallback);
  ros::spin();
  /*ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
  ros::Rate loop_rate(10);
  int count = 0;

  while (ros::ok())
  {
    std_msgs::String msg;

    std::stringstream ss;
    ss << "hello world " << count;
    msg.data = ss.str();
    ROS_INFO("%s", msg.data.c_str());
    chatter_pub.publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }*/


  return 0;
}
