/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "rv400Commands.h"
#include <netinet/in.h>

/* rv400 checksum function */
uint8_t checksum(uint8_t* packet, uint8_t length)
{
    uint8_t i=0;
    long sum=0;
    for (i=0;i<length;i++)
        sum+=packet[i];
    return (uint8_t)(~sum);
}

rv400_drive_motors_command::rv400_drive_motors_command()
{
    command.start_byte = command_forward.start_byte = 
                         command_turn_to.start_byte = START_BYTE;

    command.r_kit_code = command_forward.r_kit_code = 
                         command_turn_to.r_kit_code = R_KIT_CODE;

    command.command = command_forward.command = command_turn_to.command = 4;
    command.data = command_forward.data = command_turn_to.data = 2;
}

rv400_drive_motors_command* rv400_drive_motors_command::stop()
{
    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->move_type=STOP;
    a->command.packet_length=9;
    a->command.movement_type = STOP;
    a->command.data1 = 0;
    a->command.data2 = 0;
    a->command.checksum=checksum((uint8_t*)&a->command,8);
    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::backward()
{
    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->move_type=BACKWARD;
    a->command.movement_type = BACKWARD;
    a->command.packet_length=9;
    a->command.data1 = 0;
    a->command.data2 = 0;
    a->command.checksum=checksum((uint8_t*)&a->command,8);
    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::turn(
                                uint8_t velocity,
                                uint8_t degrees)
{
    if (velocity<MIN_TURN_RIGHT && velocity>MAX_TURN_RIGHT 
        && velocity<MIN_TURN_LEFT && velocity>MAX_TURN_LEFT)
        throw "rv400_drive_motors_command: error in velocity";

    if (degrees>MAX_TURN_DEGREE)
        throw "rv400_drive_motors_command: error in degrees";

    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command.movement_type=TURN;
    a->command.packet_length=9;
    a->command.data1 = velocity;
    a->command.data2 = degrees;
    a->command.checksum=checksum((uint8_t*)&a->command,8);

    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::countor()
{
    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command.movement_type = CONTOUR;
    a->command.packet_length = 9;
    a->command.data1 = 0;
    a->command.data2 = 0;
    a->command.checksum = checksum((uint8_t*)(&a->command), 8);

    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::docking_homing()
{
    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command.movement_type = DOCKING_HOMING;
    a->command.packet_length = 9;
    a->command.data1 = 0;
    a->command.data2 = 0;
    a->command.checksum = checksum((uint8_t*)(&a->command), 8);
    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::motors_PWM(
                                uint8_t left, 
                                uint8_t right)
{
    if (left>MAX_PWM)
       throw "rv400_drive_motors_command: error in PWM value of left";

    if (right>MAX_PWM)
       throw "rv400_drive_motors_command: error in PWM value of right";

    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command.movement_type = MOTORS_PWM;
    a->command.packet_length = 9;
    a->command.data1 = left;
    a->command.data2 = right;
    a->command.checksum = checksum((uint8_t*)(&a->command), 8);    
    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::motor_direction(
                                motor_direction_t left, 
                                motor_direction_t right)
{
    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command.movement_type = MOTORS_DIRECTION;
    a->command.packet_length = 9;
    a->command.data1 = (uint8_t)left;
    a->command.data2 = (uint8_t)right;
    a->command.checksum = checksum((uint8_t*)(&a->command), 8);
    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::curve(
                                uint8_t velocity, 
                                uint8_t radius)    
{
    if (velocity>MAX_CURVE_VELOCITY)
       throw "rv400_drive_motors_command: error in value of velocity";

    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command.movement_type = CURVE;
    a->command.packet_length = 9;
    a->command.data1 = velocity;
    a->command.data2 = radius;
    a->command.checksum = checksum((uint8_t*)(&a->command), 8);
    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::turn_to(
                                uint16_t heading)
{
    if (heading > MAX_HEADING)
       throw "rv400_drive_motors_command: error in value of heading";
       
    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command_turn_to.movement_type = TURN_TO;
    a->command_turn_to.packet_length = 10;
    a->command_turn_to.velocity = 0;
    a->command_turn_to.heading = htons(heading);
    a->command_turn_to.checksum = checksum((uint8_t*)(&a->command_turn_to), 9);
    return a;
}

rv400_drive_motors_command* rv400_drive_motors_command::forward(
                                uint8_t velocity, 
                                uint16_t heading, 
                                uint16_t distance,
                                uint32_t ultrasound_threshold)
{
    if (velocity > MAX_FORWARD_VELOCITY)
       throw "rv400_drive_motors_command: error in value of ultrasound_threshold";
    
    if ((heading > MAX_HEADING && heading != FORWARD_KEEP_HEADING))
       throw "rv400_drive_motors_command: error in value of heading";
       
    if (ultrasound_threshold < MIN_ULTRASOUND_THRESHOLD || 
        ultrasound_threshold > MAX_ULTRASOUND_THRESHOLD)
       throw "rv400_drive_motors_command: error in value of ultrasound_threshold";
    
    rv400_drive_motors_command* a = new rv400_drive_motors_command();
    a->command_forward.movement_type = FORWARD;
    a->command_forward.packet_length = 14;
    a->command_forward.velocity = velocity;
    a->command_forward.heading = htons(heading);
    a->command_forward.distance = htons(distance);
    a->command_forward.ultrasound_threshold = htons(ultrasound_threshold);
    a->command_forward.checksum = checksum((uint8_t*)(&a->command_forward), 13);
    return a;
}


rv400_telemetry_command :: rv400_telemetry_command()
{
    command.start_byte = START_BYTE;
    command.r_kit_code = R_KIT_CODE;
    command.command    = 3;
    command.length     = 10;
}

uint8_t rv400_telemetry_command :: create_byte(bool lsb, bool b,bool c, bool d, 
                                               bool e, bool f, bool g, bool msb)
{
    uint8_t bb = 0;

    bb |= msb*0x80; //1000 0000
    bb |= g  *0x40; //0100 0000
    bb |= f  *0x20; //0010 0000
    bb |= e  *0x10; //0001 0000 

    bb |= d  *0x08; //0000 1000 
    bb |= c  *0x04; //0000 0100
    bb |= b  *0x02; //0000 0010
    bb |= lsb*0x01; //0000 0001

    return bb;
}

uint8_t rv400_telemetry_command :: create_telemetry_format_A
                              (bool ask_for_ultrasound_proximity_sensors, // LSB
                               bool ask_for_odometer,                  
                               bool ask_for_left_bumper,               
                               bool ask_for_right_bumper,               
                               bool ask_for_left_nozzle_bumper,         
                               bool ask_for_right_nozzle_bumper,          
                               bool ask_for_nozlle_height,                
                               bool ask_for_charging_voltage)             // MSB
{
    return create_byte(
                    ask_for_ultrasound_proximity_sensors, 
                    ask_for_odometer, 
                    ask_for_left_bumper, 
                    ask_for_right_bumper,                   
                    ask_for_left_nozzle_bumper, 
                    ask_for_right_nozzle_bumper,
                    ask_for_nozlle_height, 
                    ask_for_charging_voltage);        
}

uint8_t rv400_telemetry_command :: create_telemetry_format_B(
                               bool ask_for_vcc_voltage,                  // LSB
                               bool ask_for_ambient_temperature,         
                               bool ask_for_brush_elevation_motor_current,
                               bool ask_for_right_drive_motor_current, 
                               bool ask_for_left_drive_motor_current,   
                               bool ask_for_selector_position,        
                               bool ask_for_front_stairs_PSD,       
                               bool ask_for_humidity)                     // MSB
{
    return create_byte(
                    ask_for_vcc_voltage, 
                    ask_for_ambient_temperature,
                    ask_for_brush_elevation_motor_current, 
                    ask_for_right_drive_motor_current, 
                    ask_for_left_drive_motor_current,
                    ask_for_selector_position,
                    ask_for_front_stairs_PSD, 
                    ask_for_humidity);
}

uint8_t rv400_telemetry_command :: create_telemetry_format_C(
                               bool ask_for_battery_voltage,              // LSB
                               bool ask_for_stairs,                    
                               bool ask_for_buttons_bitmap,              
                               bool ask_for_leash,                       
                               bool ask_for_door,                       
                               bool ask_for_IR_receivers,                
                               bool ask_for_charging_plug,               
                               bool ask_for_charging_temperature)         // MSB
{
    return create_byte(
                    ask_for_battery_voltage, 
                    ask_for_stairs, 
                    ask_for_buttons_bitmap,
                    ask_for_leash,
                    ask_for_door, 
                    ask_for_IR_receivers, 
                    ask_for_charging_plug,
                    ask_for_charging_temperature);
}


uint8_t rv400_telemetry_command :: create_telemetry_format_D(
                               bool ask_for_left_battery_thermistor,   //LSB
                               bool ask_for_right_battery)             //bit #2
{
    return create_byte(
                    ask_for_left_battery_thermistor, 
                    ask_for_right_battery, 
                    0, 0, 0, 0, 0, 0);
}


rv400_telemetry_command* rv400_telemetry_command:: telemetry_setting(
                            uint8_t messages_rate, 
                            uint8_t telemetry_A,
                            uint8_t telemetry_B, 
                            uint8_t telemetry_C, 
                            uint8_t telemetry_D)
{
    rv400_telemetry_command* a = new rv400_telemetry_command();
    a->command.telemetry_messages_rate = messages_rate;
    a->command.telemetry_format_A = telemetry_A;
    a->command.telemetry_format_B = telemetry_B;
    a->command.telemetry_format_C = telemetry_C;
    a->command.telemetry_format_D = telemetry_D;
    a->command.checksum = checksum((uint8_t*)(&a->command), a->command.length-1);
    return a;
}

int rv400_telemetry_command :: size()
{
    return sizeof(command);
}

rv400_telemetry_command :: operator uint8_t* ()
{
    return (uint8_t*)(&command);
}
                               
rv400_end_programming_mode :: rv400_end_programming_mode(){
    command.start_byte = START_BYTE;
    command.r_kit_code = R_KIT_CODE;
    command.command    = 8;
    command.packet_length = 5;
}

rv400_end_programming_mode* rv400_end_programming_mode :: end_program(){
    rv400_end_programming_mode* a = new rv400_end_programming_mode();
    a->command.checksum = checksum((uint8_t*)
                                   (&a->command), a->command.packet_length-1);
    return a;
}

int rv400_end_programming_mode :: size()
{
    return sizeof(command);
}

rv400_end_programming_mode :: operator uint8_t* ()
{
    return (uint8_t*)(&command);
}

rv400_drive_motors_command::operator uint8_t* ()
{
    uint8_t *cmd;

    switch (move_type) {
        case TURN_TO:
            cmd = (uint8_t*) &command_turn_to;
            break;
        case FORWARD:
            cmd = (uint8_t*) &command_forward;
            break;
        default:
            cmd = (uint8_t*) &command;
            break;
    };

    return cmd;
}

int rv400_drive_motors_command::size() 
{
    switch (move_type) {
        case TURN_TO:
            return sizeof(command_turn_to);           
        case FORWARD:
            return sizeof(command_forward);            
        default:
            return sizeof(command);            
    };
}

rv400_buzzer_command::rv400_buzzer_command(uint8_t num_of_buzzes,
                                           uint8_t frequency)
{
    if (num_of_buzzes<MIN_NUM_OF_BUZZES || num_of_buzzes>MAX_NUM_OF_BUZZES)
        throw "rv400_buzzer_command: error in number of buzzes";
        
    if (frequency<MIN_BUZZ_FREQUENCY)
        throw "rv400_buzzer_command: error frequency";
        
    command.start_byte=START_BYTE;
    command.packet_length=7;
    command.r_kit_code=R_KIT_CODE;
    command.command=6;
    command.num_of_buzzes=num_of_buzzes;
    command.frequency=frequency;
    command.checksum=checksum((uint8_t*)&command,6);
}

rv400_buzzer_command* rv400_buzzer_command::buzzer(uint8_t num_of_buzzes,
                                                   uint8_t frequency)
{
    return new rv400_buzzer_command(num_of_buzzes,frequency);
}

rv400_buzzer_command::operator uint8_t* ()
{
    return (uint8_t*)&command;
}

int rv400_buzzer_command::size()
{
    return sizeof(command);
}

rv400_dummy_command::rv400_dummy_command()
{
    command.start_byte = START_BYTE;
    command.packet_length = 5;
    command.r_kit_code = R_KIT_CODE;
    command.command = 7;
    command.checksum = checksum((uint8_t*)&command,4);
}

rv400_dummy_command* rv400_dummy_command::dummy()
{
    return new rv400_dummy_command();
}

rv400_dummy_command::operator uint8_t* ()
{
    return (uint8_t*)&command;
}

int rv400_dummy_command::size()
{
    return sizeof(command);
}

rv400_vac_motor_command::rv400_vac_motor_command()
{
    command.start_byte = START_BYTE;
    command.packet_length = 7;
    command.r_kit_code = R_KIT_CODE;
    command.command = 4;
    command.dummy_data = 0;    
}

rv400_vac_motor_command* rv400_vac_motor_command::vac_motor(uint8_t pwm){
    if (pwm > MAX_VAC_MOTOR_COMMAND_PWM){
        throw "rv400_vac_motor_command: error in pwm - too high";
    }
    rv400_vac_motor_command *a = new rv400_vac_motor_command();
    a->command.data = pwm;
    a->command.checksum = checksum((uint8_t*)&(a->command),a->command.packet_length);

    return a;
}

int rv400_vac_motor_command::size()
{
    return sizeof(command);
}

rv400_vac_motor_command::operator uint8_t* ()
{
    return (uint8_t*)&command;
}

////////////////////////////////////////////////////////////////////////////////

rv400_brush_motor_command::rv400_brush_motor_command()
{
    command.start_byte = START_BYTE;
    command.packet_length = 8;
    command.r_kit_code = R_KIT_CODE;
    command.command = 4;
    command.dummy_data = 1;   
    
}

rv400_brush_motor_command* rv400_brush_motor_command::brush_motor(uint8_t pwm, 
                                                motor_direction_t direction){
    if (pwm > MAX_BRUSH_MOTOR_COMMAND_PWM){
        throw "rv400_brush_motor_command: error in pwm - too high";
    }
    rv400_brush_motor_command *a = new rv400_brush_motor_command();
    a->command.pwm = pwm;
    a->command.direction = (uint8_t)direction;
    a->command.checksum = checksum((uint8_t*)&(a->command),a->command.packet_length);

    return a;
}

int rv400_brush_motor_command::size()
{
    return sizeof(command);
}

rv400_brush_motor_command::operator uint8_t* ()
{
    return (uint8_t*)&command;
}

////////////////////////////////////////////////////////////////////////////////
rv400_brush_elevation_motor_command::rv400_brush_elevation_motor_command()
{
    command.start_byte = START_BYTE;
    command.packet_length = 7;
    command.r_kit_code = R_KIT_CODE;
    command.command = 4;
    command.dummy_data = 3;   
    
}

rv400_brush_elevation_motor_command* rv400_brush_elevation_motor_command::brush_height(uint8_t height){
    if (height > MAX_BRUSH_ELEVATION_HEIGHT){
        throw "rv400_brush_elevation_motor_command: error in height - too high";
    }
    rv400_brush_elevation_motor_command *a = new rv400_brush_elevation_motor_command();
    a->command.height = height;
    a->command.checksum = checksum((uint8_t*)&(a->command),a->command.packet_length);
    return a;
}

int rv400_brush_elevation_motor_command::size()
{
    return sizeof(command);
}

rv400_brush_elevation_motor_command::operator uint8_t* ()
{
    return (uint8_t*)&command;
}

////////////////////////////////////////////////////////////////////////////////

rv400_timeout_threshold_command::rv400_timeout_threshold_command()
{
    command.start_byte = START_BYTE;
    command.packet_length=6;
    command.r_kit_code = R_KIT_CODE;
    command.command=9;
}

rv400_timeout_threshold_command* rv400_timeout_threshold_command::
                                        timeout_threshold(uint8_t threshold)
{
    rv400_timeout_threshold_command* a = new rv400_timeout_threshold_command();
    
    a->command.data = threshold;
    a->command.checksum = checksum((uint8_t*)&a->command,5);
    
    return a;
}

rv400_timeout_threshold_command::operator uint8_t* ()
{
    return (uint8_t*)&command;
}

int rv400_timeout_threshold_command::size()
{
    return sizeof(command);
}
