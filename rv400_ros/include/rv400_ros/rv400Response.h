/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef RV400_RESPONSE_H
#define RV400_RESPONSE_H

#include <inttypes.h>

#define NUMBER_OF_SONARS 10
#define START_BYTE 170
#define R_KIT_CODE 11

extern uint8_t checksum(uint8_t* packet, uint8_t length);

#define fromByteToBool(byte,place) bool(byte & 1<<place)

/* base/factory class for all rv400 responses */
class rv400_response
{
public:
    enum types { STOP=1 , DATA=3 , ACK = 5 };
protected:
    rv400_response(types type);
private:
    types m_type;
public:   
    static rv400_response* parseResponse(uint8_t* reponse,uint8_t len);
    virtual types type() const;
    
    virtual ~rv400_response() {}
};

class rv400_data_response : public rv400_response
{
public:
    uint8_t           massage_rate;
    
    //telemetry data A
    uint16_t sonars[NUMBER_OF_SONARS];
    
    uint16_t   odometery_heading;
    uint32_t   odometery_acc_distance;
    uint32_t   odometery_left_tics;
    uint32_t   odometery_right_tics;
    uint32_t   odometery_left_backward_tics;
    uint32_t   odometery_right_backward_tics;
    uint8_t    odometery_left_direction;
    uint8_t    odometery_right_direction;

    uint8_t           bumper_left;
    uint8_t           bumper_right;
    uint8_t           bumper_left_nozzle;
    uint8_t           bumper_right_nozzle;
    uint8_t           nozzle_height;
    uint8_t           charging_voltage;

    //telemetry data B
    uint8_t            vcc_voltage;
    uint8_t            ambient_temperature;
    uint8_t            brush_elevation_motor_current;
    uint8_t            right_drive_motor_current;
    uint8_t            left_drive_motor_current;
    uint8_t            selector_position;
    uint8_t            front_stairs_psd;
    uint8_t            humidity;

    //telemtry data C
    uint8_t            battery_voltage;
    uint8_t            stairs;
    uint8_t            buttons_bitmap;
    uint8_t            leash;
    uint8_t            door;

    //bit #6 returns 5 uint8_ts, as:
    uint8_t            ir_receivers_front_right_sensor;
    uint8_t            ir_receivers_front_left_sensor;
    uint8_t            ir_receivers_right_sensor;
    uint8_t            ir_receivers_left_sensor;
    uint8_t            ir_receivers_rear_sensor;

    uint8_t            charging_plug;
    uint8_t            charging_temperature;
    
    //telemtry data D
    uint8_t            left_battery_thermistor;
    uint8_t            right_battery_thermistor;   

private:    
    rv400_data_response(uint8_t *response,uint8_t len);

    friend class rv400_response;
};

class rv400_ack_response : public rv400_response
{
public:
    /* Acknowledge types */
    static const uint8_t ACK_DRIVE_RECEPTION = 1;
    static const uint8_t ACK_DRIVE_COMPLETION = 2;
    static const uint8_t ACK_GLOBAL = 3;
    
    /* Drive command termination reason */
    static const uint8_t UNRECOGNIZED_EVENT = 0;
    static const uint8_t BUMPER_EVENT = 1;
    static const uint8_t STAIR_EVENT = 2;
    static const uint8_t DISTANCE_REACHED = 3;
    static const uint8_t ULTRASOUND_EVENT = 4;
    static const uint8_t HEADING_REACHED = 5;
    
    uint8_t ack_type() const;
    uint8_t ack_reason() const;

private:  
    uint8_t acknowledgeType;
    uint8_t driveCommandTerminationReason;  
    rv400_ack_response(uint8_t ackType, uint8_t reason);

    friend class rv400_response;
};

#endif // RV400_RESPONSE_H
