# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/src/dump/avilib.c" "/home/yinon/dev/ros_corobot/build/corobot/corobot_camera/CMakeFiles/dump.dir/src/dump/avilib.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"corobot_camera\""
  "_FILE_OFFSET_BITS=64"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/yinon/dev/ros_corobot/devel/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_diagnostics/include"
  "/opt/ros/kinetic/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/cfg/cpp"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/src/dump/dump.cpp" "/home/yinon/dev/ros_corobot/build/corobot/corobot_camera/CMakeFiles/dump.dir/src/dump/dump.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"corobot_camera\""
  "_FILE_OFFSET_BITS=64"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/yinon/dev/ros_corobot/devel/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_diagnostics/include"
  "/opt/ros/kinetic/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/cfg/cpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/yinon/dev/ros_corobot/build/corobot/corobot_camera/CMakeFiles/uvc_cam.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
