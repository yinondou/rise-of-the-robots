# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/src/enumerate/enumerate.cpp" "/home/yinon/dev/ros_corobot/build/corobot/corobot_camera/CMakeFiles/enumerate.dir/src/enumerate/enumerate.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"corobot_camera\""
  "_FILE_OFFSET_BITS=64"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/yinon/dev/ros_corobot/devel/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_diagnostics/include"
  "/opt/ros/kinetic/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_camera/cfg/cpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/yinon/dev/ros_corobot/build/corobot/corobot_camera/CMakeFiles/uvc_cam.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
