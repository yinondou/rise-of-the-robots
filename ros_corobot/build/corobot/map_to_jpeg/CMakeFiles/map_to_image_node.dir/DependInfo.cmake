# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yinon/dev/ros_corobot/src/corobot/map_to_jpeg/src/map_to_image_node.cpp" "/home/yinon/dev/ros_corobot/build/corobot/map_to_jpeg/CMakeFiles/map_to_image_node.dir/src/map_to_image_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"map_to_jpeg\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/eigen3"
  "/home/yinon/dev/ros_corobot/src/corobot/map_to_jpeg/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/include/opencv-3.1.0-dev/opencv"
  "/opt/ros/kinetic/include/opencv-3.1.0-dev"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
