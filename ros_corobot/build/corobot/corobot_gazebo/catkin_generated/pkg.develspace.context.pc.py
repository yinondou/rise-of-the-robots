# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/yinon/dev/ros_corobot/devel/include;/home/yinon/dev/ros_corobot/src/corobot/corobot_gazebo/include".split(';') if "/home/yinon/dev/ros_corobot/devel/include;/home/yinon/dev/ros_corobot/src/corobot/corobot_gazebo/include" != "" else []
PROJECT_CATKIN_DEPENDS = "gazebo_msgs;geometry_msgs;message_generation".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lcorobot_gazebo".split(';') if "-lcorobot_gazebo" != "" else []
PROJECT_NAME = "corobot_gazebo"
PROJECT_SPACE_DIR = "/home/yinon/dev/ros_corobot/devel"
PROJECT_VERSION = "1.0.0"
