set(_CATKIN_CURRENT_PACKAGE "corobot_gazebo")
set(corobot_gazebo_VERSION "1.0.0")
set(corobot_gazebo_MAINTAINER "Morgan Cormier <mcormier@coroware.com>")
set(corobot_gazebo_PACKAGE_FORMAT "1")
set(corobot_gazebo_BUILD_DEPENDS "roscpp" "std_msgs" "corobot_msgs" "gazebo_msgs" "geometry_msgs" "tf" "message_generation")
set(corobot_gazebo_BUILD_EXPORT_DEPENDS "roscpp" "std_msgs" "corobot_msgs" "gazebo_msgs" "geometry_msgs" "tf" "message_generation")
set(corobot_gazebo_BUILDTOOL_DEPENDS "catkin")
set(corobot_gazebo_BUILDTOOL_EXPORT_DEPENDS )
set(corobot_gazebo_EXEC_DEPENDS "roscpp" "std_msgs" "corobot_msgs" "gazebo_msgs" "geometry_msgs" "tf" "message_generation")
set(corobot_gazebo_RUN_DEPENDS "roscpp" "std_msgs" "corobot_msgs" "gazebo_msgs" "geometry_msgs" "tf" "message_generation")
set(corobot_gazebo_TEST_DEPENDS )
set(corobot_gazebo_DOC_DEPENDS )
set(corobot_gazebo_DEPRECATED "")