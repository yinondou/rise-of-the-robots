set(_CATKIN_CURRENT_PACKAGE "corobot_phidget_ik")
set(corobot_phidget_ik_VERSION "1.0.0")
set(corobot_phidget_ik_MAINTAINER "Morgan Cormier <mcormier@coroware.com>")
set(corobot_phidget_ik_PACKAGE_FORMAT "1")
set(corobot_phidget_ik_BUILD_DEPENDS "roscpp" "sensor_msgs" "tf" "std_msgs" "corobot_msgs" "corobot_diagnostics" "std_srvs")
set(corobot_phidget_ik_BUILD_EXPORT_DEPENDS "roscpp" "sensor_msgs" "tf" "std_msgs" "corobot_msgs" "corobot_diagnostics" "std_srvs")
set(corobot_phidget_ik_BUILDTOOL_DEPENDS "catkin")
set(corobot_phidget_ik_BUILDTOOL_EXPORT_DEPENDS )
set(corobot_phidget_ik_EXEC_DEPENDS "roscpp" "sensor_msgs" "tf" "std_msgs" "corobot_msgs" "corobot_diagnostics" "std_srvs")
set(corobot_phidget_ik_RUN_DEPENDS "roscpp" "sensor_msgs" "tf" "std_msgs" "corobot_msgs" "corobot_diagnostics" "std_srvs")
set(corobot_phidget_ik_TEST_DEPENDS )
set(corobot_phidget_ik_DOC_DEPENDS )
set(corobot_phidget_ik_DEPRECATED "")