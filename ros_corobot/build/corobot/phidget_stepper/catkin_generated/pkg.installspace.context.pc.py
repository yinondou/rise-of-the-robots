# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/yinon/dev/ros_corobot/install/include".split(';') if "/home/yinon/dev/ros_corobot/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;corobot_diagnostics".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lphidget_stepper".split(';') if "-lphidget_stepper" != "" else []
PROJECT_NAME = "phidget_stepper"
PROJECT_SPACE_DIR = "/home/yinon/dev/ros_corobot/install"
PROJECT_VERSION = "1.0.0"
