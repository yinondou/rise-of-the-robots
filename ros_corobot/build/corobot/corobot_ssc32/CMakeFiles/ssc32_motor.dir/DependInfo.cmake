# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_ssc32/src/motor.cpp" "/home/yinon/dev/ros_corobot/build/corobot/corobot_ssc32/CMakeFiles/ssc32_motor.dir/src/motor.cpp.o"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_ssc32/src/ssc32.cpp" "/home/yinon/dev/ros_corobot/build/corobot/corobot_ssc32/CMakeFiles/ssc32_motor.dir/src/ssc32.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"corobot_ssc32\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_ssc32/include"
  "/home/yinon/dev/ros_corobot/devel/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_diagnostics/include"
  "/opt/ros/kinetic/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
