# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/yinon/dev/ros_corobot/src/corobot/corobot_arm/include".split(';') if "/home/yinon/dev/ros_corobot/src/corobot/corobot_arm/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rosconsole;std_msgs;xmlrpcpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lcorobot_arm".split(';') if "-lcorobot_arm" != "" else []
PROJECT_NAME = "corobot_arm"
PROJECT_SPACE_DIR = "/home/yinon/dev/ros_corobot/devel"
PROJECT_VERSION = "1.0.0"
