# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "corobot_msgs: 13 messages, 0 services")

set(MSG_I_FLAGS "-Icorobot_msgs:/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/kinetic/share/sensor_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/kinetic/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(corobot_msgs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg" ""
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg" "std_msgs/Header:corobot_msgs/GPSStatus"
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg" NAME_WE)
add_custom_target(_corobot_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "corobot_msgs" "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg" "std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_cpp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
)

### Generating Services

### Generating Module File
_generate_module_cpp(corobot_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(corobot_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(corobot_msgs_generate_messages corobot_msgs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_cpp _corobot_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(corobot_msgs_gencpp)
add_dependencies(corobot_msgs_gencpp corobot_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS corobot_msgs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)
_generate_msg_eus(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
)

### Generating Services

### Generating Module File
_generate_module_eus(corobot_msgs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(corobot_msgs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(corobot_msgs_generate_messages corobot_msgs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_eus _corobot_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(corobot_msgs_geneus)
add_dependencies(corobot_msgs_geneus corobot_msgs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS corobot_msgs_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)
_generate_msg_lisp(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
)

### Generating Services

### Generating Module File
_generate_module_lisp(corobot_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(corobot_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(corobot_msgs_generate_messages corobot_msgs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_lisp _corobot_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(corobot_msgs_genlisp)
add_dependencies(corobot_msgs_genlisp corobot_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS corobot_msgs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)
_generate_msg_nodejs(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
)

### Generating Services

### Generating Module File
_generate_module_nodejs(corobot_msgs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(corobot_msgs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(corobot_msgs_generate_messages corobot_msgs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_nodejs _corobot_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(corobot_msgs_gennodejs)
add_dependencies(corobot_msgs_gennodejs corobot_msgs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS corobot_msgs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)
_generate_msg_py(corobot_msgs
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
)

### Generating Services

### Generating Module File
_generate_module_py(corobot_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(corobot_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(corobot_msgs_generate_messages corobot_msgs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg" NAME_WE)
add_dependencies(corobot_msgs_generate_messages_py _corobot_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(corobot_msgs_genpy)
add_dependencies(corobot_msgs_genpy corobot_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS corobot_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/corobot_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(corobot_msgs_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(corobot_msgs_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/corobot_msgs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(corobot_msgs_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(corobot_msgs_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/corobot_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(corobot_msgs_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(corobot_msgs_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/corobot_msgs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(corobot_msgs_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(corobot_msgs_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/corobot_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(corobot_msgs_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(corobot_msgs_generate_messages_py sensor_msgs_generate_messages_py)
endif()
