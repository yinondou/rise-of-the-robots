# Install script for directory: /home/yinon/dev/ros_corobot/src/corobot/corobot_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/yinon/dev/ros_corobot/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/corobot_msgs/msg" TYPE FILE FILES
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/SensorMsg.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSFix.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/GPSStatus.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MotorCommand.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/MoveArm.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PanTilt.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PosMsg.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/PowerMsg.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoPosition.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/ServoType.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/state.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/takepic.msg"
    "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/msg/videomode.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/corobot_msgs/cmake" TYPE FILE FILES "/home/yinon/dev/ros_corobot/build/corobot/corobot_msgs/catkin_generated/installspace/corobot_msgs-msg-paths.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/yinon/dev/ros_corobot/devel/include/corobot_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/yinon/dev/ros_corobot/devel/share/roseus/ros/corobot_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/yinon/dev/ros_corobot/devel/share/common-lisp/ros/corobot_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/yinon/dev/ros_corobot/devel/share/gennodejs/ros/corobot_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python" -m compileall "/home/yinon/dev/ros_corobot/devel/lib/python2.7/dist-packages/corobot_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/yinon/dev/ros_corobot/devel/lib/python2.7/dist-packages/corobot_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/yinon/dev/ros_corobot/build/corobot/corobot_msgs/catkin_generated/installspace/corobot_msgs.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/corobot_msgs/cmake" TYPE FILE FILES "/home/yinon/dev/ros_corobot/build/corobot/corobot_msgs/catkin_generated/installspace/corobot_msgs-msg-extras.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/corobot_msgs/cmake" TYPE FILE FILES
    "/home/yinon/dev/ros_corobot/build/corobot/corobot_msgs/catkin_generated/installspace/corobot_msgsConfig.cmake"
    "/home/yinon/dev/ros_corobot/build/corobot/corobot_msgs/catkin_generated/installspace/corobot_msgsConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/corobot_msgs" TYPE FILE FILES "/home/yinon/dev/ros_corobot/src/corobot/corobot_msgs/package.xml")
endif()

