/****************************************************************************
** Meta object code from reading C++ file 'GPS.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/corobot/corobot_teleop/include/GPS.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GPS.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Gps[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,    5,    4,    4, 0x05,

 // slots: signature, parameters, type, tag, flags
      29,    5,    4,    4, 0x0a,
      51,    5,    4,    4, 0x0a,
      73,   65,    4,    4, 0x0a,
     101,    4,    4,    4, 0x0a,
     117,    4,    4,    4, 0x0a,
     132,    4,    4,    4, 0x0a,
     147,    4,    4,    4, 0x0a,
     162,    4,    4,    4, 0x0a,
     175,    4,    4,    4, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Gps[] = {
    "Gps\0\0value\0url_changed(QUrl)\0"
    "set_map_type(QString)\0set_zoom(int)\0"
    "lat,lon\0update_coord(double,double)\0"
    "start_clicked()\0stop_clicked()\0"
    "save_clicked()\0load_clicked()\0"
    "check_size()\0selection_changed()\0"
};

void Gps::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Gps *_t = static_cast<Gps *>(_o);
        switch (_id) {
        case 0: _t->url_changed((*reinterpret_cast< QUrl(*)>(_a[1]))); break;
        case 1: _t->set_map_type((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->set_zoom((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->update_coord((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 4: _t->start_clicked(); break;
        case 5: _t->stop_clicked(); break;
        case 6: _t->save_clicked(); break;
        case 7: _t->load_clicked(); break;
        case 8: _t->check_size(); break;
        case 9: _t->selection_changed(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Gps::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Gps::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Gps,
      qt_meta_data_Gps, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Gps::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Gps::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Gps::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Gps))
        return static_cast<void*>(const_cast< Gps*>(this));
    return QWidget::qt_metacast(_clname);
}

int Gps::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void Gps::url_changed(QUrl _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
