/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/corobot/corobot_teleop/include/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      31,   27,   11,   11, 0x0a,
      48,   11,   11,   11, 0x0a,
      72,   66,   11,   11, 0x0a,
      91,   66,   11,   11, 0x0a,
     110,   66,   11,   11, 0x0a,
     130,   66,   11,   11, 0x0a,
     155,   66,   11,   11, 0x0a,
     214,  182,   11,   11, 0x0a,
     250,   11,   11,   11, 0x0a,
     298,  267,   11,   11, 0x0a,
     339,  333,   11,   11, 0x0a,
     375,  365,   11,   11, 0x0a,
     445,  409,   11,   11, 0x0a,
     522,  504,   11,   11, 0x0a,
     565,   11,   11,   11, 0x0a,
     580,   11,   11,   11, 0x0a,
     596,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0size_changed()\0url\0"
    "change_url(QUrl)\0connect_clicked()\0"
    "image\0update_ptz(QImage)\0update_map(QImage)\0"
    "update_rear(QImage)\0update_kinectRGB(QImage)\0"
    "update_kinectDepth(QImage)\0"
    "bumper1,bumper2,bumper3,bumper4\0"
    "bumper_update_slot(int,int,int,int)\0"
    "Pan_Tilt_reset()\0linearVelocity,angularVelocity\0"
    "encoder_info_update(double,double)\0"
    "value\0motor_control_toggle(int)\0"
    "ir01,ir02\0irdata_update_slot(double,double)\0"
    "acc_x,acc_y,acc_z,ang_x,ang_y,ang_z\0"
    "imu_update_slot(double,double,double,double,double,double)\0"
    "mag_x,mag_y,mag_z\0"
    "magnetic_update_slot(double,double,double)\0"
    "showLRFlines()\0showIRdata(int)\0"
    "showLRFdata(int)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->size_changed(); break;
        case 1: _t->change_url((*reinterpret_cast< QUrl(*)>(_a[1]))); break;
        case 2: _t->connect_clicked(); break;
        case 3: _t->update_ptz((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 4: _t->update_map((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 5: _t->update_rear((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 6: _t->update_kinectRGB((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 7: _t->update_kinectDepth((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 8: _t->bumper_update_slot((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 9: _t->Pan_Tilt_reset(); break;
        case 10: _t->encoder_info_update((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 11: _t->motor_control_toggle((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->irdata_update_slot((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 13: _t->imu_update_slot((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6]))); break;
        case 14: _t->magnetic_update_slot((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 15: _t->showLRFlines(); break;
        case 16: _t->showIRdata((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->showLRFdata((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::size_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
