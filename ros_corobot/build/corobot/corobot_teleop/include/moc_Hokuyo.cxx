/****************************************************************************
** Meta object code from reading C++ file 'Hokuyo.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/corobot/corobot_teleop/include/Hokuyo.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Hokuyo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Hokuyo[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,    8,    7,    7, 0x0a,
      71,   53,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Hokuyo[] = {
    "Hokuyo\0\0hokuyo_points_\0"
    "hokuyo_update(Hokuyo_Points*)\0"
    "IR01_new,IR02_new\0IR_update(double,double)\0"
};

void Hokuyo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Hokuyo *_t = static_cast<Hokuyo *>(_o);
        switch (_id) {
        case 0: _t->hokuyo_update((*reinterpret_cast< Hokuyo_Points*(*)>(_a[1]))); break;
        case 1: _t->IR_update((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Hokuyo::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Hokuyo::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Hokuyo,
      qt_meta_data_Hokuyo, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Hokuyo::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Hokuyo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Hokuyo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Hokuyo))
        return static_cast<void*>(const_cast< Hokuyo*>(this));
    return QWidget::qt_metacast(_clname);
}

int Hokuyo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
