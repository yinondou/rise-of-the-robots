/****************************************************************************
** Meta object code from reading C++ file 'Ros.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/corobot/corobot_teleop/include/Ros.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Ros.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Ros[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      39,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      20,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    5,    4,    4, 0x05,
      41,   35,    4,    4, 0x05,
      74,   64,    4,    4, 0x05,
     102,   96,    4,    4, 0x05,
     127,  123,    4,    4, 0x05,
     147,  143,    4,    4, 0x05,
     171,  163,    4,    4, 0x05,
     245,  196,    4,    4, 0x05,
     282,  276,    4,    4, 0x05,
     306,  276,    4,    4, 0x05,
     329,  276,    4,    4, 0x05,
     351,  276,    4,    4, 0x05,
     381,  276,    4,    4, 0x05,
     415,  409,    4,    4, 0x05,
     469,  437,    4,    4, 0x05,
     515,  500,    4,    4, 0x05,
     580,  544,    4,    4, 0x05,
     650,  632,    4,    4, 0x05,
     700,  686,    4,    4, 0x05,
     735,  730,    4,    4, 0x05,

 // slots: signature, parameters, type, tag, flags
     752,   35,    4,    4, 0x0a,
     775,  769,    4,    4, 0x0a,
     798,    4,  793,    4, 0x0a,
     810,    4,  793,    4, 0x0a,
     823,    4,  793,    4, 0x0a,
     845,  836,    4,    4, 0x0a,
     875,  869,    4,    4, 0x0a,
     896,   35,    4,    4, 0x0a,
     914,    4,    4,    4, 0x0a,
     925,    4,  793,    4, 0x0a,
     942,    4,  793,    4, 0x0a,
     959,    4,  793,    4, 0x0a,
     985,    4,  793,    4, 0x0a,
    1005,  997,    4,    4, 0x0a,
    1024,  997,    4,    4, 0x0a,
    1047,  997,    4,    4, 0x0a,
    1072, 1066,    4,    4, 0x0a,
    1101,   96,    4,    4, 0x0a,
    1118,   96,    4,    4, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Ros[] = {
    "Ros\0\0x,y\0posArmReal(double,double)\0"
    "angle\0angleWristReal(double)\0ir01,ir02\0"
    "irData(double,double)\0value\0"
    "battery_percent(int)\0lat\0gps_lat(double)\0"
    "lon\0gps_lon(double)\0lat,lon\0"
    "gps_coord(double,double)\0"
    "arm_al5a,arm_pincher,arm_reactor,arm_old_corobot\0"
    "arm_model(bool,bool,bool,bool)\0image\0"
    "update_mapimage(QImage)\0update_rearcam(QImage)\0"
    "update_ptzcam(QImage)\0"
    "update_kinectDepthcam(QImage)\0"
    "update_kinectRGBcam(QImage)\0volts\0"
    "battery_volts(double)\0"
    "bumper1,bumper2,bumper3,bumper4\0"
    "bumper_update(int,int,int,int)\0"
    "linear,angular\0velocity_info(double,double)\0"
    "acc_x,acc_y,acc_z,ang_x,ang_y,ang_z\0"
    "imu_data(double,double,double,double,double,double)\0"
    "mag_x,mag_y,mag_z\0magnetic_data(double,double,double)\0"
    "hokuyo_points\0hokuyo_update(Hokuyo_Points*)\0"
    "save\0save_image(bool)\0turnWrist(float)\0"
    "state\0moveGripper(bool)\0bool\0turn_left()\0"
    "turn_right()\0motor_stop()\0shoulder\0"
    "moveShoulderArm(double)\0elbow\0"
    "moveElbowArm(double)\0rotateArm(double)\0"
    "ResetArm()\0decrease_speed()\0"
    "increase_speed()\0increase_backward_speed()\0"
    "stop_turn()\0toggled\0setSpeedFast(bool)\0"
    "setSpeedModerate(bool)\0setSpeedSlow(bool)\0"
    "index\0currentCameraTabChanged(int)\0"
    "Pan_control(int)\0Tilt_control(int)\0"
};

void Ros::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Ros *_t = static_cast<Ros *>(_o);
        switch (_id) {
        case 0: _t->posArmReal((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 1: _t->angleWristReal((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->irData((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 3: _t->battery_percent((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->gps_lat((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->gps_lon((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->gps_coord((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 7: _t->arm_model((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        case 8: _t->update_mapimage((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 9: _t->update_rearcam((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 10: _t->update_ptzcam((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 11: _t->update_kinectDepthcam((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 12: _t->update_kinectRGBcam((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 13: _t->battery_volts((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->bumper_update((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 15: _t->velocity_info((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 16: _t->imu_data((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6]))); break;
        case 17: _t->magnetic_data((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 18: _t->hokuyo_update((*reinterpret_cast< Hokuyo_Points*(*)>(_a[1]))); break;
        case 19: _t->save_image((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->turnWrist((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 21: _t->moveGripper((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: { bool _r = _t->turn_left();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 23: { bool _r = _t->turn_right();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 24: { bool _r = _t->motor_stop();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 25: _t->moveShoulderArm((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 26: _t->moveElbowArm((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 27: _t->rotateArm((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 28: _t->ResetArm(); break;
        case 29: { bool _r = _t->decrease_speed();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 30: { bool _r = _t->increase_speed();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 31: { bool _r = _t->increase_backward_speed();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 32: { bool _r = _t->stop_turn();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 33: _t->setSpeedFast((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 34: _t->setSpeedModerate((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 35: _t->setSpeedSlow((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 36: _t->currentCameraTabChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->Pan_control((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 38: _t->Tilt_control((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Ros::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Ros::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_Ros,
      qt_meta_data_Ros, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Ros::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Ros::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Ros::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Ros))
        return static_cast<void*>(const_cast< Ros*>(this));
    return QThread::qt_metacast(_clname);
}

int Ros::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 39)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 39;
    }
    return _id;
}

// SIGNAL 0
void Ros::posArmReal(double _t1, double _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Ros::angleWristReal(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Ros::irData(double _t1, double _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Ros::battery_percent(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Ros::gps_lat(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Ros::gps_lon(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Ros::gps_coord(double _t1, double _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Ros::arm_model(bool _t1, bool _t2, bool _t3, bool _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Ros::update_mapimage(QImage _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Ros::update_rearcam(QImage _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Ros::update_ptzcam(QImage _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Ros::update_kinectDepthcam(QImage _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Ros::update_kinectRGBcam(QImage _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void Ros::battery_volts(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void Ros::bumper_update(int _t1, int _t2, int _t3, int _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void Ros::velocity_info(double _t1, double _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void Ros::imu_data(double _t1, double _t2, double _t3, double _t4, double _t5, double _t6)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void Ros::magnetic_data(double _t1, double _t2, double _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void Ros::hokuyo_update(Hokuyo_Points * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void Ros::save_image(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}
QT_END_MOC_NAMESPACE
