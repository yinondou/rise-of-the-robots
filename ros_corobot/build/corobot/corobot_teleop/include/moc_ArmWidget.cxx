/****************************************************************************
** Meta object code from reading C++ file 'ArmWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/corobot/corobot_teleop/include/ArmWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ArmWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ArmWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   11,   10,   10, 0x05,
      32,   11,   10,   10, 0x05,
      47,   11,   10,   10, 0x05,
      73,   11,   10,   10, 0x05,
     100,   96,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
     120,   11,   10,   10, 0x0a,
     142,   11,   10,   10, 0x0a,
     161,   96,   10,   10, 0x0a,
     189,   11,   10,   10, 0x0a,
     252,  203,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ArmWidget[] = {
    "ArmWidget\0\0value\0theta1(double)\0"
    "theta2(double)\0shoulderAngle_rad(double)\0"
    "elbowAngle_rad(double)\0x,y\0"
    "posarm(float,float)\0shoulder_degree(bool)\0"
    "elbow_degree(bool)\0received_pos(double,double)\0"
    "Corobot(bool)\0"
    "arm_al5a,arm_pincher,arm_reactor,arm_old_corobot\0"
    "setModel(bool,bool,bool,bool)\0"
};

void ArmWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ArmWidget *_t = static_cast<ArmWidget *>(_o);
        switch (_id) {
        case 0: _t->theta1((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->theta2((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->shoulderAngle_rad((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->elbowAngle_rad((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->posarm((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 5: _t->shoulder_degree((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->elbow_degree((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->received_pos((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 8: _t->Corobot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->setModel((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ArmWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ArmWidget::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_ArmWidget,
      qt_meta_data_ArmWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ArmWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ArmWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ArmWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ArmWidget))
        return static_cast<void*>(const_cast< ArmWidget*>(this));
    return QGraphicsView::qt_metacast(_clname);
}

int ArmWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void ArmWidget::theta1(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ArmWidget::theta2(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ArmWidget::shoulderAngle_rad(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ArmWidget::elbowAngle_rad(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ArmWidget::posarm(float _t1, float _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
