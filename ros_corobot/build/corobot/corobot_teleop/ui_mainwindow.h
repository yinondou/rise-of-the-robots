/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QFormLayout>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include <QtWebKit/QWebView>
#include "../include/MyQGraphicsView.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab_5;
    QGridLayout *gridLayout_24;
    QTabWidget *tabWidget_5;
    QWidget *widget;
    QGridLayout *gridLayout_25;
    QLabel *label_10;
    QLineEdit *Master;
    QLabel *label_39;
    QLineEdit *Host;
    QPushButton *connect;
    QCheckBox *environmentcheckbox;
    QTabWidget *tabWidget_8;
    QWidget *Select;
    QGridLayout *gridLayout_28;
    QRadioButton *radioButton_9;
    QRadioButton *radioButton_10;
    QPushButton *quitButton;
    QWidget *tab;
    QGridLayout *gridLayout;
    QTabWidget *arm;
    QWidget *armPage1;
    QGridLayout *gridLayout_6;
    QFrame *frame_4;
    QGridLayout *gridLayout_11;
    QLabel *label_3;
    QLCDNumber *lcdNumber;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QFrame *frame_5;
    QGridLayout *gridLayout_12;
    QLabel *label_4;
    QRadioButton *radioButton_8;
    QRadioButton *radioButton_7;
    QLCDNumber *lcdNumber_2;
    QFrame *frame;
    QPushButton *ArmResetButton;
    QFrame *frame_6;
    QWidget *tab_11;
    QGridLayout *gridLayout_10;
    QFrame *frame_3;
    QGridLayout *gridLayout_14;
    QRadioButton *radioButton;
    QFrame *frame_7;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_3;
    QRadioButton *radioButton_2;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_12;
    QFrame *frame_2;
    QGridLayout *gridLayout_13;
    QFrame *frame_8;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_11;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_3;
    QLCDNumber *lcdNumber_3;
    QLabel *label_7;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout;
    QTabWidget *drive;
    QWidget *drivePage1;
    QGridLayout *gridLayout_3;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLabel *label_21;
    QWidget *tab_25;
    QGridLayout *gridLayout_8;
    QRadioButton *speed_fast;
    QRadioButton *speed_moderate;
    QRadioButton *speed_slow;
    QGridLayout *gridLayout_35;
    QPushButton *Forward;
    QPushButton *TURNLEFT;
    QPushButton *STOP;
    QPushButton *TURNRIGHT;
    QPushButton *BACKWARD;
    QWidget *tab_3;
    QCheckBox *MotorControlToggle;
    QTabWidget *tabWidget_2;
    QWidget *tab_12;
    QLabel *label_13;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_14;
    QLCDNumber *lcdNumber_8;
    QLabel *label_36;
    QProgressBar *progressBar;
    QWidget *tab_14;
    QFormLayout *formLayout;
    QGridLayout *gridLayout_7;
    QLabel *label_29;
    QLCDNumber *lcdNumber_acc_x;
    QLCDNumber *lcdNumber_acc_y;
    QLabel *label_30;
    QLCDNumber *lcdNumber_ang_x;
    QLCDNumber *lcdNumber_ang_y;
    QLCDNumber *lcdNumber_any_z;
    QLabel *label_31;
    QLCDNumber *lcdNumber_mag_x;
    QLCDNumber *lcdNumber_mag_y;
    QLCDNumber *lcdNumber_mag_z;
    QLCDNumber *lcdNumber_acc_z;
    QLabel *label_27;
    QLabel *label_28;
    QLCDNumber *lcdNumber_IR01;
    QLCDNumber *lcdNumber_IR02;
    QWidget *tab_10;
    QGridLayout *gridLayout_5;
    QLabel *label_24;
    QLabel *label_26;
    QLabel *label_40;
    QLabel *label_19;
    QWidget *tab_23;
    QSlider *Pan_control_bar;
    QSlider *Tilt_control_bar;
    QPushButton *PanTilt_Reset;
    QWidget *tab_26;
    QLabel *label_42;
    QLabel *label_43;
    QLCDNumber *linear_velocity;
    QLCDNumber *angular_velocity;
    QTabWidget *camera;
    QWidget *cameraPage1;
    QGridLayout *gridLayout_4;
    MyQGraphicsView *graphicsView;
    QWidget *tab_16;
    QGridLayout *gridLayout_20;
    MyQGraphicsView *graphicsView_10;
    QWidget *tab_28;
    QGridLayout *gridLayout_21;
    QCheckBox *showLRF;
    QPushButton *ShowLRFlines;
    QCheckBox *ShowIRdata;
    QFrame *frame_hokuyo;
    QLabel *label_22;
    QWidget *tab_4;
    QGridLayout *gridLayout_9;
    MyQGraphicsView *graphicsView_2;
    QWidget *tab_6;
    QGridLayout *gridLayout_17;
    MyQGraphicsView *graphicsView_3;
    QWidget *tab_7;
    QGridLayout *gridLayout_15;
    QTabWidget *drive_2;
    QWidget *tab_27;
    QGridLayout *gridLayout_19;
    QRadioButton *speed_fast_2;
    QRadioButton *speed_moderate_2;
    QRadioButton *speed_slow_2;
    QGridLayout *gridLayout_36;
    QPushButton *Forward_2;
    QPushButton *TURNLEFT_2;
    QPushButton *STOP_2;
    QPushButton *TURNRIGHT_2;
    QPushButton *BACKWARD_2;
    MyQGraphicsView *frontView;
    MyQGraphicsView *mapView;
    QWidget *tab_2;
    QGridLayout *gridLayout_16;
    QWebView *webView;
    QListWidget *listWidget;
    QGridLayout *gridLayout_18;
    QLabel *label_16;
    QLCDNumber *lcdNumber_4;
    QLabel *label_17;
    QLCDNumber *lcdNumber_5;
    QLabel *label_9;
    QSpinBox *spinBox;
    QLabel *label_37;
    QComboBox *comboBox;
    QLabel *label_38;
    QSpinBox *spinBox_2;
    QPushButton *start_gps;
    QPushButton *sto_gps;
    QPushButton *load_gps;
    QPushButton *save_gps;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(1458, 805);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setContextMenuPolicy(Qt::DefaultContextMenu);
        MainWindow->setAutoFillBackground(true);
        MainWindow->setAnimated(false);
        MainWindow->setDockOptions(QMainWindow::AllowTabbedDocks);
        MainWindow->setUnifiedTitleAndToolBarOnMac(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy1);
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        gridLayout_24 = new QGridLayout(tab_5);
        gridLayout_24->setSpacing(6);
        gridLayout_24->setContentsMargins(11, 11, 11, 11);
        gridLayout_24->setObjectName(QString::fromUtf8("gridLayout_24"));
        tabWidget_5 = new QTabWidget(tab_5);
        tabWidget_5->setObjectName(QString::fromUtf8("tabWidget_5"));
        widget = new QWidget();
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_25 = new QGridLayout(widget);
        gridLayout_25->setSpacing(6);
        gridLayout_25->setContentsMargins(11, 11, 11, 11);
        gridLayout_25->setObjectName(QString::fromUtf8("gridLayout_25"));
        label_10 = new QLabel(widget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_25->addWidget(label_10, 0, 0, 1, 1);

        Master = new QLineEdit(widget);
        Master->setObjectName(QString::fromUtf8("Master"));

        gridLayout_25->addWidget(Master, 0, 1, 1, 1);

        label_39 = new QLabel(widget);
        label_39->setObjectName(QString::fromUtf8("label_39"));

        gridLayout_25->addWidget(label_39, 1, 0, 1, 1);

        Host = new QLineEdit(widget);
        Host->setObjectName(QString::fromUtf8("Host"));

        gridLayout_25->addWidget(Host, 1, 1, 1, 1);

        connect = new QPushButton(widget);
        connect->setObjectName(QString::fromUtf8("connect"));
        connect->setMaximumSize(QSize(100, 16777215));

        gridLayout_25->addWidget(connect, 3, 1, 1, 1);

        environmentcheckbox = new QCheckBox(widget);
        environmentcheckbox->setObjectName(QString::fromUtf8("environmentcheckbox"));
        environmentcheckbox->setChecked(true);

        gridLayout_25->addWidget(environmentcheckbox, 2, 1, 1, 1);

        tabWidget_5->addTab(widget, QString());

        gridLayout_24->addWidget(tabWidget_5, 0, 0, 1, 1);

        tabWidget_8 = new QTabWidget(tab_5);
        tabWidget_8->setObjectName(QString::fromUtf8("tabWidget_8"));
        Select = new QWidget();
        Select->setObjectName(QString::fromUtf8("Select"));
        gridLayout_28 = new QGridLayout(Select);
        gridLayout_28->setSpacing(6);
        gridLayout_28->setContentsMargins(11, 11, 11, 11);
        gridLayout_28->setObjectName(QString::fromUtf8("gridLayout_28"));
        radioButton_9 = new QRadioButton(Select);
        radioButton_9->setObjectName(QString::fromUtf8("radioButton_9"));
        radioButton_9->setChecked(true);

        gridLayout_28->addWidget(radioButton_9, 0, 0, 1, 1);

        radioButton_10 = new QRadioButton(Select);
        radioButton_10->setObjectName(QString::fromUtf8("radioButton_10"));

        gridLayout_28->addWidget(radioButton_10, 1, 0, 1, 1);

        tabWidget_8->addTab(Select, QString());

        gridLayout_24->addWidget(tabWidget_8, 0, 1, 1, 1);

        quitButton = new QPushButton(tab_5);
        quitButton->setObjectName(QString::fromUtf8("quitButton"));

        gridLayout_24->addWidget(quitButton, 1, 1, 1, 1);

        tabWidget->addTab(tab_5, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout = new QGridLayout(tab);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        arm = new QTabWidget(tab);
        arm->setObjectName(QString::fromUtf8("arm"));
        sizePolicy1.setHeightForWidth(arm->sizePolicy().hasHeightForWidth());
        arm->setSizePolicy(sizePolicy1);
        armPage1 = new QWidget();
        armPage1->setObjectName(QString::fromUtf8("armPage1"));
        gridLayout_6 = new QGridLayout(armPage1);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        frame_4 = new QFrame(armPage1);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        sizePolicy1.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy1);
        frame_4->setFrameShape(QFrame::Panel);
        frame_4->setFrameShadow(QFrame::Raised);
        gridLayout_11 = new QGridLayout(frame_4);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        label_3 = new QLabel(frame_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(label_3, 0, 0, 1, 1);

        lcdNumber = new QLCDNumber(frame_4);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(lcdNumber->sizePolicy().hasHeightForWidth());
        lcdNumber->setSizePolicy(sizePolicy3);
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        lcdNumber->setFont(font);
        lcdNumber->setLineWidth(0);
        lcdNumber->setSmallDecimalPoint(false);
        lcdNumber->setProperty("value", QVariant(0));
        lcdNumber->setProperty("intValue", QVariant(0));

        gridLayout_11->addWidget(lcdNumber, 0, 2, 1, 1);

        radioButton_5 = new QRadioButton(frame_4);
        radioButton_5->setObjectName(QString::fromUtf8("radioButton_5"));
        sizePolicy1.setHeightForWidth(radioButton_5->sizePolicy().hasHeightForWidth());
        radioButton_5->setSizePolicy(sizePolicy1);
        radioButton_5->setChecked(true);

        gridLayout_11->addWidget(radioButton_5, 2, 0, 1, 1);

        radioButton_6 = new QRadioButton(frame_4);
        radioButton_6->setObjectName(QString::fromUtf8("radioButton_6"));
        sizePolicy1.setHeightForWidth(radioButton_6->sizePolicy().hasHeightForWidth());
        radioButton_6->setSizePolicy(sizePolicy1);

        gridLayout_11->addWidget(radioButton_6, 2, 2, 1, 1);


        gridLayout_6->addWidget(frame_4, 2, 0, 1, 1);

        frame_5 = new QFrame(armPage1);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        sizePolicy1.setHeightForWidth(frame_5->sizePolicy().hasHeightForWidth());
        frame_5->setSizePolicy(sizePolicy1);
        frame_5->setFrameShape(QFrame::Panel);
        frame_5->setFrameShadow(QFrame::Raised);
        gridLayout_12 = new QGridLayout(frame_5);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        label_4 = new QLabel(frame_5);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy2.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy2);

        gridLayout_12->addWidget(label_4, 0, 0, 1, 1);

        radioButton_8 = new QRadioButton(frame_5);
        radioButton_8->setObjectName(QString::fromUtf8("radioButton_8"));
        sizePolicy1.setHeightForWidth(radioButton_8->sizePolicy().hasHeightForWidth());
        radioButton_8->setSizePolicy(sizePolicy1);

        gridLayout_12->addWidget(radioButton_8, 2, 1, 1, 1);

        radioButton_7 = new QRadioButton(frame_5);
        radioButton_7->setObjectName(QString::fromUtf8("radioButton_7"));
        sizePolicy1.setHeightForWidth(radioButton_7->sizePolicy().hasHeightForWidth());
        radioButton_7->setSizePolicy(sizePolicy1);
        radioButton_7->setChecked(true);

        gridLayout_12->addWidget(radioButton_7, 2, 0, 1, 1);

        lcdNumber_2 = new QLCDNumber(frame_5);
        lcdNumber_2->setObjectName(QString::fromUtf8("lcdNumber_2"));
        sizePolicy3.setHeightForWidth(lcdNumber_2->sizePolicy().hasHeightForWidth());
        lcdNumber_2->setSizePolicy(sizePolicy3);
        lcdNumber_2->setFont(font);
        lcdNumber_2->setLineWidth(0);

        gridLayout_12->addWidget(lcdNumber_2, 0, 1, 1, 1);


        gridLayout_6->addWidget(frame_5, 2, 2, 1, 1);

        frame = new QFrame(armPage1);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy4(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy4);
        frame->setMinimumSize(QSize(210, 110));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);

        gridLayout_6->addWidget(frame, 0, 0, 1, 1);

        ArmResetButton = new QPushButton(armPage1);
        ArmResetButton->setObjectName(QString::fromUtf8("ArmResetButton"));

        gridLayout_6->addWidget(ArmResetButton, 1, 0, 1, 1);

        frame_6 = new QFrame(armPage1);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        sizePolicy4.setHeightForWidth(frame_6->sizePolicy().hasHeightForWidth());
        frame_6->setSizePolicy(sizePolicy4);
        frame_6->setMinimumSize(QSize(210, 110));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);

        gridLayout_6->addWidget(frame_6, 0, 2, 1, 1);

        arm->addTab(armPage1, QString());
        tab_11 = new QWidget();
        tab_11->setObjectName(QString::fromUtf8("tab_11"));
        gridLayout_10 = new QGridLayout(tab_11);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        frame_3 = new QFrame(tab_11);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        sizePolicy1.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy1);
        frame_3->setFrameShape(QFrame::Panel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayout_14 = new QGridLayout(frame_3);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        radioButton = new QRadioButton(frame_3);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));
        radioButton->setChecked(true);

        gridLayout_14->addWidget(radioButton, 1, 4, 1, 1);

        frame_7 = new QFrame(frame_3);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        sizePolicy4.setHeightForWidth(frame_7->sizePolicy().hasHeightForWidth());
        frame_7->setSizePolicy(sizePolicy4);
        frame_7->setMinimumSize(QSize(50, 50));
        frame_7->setFrameShape(QFrame::StyledPanel);
        frame_7->setFrameShadow(QFrame::Raised);

        gridLayout_14->addWidget(frame_7, 0, 2, 3, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_4->addWidget(label_5);


        gridLayout_14->addLayout(verticalLayout_4, 0, 3, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        radioButton_2 = new QRadioButton(frame_3);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));

        verticalLayout_3->addWidget(radioButton_2);


        gridLayout_14->addLayout(verticalLayout_3, 1, 5, 1, 1);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_12 = new QLabel(frame_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        sizePolicy4.setHeightForWidth(label_12->sizePolicy().hasHeightForWidth());
        label_12->setSizePolicy(sizePolicy4);
        label_12->setMinimumSize(QSize(50, 50));
        label_12->setMaximumSize(QSize(50, 50));

        verticalLayout_7->addWidget(label_12);


        gridLayout_14->addLayout(verticalLayout_7, 0, 0, 3, 1);


        gridLayout_10->addWidget(frame_3, 1, 0, 1, 1);

        frame_2 = new QFrame(tab_11);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        sizePolicy1.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy1);
        frame_2->setFrameShape(QFrame::Panel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_13 = new QGridLayout(frame_2);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        frame_8 = new QFrame(frame_2);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        sizePolicy4.setHeightForWidth(frame_8->sizePolicy().hasHeightForWidth());
        frame_8->setSizePolicy(sizePolicy4);
        frame_8->setMinimumSize(QSize(50, 50));
        frame_8->setFrameShape(QFrame::StyledPanel);
        frame_8->setFrameShadow(QFrame::Raised);

        gridLayout_13->addWidget(frame_8, 0, 1, 4, 1);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_11 = new QLabel(frame_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        sizePolicy4.setHeightForWidth(label_11->sizePolicy().hasHeightForWidth());
        label_11->setSizePolicy(sizePolicy4);
        label_11->setMinimumSize(QSize(50, 50));
        label_11->setMaximumSize(QSize(50, 50));

        verticalLayout_8->addWidget(label_11);


        gridLayout_13->addLayout(verticalLayout_8, 0, 0, 4, 1);

        radioButton_4 = new QRadioButton(frame_2);
        radioButton_4->setObjectName(QString::fromUtf8("radioButton_4"));

        gridLayout_13->addWidget(radioButton_4, 3, 4, 1, 1);

        radioButton_3 = new QRadioButton(frame_2);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));
        radioButton_3->setChecked(true);

        gridLayout_13->addWidget(radioButton_3, 3, 3, 1, 1);

        lcdNumber_3 = new QLCDNumber(frame_2);
        lcdNumber_3->setObjectName(QString::fromUtf8("lcdNumber_3"));
        sizePolicy1.setHeightForWidth(lcdNumber_3->sizePolicy().hasHeightForWidth());
        lcdNumber_3->setSizePolicy(sizePolicy1);
        lcdNumber_3->setLineWidth(0);

        gridLayout_13->addWidget(lcdNumber_3, 1, 4, 1, 1);

        label_7 = new QLabel(frame_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        sizePolicy1.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy1);

        gridLayout_13->addWidget(label_7, 1, 3, 1, 1);

        label_8 = new QLabel(frame_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_13->addWidget(label_8, 1, 2, 1, 1);


        gridLayout_10->addWidget(frame_2, 0, 0, 1, 1);

        arm->addTab(tab_11, QString());

        gridLayout->addWidget(arm, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        drive = new QTabWidget(tab);
        drive->setObjectName(QString::fromUtf8("drive"));
        sizePolicy1.setHeightForWidth(drive->sizePolicy().hasHeightForWidth());
        drive->setSizePolicy(sizePolicy1);
        drivePage1 = new QWidget();
        drivePage1->setObjectName(QString::fromUtf8("drivePage1"));
        gridLayout_3 = new QGridLayout(drivePage1);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label = new QLabel(drivePage1);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy4.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy4);
        label->setMinimumSize(QSize(222, 102));
        label->setMaximumSize(QSize(222, 102));
        label->setScaledContents(true);

        gridLayout_3->addWidget(label, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(drivePage1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy4.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy4);
        label_2->setMinimumSize(QSize(52, 52));
        label_2->setMaximumSize(QSize(52, 52));
        label_2->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        label_2->setScaledContents(true);

        horizontalLayout_2->addWidget(label_2);

        label_21 = new QLabel(drivePage1);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(50, 50));
        label_21->setMaximumSize(QSize(50, 50));

        horizontalLayout_2->addWidget(label_21);


        gridLayout_3->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        drive->addTab(drivePage1, QString());
        tab_25 = new QWidget();
        tab_25->setObjectName(QString::fromUtf8("tab_25"));
        gridLayout_8 = new QGridLayout(tab_25);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        speed_fast = new QRadioButton(tab_25);
        speed_fast->setObjectName(QString::fromUtf8("speed_fast"));
        speed_fast->setChecked(true);

        gridLayout_8->addWidget(speed_fast, 0, 0, 1, 1);

        speed_moderate = new QRadioButton(tab_25);
        speed_moderate->setObjectName(QString::fromUtf8("speed_moderate"));

        gridLayout_8->addWidget(speed_moderate, 1, 0, 1, 1);

        speed_slow = new QRadioButton(tab_25);
        speed_slow->setObjectName(QString::fromUtf8("speed_slow"));

        gridLayout_8->addWidget(speed_slow, 2, 0, 1, 1);

        gridLayout_35 = new QGridLayout();
        gridLayout_35->setSpacing(6);
        gridLayout_35->setObjectName(QString::fromUtf8("gridLayout_35"));
        Forward = new QPushButton(tab_25);
        Forward->setObjectName(QString::fromUtf8("Forward"));

        gridLayout_35->addWidget(Forward, 0, 1, 1, 1);

        TURNLEFT = new QPushButton(tab_25);
        TURNLEFT->setObjectName(QString::fromUtf8("TURNLEFT"));

        gridLayout_35->addWidget(TURNLEFT, 1, 0, 1, 1);

        STOP = new QPushButton(tab_25);
        STOP->setObjectName(QString::fromUtf8("STOP"));

        gridLayout_35->addWidget(STOP, 1, 1, 1, 1);

        TURNRIGHT = new QPushButton(tab_25);
        TURNRIGHT->setObjectName(QString::fromUtf8("TURNRIGHT"));

        gridLayout_35->addWidget(TURNRIGHT, 1, 2, 1, 1);

        BACKWARD = new QPushButton(tab_25);
        BACKWARD->setObjectName(QString::fromUtf8("BACKWARD"));

        gridLayout_35->addWidget(BACKWARD, 2, 1, 1, 1);


        gridLayout_8->addLayout(gridLayout_35, 3, 0, 1, 1);

        drive->addTab(tab_25, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        MotorControlToggle = new QCheckBox(tab_3);
        MotorControlToggle->setObjectName(QString::fromUtf8("MotorControlToggle"));
        MotorControlToggle->setGeometry(QRect(10, 70, 191, 41));
        MotorControlToggle->setMouseTracking(true);
        drive->addTab(tab_3, QString());

        horizontalLayout->addWidget(drive);

        tabWidget_2 = new QTabWidget(tab);
        tabWidget_2->setObjectName(QString::fromUtf8("tabWidget_2"));
        sizePolicy1.setHeightForWidth(tabWidget_2->sizePolicy().hasHeightForWidth());
        tabWidget_2->setSizePolicy(sizePolicy1);
        tab_12 = new QWidget();
        tab_12->setObjectName(QString::fromUtf8("tab_12"));
        label_13 = new QLabel(tab_12);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(9, 75, 16, 17));
        layoutWidget1 = new QWidget(tab_12);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(21, 32, 261, 191));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_14 = new QLabel(layoutWidget1);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        sizePolicy1.setHeightForWidth(label_14->sizePolicy().hasHeightForWidth());
        label_14->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_14);

        lcdNumber_8 = new QLCDNumber(layoutWidget1);
        lcdNumber_8->setObjectName(QString::fromUtf8("lcdNumber_8"));
        sizePolicy1.setHeightForWidth(lcdNumber_8->sizePolicy().hasHeightForWidth());
        lcdNumber_8->setSizePolicy(sizePolicy1);
        lcdNumber_8->setLineWidth(0);

        horizontalLayout_4->addWidget(lcdNumber_8);

        label_36 = new QLabel(layoutWidget1);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        horizontalLayout_4->addWidget(label_36);


        verticalLayout->addLayout(horizontalLayout_4);

        progressBar = new QProgressBar(layoutWidget1);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        sizePolicy1.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy1);
        progressBar->setMaximumSize(QSize(1677, 1677));
        progressBar->setLayoutDirection(Qt::LeftToRight);
        progressBar->setValue(100);
        progressBar->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        progressBar->setTextVisible(true);
        progressBar->setOrientation(Qt::Horizontal);
        progressBar->setInvertedAppearance(false);

        verticalLayout->addWidget(progressBar);

        tabWidget_2->addTab(tab_12, QString());
        tab_14 = new QWidget();
        tab_14->setObjectName(QString::fromUtf8("tab_14"));
        formLayout = new QFormLayout(tab_14);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        label_29 = new QLabel(tab_14);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        gridLayout_7->addWidget(label_29, 2, 0, 1, 1);

        lcdNumber_acc_x = new QLCDNumber(tab_14);
        lcdNumber_acc_x->setObjectName(QString::fromUtf8("lcdNumber_acc_x"));
        sizePolicy3.setHeightForWidth(lcdNumber_acc_x->sizePolicy().hasHeightForWidth());
        lcdNumber_acc_x->setSizePolicy(sizePolicy3);
        lcdNumber_acc_x->setFont(font);
        lcdNumber_acc_x->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_acc_x, 2, 1, 1, 1);

        lcdNumber_acc_y = new QLCDNumber(tab_14);
        lcdNumber_acc_y->setObjectName(QString::fromUtf8("lcdNumber_acc_y"));
        sizePolicy3.setHeightForWidth(lcdNumber_acc_y->sizePolicy().hasHeightForWidth());
        lcdNumber_acc_y->setSizePolicy(sizePolicy3);
        lcdNumber_acc_y->setFont(font);
        lcdNumber_acc_y->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_acc_y, 2, 2, 1, 2);

        label_30 = new QLabel(tab_14);
        label_30->setObjectName(QString::fromUtf8("label_30"));

        gridLayout_7->addWidget(label_30, 3, 0, 1, 1);

        lcdNumber_ang_x = new QLCDNumber(tab_14);
        lcdNumber_ang_x->setObjectName(QString::fromUtf8("lcdNumber_ang_x"));
        sizePolicy3.setHeightForWidth(lcdNumber_ang_x->sizePolicy().hasHeightForWidth());
        lcdNumber_ang_x->setSizePolicy(sizePolicy3);
        lcdNumber_ang_x->setFont(font);
        lcdNumber_ang_x->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_ang_x, 3, 1, 1, 1);

        lcdNumber_ang_y = new QLCDNumber(tab_14);
        lcdNumber_ang_y->setObjectName(QString::fromUtf8("lcdNumber_ang_y"));
        sizePolicy3.setHeightForWidth(lcdNumber_ang_y->sizePolicy().hasHeightForWidth());
        lcdNumber_ang_y->setSizePolicy(sizePolicy3);
        lcdNumber_ang_y->setFont(font);
        lcdNumber_ang_y->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_ang_y, 3, 2, 1, 2);

        lcdNumber_any_z = new QLCDNumber(tab_14);
        lcdNumber_any_z->setObjectName(QString::fromUtf8("lcdNumber_any_z"));
        sizePolicy3.setHeightForWidth(lcdNumber_any_z->sizePolicy().hasHeightForWidth());
        lcdNumber_any_z->setSizePolicy(sizePolicy3);
        lcdNumber_any_z->setFont(font);
        lcdNumber_any_z->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_any_z, 3, 4, 1, 1);

        label_31 = new QLabel(tab_14);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        gridLayout_7->addWidget(label_31, 4, 0, 1, 1);

        lcdNumber_mag_x = new QLCDNumber(tab_14);
        lcdNumber_mag_x->setObjectName(QString::fromUtf8("lcdNumber_mag_x"));
        sizePolicy3.setHeightForWidth(lcdNumber_mag_x->sizePolicy().hasHeightForWidth());
        lcdNumber_mag_x->setSizePolicy(sizePolicy3);
        lcdNumber_mag_x->setFont(font);
        lcdNumber_mag_x->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_mag_x, 4, 1, 1, 1);

        lcdNumber_mag_y = new QLCDNumber(tab_14);
        lcdNumber_mag_y->setObjectName(QString::fromUtf8("lcdNumber_mag_y"));
        sizePolicy3.setHeightForWidth(lcdNumber_mag_y->sizePolicy().hasHeightForWidth());
        lcdNumber_mag_y->setSizePolicy(sizePolicy3);
        lcdNumber_mag_y->setFont(font);
        lcdNumber_mag_y->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_mag_y, 4, 2, 1, 2);

        lcdNumber_mag_z = new QLCDNumber(tab_14);
        lcdNumber_mag_z->setObjectName(QString::fromUtf8("lcdNumber_mag_z"));
        sizePolicy3.setHeightForWidth(lcdNumber_mag_z->sizePolicy().hasHeightForWidth());
        lcdNumber_mag_z->setSizePolicy(sizePolicy3);
        lcdNumber_mag_z->setFont(font);
        lcdNumber_mag_z->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_mag_z, 4, 4, 1, 1);

        lcdNumber_acc_z = new QLCDNumber(tab_14);
        lcdNumber_acc_z->setObjectName(QString::fromUtf8("lcdNumber_acc_z"));
        sizePolicy3.setHeightForWidth(lcdNumber_acc_z->sizePolicy().hasHeightForWidth());
        lcdNumber_acc_z->setSizePolicy(sizePolicy3);
        lcdNumber_acc_z->setFont(font);
        lcdNumber_acc_z->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_acc_z, 2, 4, 1, 1);

        label_27 = new QLabel(tab_14);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        gridLayout_7->addWidget(label_27, 0, 0, 1, 1);

        label_28 = new QLabel(tab_14);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        gridLayout_7->addWidget(label_28, 1, 0, 1, 1);

        lcdNumber_IR01 = new QLCDNumber(tab_14);
        lcdNumber_IR01->setObjectName(QString::fromUtf8("lcdNumber_IR01"));
        sizePolicy3.setHeightForWidth(lcdNumber_IR01->sizePolicy().hasHeightForWidth());
        lcdNumber_IR01->setSizePolicy(sizePolicy3);
        lcdNumber_IR01->setFont(font);
        lcdNumber_IR01->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_IR01, 0, 1, 1, 1);

        lcdNumber_IR02 = new QLCDNumber(tab_14);
        lcdNumber_IR02->setObjectName(QString::fromUtf8("lcdNumber_IR02"));
        sizePolicy3.setHeightForWidth(lcdNumber_IR02->sizePolicy().hasHeightForWidth());
        lcdNumber_IR02->setSizePolicy(sizePolicy3);
        lcdNumber_IR02->setFont(font);
        lcdNumber_IR02->setLineWidth(0);

        gridLayout_7->addWidget(lcdNumber_IR02, 1, 1, 1, 1);


        formLayout->setLayout(2, QFormLayout::LabelRole, gridLayout_7);

        tabWidget_2->addTab(tab_14, QString());
        tab_10 = new QWidget();
        tab_10->setObjectName(QString::fromUtf8("tab_10"));
        gridLayout_5 = new QGridLayout(tab_10);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_24 = new QLabel(tab_10);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        gridLayout_5->addWidget(label_24, 0, 2, 1, 1);

        label_26 = new QLabel(tab_10);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        gridLayout_5->addWidget(label_26, 1, 0, 1, 1);

        label_40 = new QLabel(tab_10);
        label_40->setObjectName(QString::fromUtf8("label_40"));

        gridLayout_5->addWidget(label_40, 1, 2, 1, 1);

        label_19 = new QLabel(tab_10);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout_5->addWidget(label_19, 0, 0, 1, 1);

        tabWidget_2->addTab(tab_10, QString());
        tab_23 = new QWidget();
        tab_23->setObjectName(QString::fromUtf8("tab_23"));
        Pan_control_bar = new QSlider(tab_23);
        Pan_control_bar->setObjectName(QString::fromUtf8("Pan_control_bar"));
        Pan_control_bar->setGeometry(QRect(70, 200, 160, 29));
        Pan_control_bar->setMinimum(-70);
        Pan_control_bar->setMaximum(70);
        Pan_control_bar->setValue(0);
        Pan_control_bar->setOrientation(Qt::Horizontal);
        Tilt_control_bar = new QSlider(tab_23);
        Tilt_control_bar->setObjectName(QString::fromUtf8("Tilt_control_bar"));
        Tilt_control_bar->setGeometry(QRect(140, 20, 29, 160));
        Tilt_control_bar->setMinimum(-30);
        Tilt_control_bar->setMaximum(30);
        Tilt_control_bar->setValue(0);
        Tilt_control_bar->setOrientation(Qt::Vertical);
        PanTilt_Reset = new QPushButton(tab_23);
        PanTilt_Reset->setObjectName(QString::fromUtf8("PanTilt_Reset"));
        PanTilt_Reset->setGeometry(QRect(10, 70, 98, 27));
        tabWidget_2->addTab(tab_23, QString());
        tab_26 = new QWidget();
        tab_26->setObjectName(QString::fromUtf8("tab_26"));
        label_42 = new QLabel(tab_26);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setGeometry(QRect(40, 80, 91, 21));
        label_43 = new QLabel(tab_26);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setGeometry(QRect(40, 140, 101, 21));
        linear_velocity = new QLCDNumber(tab_26);
        linear_velocity->setObjectName(QString::fromUtf8("linear_velocity"));
        linear_velocity->setGeometry(QRect(180, 70, 81, 41));
        linear_velocity->setProperty("value", QVariant(0));
        angular_velocity = new QLCDNumber(tab_26);
        angular_velocity->setObjectName(QString::fromUtf8("angular_velocity"));
        angular_velocity->setGeometry(QRect(180, 130, 81, 41));
        tabWidget_2->addTab(tab_26, QString());

        horizontalLayout->addWidget(tabWidget_2);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);

        camera = new QTabWidget(tab);
        camera->setObjectName(QString::fromUtf8("camera"));
        sizePolicy1.setHeightForWidth(camera->sizePolicy().hasHeightForWidth());
        camera->setSizePolicy(sizePolicy1);
        camera->setFocusPolicy(Qt::NoFocus);
        camera->setTabPosition(QTabWidget::North);
        camera->setTabShape(QTabWidget::Rounded);
        camera->setElideMode(Qt::ElideNone);
        camera->setUsesScrollButtons(true);
        cameraPage1 = new QWidget();
        cameraPage1->setObjectName(QString::fromUtf8("cameraPage1"));
        gridLayout_4 = new QGridLayout(cameraPage1);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        graphicsView = new MyQGraphicsView(cameraPage1);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        sizePolicy1.setHeightForWidth(graphicsView->sizePolicy().hasHeightForWidth());
        graphicsView->setSizePolicy(sizePolicy1);
        graphicsView->setMaximumSize(QSize(16777215, 16777215));
        graphicsView->setFocusPolicy(Qt::NoFocus);
        graphicsView->setInteractive(false);
        graphicsView->setCacheMode(QGraphicsView::CacheBackground);
        graphicsView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
        graphicsView->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

        gridLayout_4->addWidget(graphicsView, 0, 0, 2, 2);

        camera->addTab(cameraPage1, QString());
        tab_16 = new QWidget();
        tab_16->setObjectName(QString::fromUtf8("tab_16"));
        gridLayout_20 = new QGridLayout(tab_16);
        gridLayout_20->setSpacing(6);
        gridLayout_20->setContentsMargins(11, 11, 11, 11);
        gridLayout_20->setObjectName(QString::fromUtf8("gridLayout_20"));
        graphicsView_10 = new MyQGraphicsView(tab_16);
        graphicsView_10->setObjectName(QString::fromUtf8("graphicsView_10"));

        gridLayout_20->addWidget(graphicsView_10, 0, 0, 1, 1);

        camera->addTab(tab_16, QString());
        tab_28 = new QWidget();
        tab_28->setObjectName(QString::fromUtf8("tab_28"));
        gridLayout_21 = new QGridLayout(tab_28);
        gridLayout_21->setSpacing(6);
        gridLayout_21->setContentsMargins(11, 11, 11, 11);
        gridLayout_21->setObjectName(QString::fromUtf8("gridLayout_21"));
        showLRF = new QCheckBox(tab_28);
        showLRF->setObjectName(QString::fromUtf8("showLRF"));

        gridLayout_21->addWidget(showLRF, 0, 0, 1, 1);

        ShowLRFlines = new QPushButton(tab_28);
        ShowLRFlines->setObjectName(QString::fromUtf8("ShowLRFlines"));

        gridLayout_21->addWidget(ShowLRFlines, 0, 1, 1, 1);

        ShowIRdata = new QCheckBox(tab_28);
        ShowIRdata->setObjectName(QString::fromUtf8("ShowIRdata"));

        gridLayout_21->addWidget(ShowIRdata, 0, 2, 1, 1);

        frame_hokuyo = new QFrame(tab_28);
        frame_hokuyo->setObjectName(QString::fromUtf8("frame_hokuyo"));
        frame_hokuyo->setFrameShape(QFrame::StyledPanel);
        frame_hokuyo->setFrameShadow(QFrame::Raised);
        label_22 = new QLabel(frame_hokuyo);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(40, 10, 91, 16));

        gridLayout_21->addWidget(frame_hokuyo, 1, 0, 1, 3);

        camera->addTab(tab_28, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        gridLayout_9 = new QGridLayout(tab_4);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        graphicsView_2 = new MyQGraphicsView(tab_4);
        graphicsView_2->setObjectName(QString::fromUtf8("graphicsView_2"));
        graphicsView_2->setEnabled(true);
        sizePolicy1.setHeightForWidth(graphicsView_2->sizePolicy().hasHeightForWidth());
        graphicsView_2->setSizePolicy(sizePolicy1);
        graphicsView_2->setMaximumSize(QSize(16777215, 16777215));
        graphicsView_2->setFocusPolicy(Qt::NoFocus);

        gridLayout_9->addWidget(graphicsView_2, 0, 0, 1, 1);

        camera->addTab(tab_4, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        gridLayout_17 = new QGridLayout(tab_6);
        gridLayout_17->setSpacing(6);
        gridLayout_17->setContentsMargins(11, 11, 11, 11);
        gridLayout_17->setObjectName(QString::fromUtf8("gridLayout_17"));
        graphicsView_3 = new MyQGraphicsView(tab_6);
        graphicsView_3->setObjectName(QString::fromUtf8("graphicsView_3"));
        sizePolicy1.setHeightForWidth(graphicsView_3->sizePolicy().hasHeightForWidth());
        graphicsView_3->setSizePolicy(sizePolicy1);
        graphicsView_3->setMaximumSize(QSize(16777215, 16777215));
        graphicsView_3->setFocusPolicy(Qt::NoFocus);

        gridLayout_17->addWidget(graphicsView_3, 0, 0, 1, 1);

        camera->addTab(tab_6, QString());

        gridLayout->addWidget(camera, 1, 6, 2, 1);

        tabWidget->addTab(tab, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        gridLayout_15 = new QGridLayout(tab_7);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QString::fromUtf8("gridLayout_15"));
        drive_2 = new QTabWidget(tab_7);
        drive_2->setObjectName(QString::fromUtf8("drive_2"));
        sizePolicy1.setHeightForWidth(drive_2->sizePolicy().hasHeightForWidth());
        drive_2->setSizePolicy(sizePolicy1);
        drive_2->setMaximumSize(QSize(320, 16777215));
        tab_27 = new QWidget();
        tab_27->setObjectName(QString::fromUtf8("tab_27"));
        gridLayout_19 = new QGridLayout(tab_27);
        gridLayout_19->setSpacing(6);
        gridLayout_19->setContentsMargins(11, 11, 11, 11);
        gridLayout_19->setObjectName(QString::fromUtf8("gridLayout_19"));
        speed_fast_2 = new QRadioButton(tab_27);
        speed_fast_2->setObjectName(QString::fromUtf8("speed_fast_2"));
        speed_fast_2->setChecked(true);

        gridLayout_19->addWidget(speed_fast_2, 0, 0, 1, 1);

        speed_moderate_2 = new QRadioButton(tab_27);
        speed_moderate_2->setObjectName(QString::fromUtf8("speed_moderate_2"));

        gridLayout_19->addWidget(speed_moderate_2, 1, 0, 1, 1);

        speed_slow_2 = new QRadioButton(tab_27);
        speed_slow_2->setObjectName(QString::fromUtf8("speed_slow_2"));

        gridLayout_19->addWidget(speed_slow_2, 2, 0, 1, 1);

        gridLayout_36 = new QGridLayout();
        gridLayout_36->setSpacing(6);
        gridLayout_36->setObjectName(QString::fromUtf8("gridLayout_36"));
        Forward_2 = new QPushButton(tab_27);
        Forward_2->setObjectName(QString::fromUtf8("Forward_2"));

        gridLayout_36->addWidget(Forward_2, 0, 1, 1, 1);

        TURNLEFT_2 = new QPushButton(tab_27);
        TURNLEFT_2->setObjectName(QString::fromUtf8("TURNLEFT_2"));

        gridLayout_36->addWidget(TURNLEFT_2, 1, 0, 1, 1);

        STOP_2 = new QPushButton(tab_27);
        STOP_2->setObjectName(QString::fromUtf8("STOP_2"));

        gridLayout_36->addWidget(STOP_2, 1, 1, 1, 1);

        TURNRIGHT_2 = new QPushButton(tab_27);
        TURNRIGHT_2->setObjectName(QString::fromUtf8("TURNRIGHT_2"));

        gridLayout_36->addWidget(TURNRIGHT_2, 1, 2, 1, 1);

        BACKWARD_2 = new QPushButton(tab_27);
        BACKWARD_2->setObjectName(QString::fromUtf8("BACKWARD_2"));

        gridLayout_36->addWidget(BACKWARD_2, 2, 1, 1, 1);


        gridLayout_19->addLayout(gridLayout_36, 3, 0, 1, 1);

        drive_2->addTab(tab_27, QString());

        gridLayout_15->addWidget(drive_2, 0, 0, 1, 1);

        frontView = new MyQGraphicsView(tab_7);
        frontView->setObjectName(QString::fromUtf8("frontView"));
        sizePolicy1.setHeightForWidth(frontView->sizePolicy().hasHeightForWidth());
        frontView->setSizePolicy(sizePolicy1);
        frontView->setMinimumSize(QSize(643, 483));
        frontView->setMaximumSize(QSize(643, 483));
        frontView->setFocusPolicy(Qt::NoFocus);
        frontView->setInteractive(false);
        frontView->setCacheMode(QGraphicsView::CacheBackground);
        frontView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
        frontView->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

        gridLayout_15->addWidget(frontView, 0, 1, 2, 1);

        mapView = new MyQGraphicsView(tab_7);
        mapView->setObjectName(QString::fromUtf8("mapView"));
        sizePolicy1.setHeightForWidth(mapView->sizePolicy().hasHeightForWidth());
        mapView->setSizePolicy(sizePolicy1);
        mapView->setMinimumSize(QSize(512, 384));
        mapView->setMaximumSize(QSize(16777215, 16777215));
        mapView->setFocusPolicy(Qt::NoFocus);
        mapView->setInteractive(false);
        mapView->setCacheMode(QGraphicsView::CacheBackground);
        mapView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
        mapView->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

        gridLayout_15->addWidget(mapView, 1, 0, 1, 1);

        tabWidget->addTab(tab_7, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_16 = new QGridLayout(tab_2);
        gridLayout_16->setSpacing(6);
        gridLayout_16->setContentsMargins(11, 11, 11, 11);
        gridLayout_16->setObjectName(QString::fromUtf8("gridLayout_16"));
        webView = new QWebView(tab_2);
        webView->setObjectName(QString::fromUtf8("webView"));
        sizePolicy1.setHeightForWidth(webView->sizePolicy().hasHeightForWidth());
        webView->setSizePolicy(sizePolicy1);
        webView->setMaximumSize(QSize(640, 16777215));
        webView->setMouseTracking(true);
        webView->setFocusPolicy(Qt::NoFocus);
        webView->setLayoutDirection(Qt::LeftToRight);
        webView->setUrl(QUrl(QString::fromUtf8("http://maps.google.com/maps/api/staticmap?size=1200x440&maptype=roadmap&sensor=false")));
        webView->setZoomFactor(1);

        gridLayout_16->addWidget(webView, 0, 0, 3, 1);

        listWidget = new QListWidget(tab_2);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        gridLayout_16->addWidget(listWidget, 2, 2, 1, 1);

        gridLayout_18 = new QGridLayout();
        gridLayout_18->setSpacing(6);
        gridLayout_18->setObjectName(QString::fromUtf8("gridLayout_18"));
        label_16 = new QLabel(tab_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_18->addWidget(label_16, 1, 1, 1, 1);

        lcdNumber_4 = new QLCDNumber(tab_2);
        lcdNumber_4->setObjectName(QString::fromUtf8("lcdNumber_4"));
        lcdNumber_4->setLineWidth(0);

        gridLayout_18->addWidget(lcdNumber_4, 1, 5, 1, 1);

        label_17 = new QLabel(tab_2);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setLayoutDirection(Qt::LeftToRight);
        label_17->setAutoFillBackground(false);
        label_17->setScaledContents(false);
        label_17->setMargin(0);
        label_17->setOpenExternalLinks(false);

        gridLayout_18->addWidget(label_17, 2, 1, 1, 1);

        lcdNumber_5 = new QLCDNumber(tab_2);
        lcdNumber_5->setObjectName(QString::fromUtf8("lcdNumber_5"));
        lcdNumber_5->setLineWidth(0);

        gridLayout_18->addWidget(lcdNumber_5, 2, 5, 1, 1);

        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_18->addWidget(label_9, 3, 1, 1, 1);

        spinBox = new QSpinBox(tab_2);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));

        gridLayout_18->addWidget(spinBox, 3, 5, 1, 1);

        label_37 = new QLabel(tab_2);
        label_37->setObjectName(QString::fromUtf8("label_37"));

        gridLayout_18->addWidget(label_37, 4, 1, 1, 1);

        comboBox = new QComboBox(tab_2);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_18->addWidget(comboBox, 4, 5, 1, 1);

        label_38 = new QLabel(tab_2);
        label_38->setObjectName(QString::fromUtf8("label_38"));

        gridLayout_18->addWidget(label_38, 5, 1, 1, 1);

        spinBox_2 = new QSpinBox(tab_2);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));

        gridLayout_18->addWidget(spinBox_2, 5, 5, 1, 1);

        start_gps = new QPushButton(tab_2);
        start_gps->setObjectName(QString::fromUtf8("start_gps"));
        sizePolicy4.setHeightForWidth(start_gps->sizePolicy().hasHeightForWidth());
        start_gps->setSizePolicy(sizePolicy4);
        start_gps->setMinimumSize(QSize(114, 0));
        start_gps->setMaximumSize(QSize(114, 16777215));

        gridLayout_18->addWidget(start_gps, 6, 1, 1, 1);

        sto_gps = new QPushButton(tab_2);
        sto_gps->setObjectName(QString::fromUtf8("sto_gps"));
        sizePolicy4.setHeightForWidth(sto_gps->sizePolicy().hasHeightForWidth());
        sto_gps->setSizePolicy(sizePolicy4);
        sto_gps->setMinimumSize(QSize(114, 0));
        sto_gps->setMaximumSize(QSize(114, 16777215));

        gridLayout_18->addWidget(sto_gps, 6, 5, 1, 1);

        load_gps = new QPushButton(tab_2);
        load_gps->setObjectName(QString::fromUtf8("load_gps"));
        sizePolicy4.setHeightForWidth(load_gps->sizePolicy().hasHeightForWidth());
        load_gps->setSizePolicy(sizePolicy4);

        gridLayout_18->addWidget(load_gps, 7, 5, 1, 1);

        save_gps = new QPushButton(tab_2);
        save_gps->setObjectName(QString::fromUtf8("save_gps"));
        sizePolicy4.setHeightForWidth(save_gps->sizePolicy().hasHeightForWidth());
        save_gps->setSizePolicy(sizePolicy4);
        save_gps->setMinimumSize(QSize(114, 0));
        save_gps->setMaximumSize(QSize(114, 16777215));

        gridLayout_18->addWidget(save_gps, 7, 1, 1, 1);


        gridLayout_16->addLayout(gridLayout_18, 0, 2, 1, 1);

        tabWidget->addTab(tab_2, QString());

        gridLayout_2->addWidget(tabWidget, 0, 1, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1458, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        tabWidget_5->setCurrentIndex(0);
        tabWidget_8->setCurrentIndex(0);
        arm->setCurrentIndex(0);
        drive->setCurrentIndex(1);
        tabWidget_2->setCurrentIndex(1);
        camera->setCurrentIndex(0);
        drive_2->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Corobot-Remote Control Interface", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "Master URL:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        Master->setToolTip(QApplication::translate("MainWindow", "Address Ip or Url of the robot", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_39->setText(QApplication::translate("MainWindow", "Host IP:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        Host->setToolTip(QApplication::translate("MainWindow", "Adress IP of this machine.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        connect->setText(QApplication::translate("MainWindow", "Connect", 0, QApplication::UnicodeUTF8));
        environmentcheckbox->setText(QApplication::translate("MainWindow", "Environment variables", 0, QApplication::UnicodeUTF8));
        tabWidget_5->setTabText(tabWidget_5->indexOf(widget), QApplication::translate("MainWindow", "Connection", 0, QApplication::UnicodeUTF8));
        tabWidget_5->setTabToolTip(tabWidget_5->indexOf(widget), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Configure the connection to the ROS server, on the robot. If the Remote control interface is on the robot, you can just check Environment variables.</p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Click connect to connect to the server. </p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This is necessary for the remote control to work.</p></body></html>", 0, QApplication::UnicodeUTF8));
        radioButton_9->setText(QApplication::translate("MainWindow", "Corobot", 0, QApplication::UnicodeUTF8));
        radioButton_10->setText(QApplication::translate("MainWindow", "Explorer", 0, QApplication::UnicodeUTF8));
        tabWidget_8->setTabText(tabWidget_8->indexOf(Select), QApplication::translate("MainWindow", "Robot", 0, QApplication::UnicodeUTF8));
        tabWidget_8->setTabToolTip(tabWidget_8->indexOf(Select), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Select the robot you are using.</p></body></html>", 0, QApplication::UnicodeUTF8));
        quitButton->setText(QApplication::translate("MainWindow", "Quit", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("MainWindow", "Configuration", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabToolTip(tabWidget->indexOf(tab_5), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Configuration tab</p></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        arm->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        arm->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        arm->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        arm->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        arm->setAccessibleDescription(QString());
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_TOOLTIP
        label_3->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Angle between the upper arm and the robot </p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("MainWindow", "Shoulder angle:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioButton_5->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the Shoulder angle in degree</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton_5->setText(QApplication::translate("MainWindow", "degree", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioButton_6->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the shoulder angle in radian</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton_6->setText(QApplication::translate("MainWindow", "radian   ", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        label_4->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Angle between the upper and the lower arm</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("MainWindow", "   Elbow angle:    ", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioButton_8->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the elbow angle in radian</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton_8->setText(QApplication::translate("MainWindow", "radian", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioButton_7->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the elbow angle in degree</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton_7->setText(QApplication::translate("MainWindow", "degree", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        frame->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This is a representation of the actual arm on the robot. You can grab the end effector with your mouse to make the amr move.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        ArmResetButton->setText(QApplication::translate("MainWindow", "Arm Reset", 0, QApplication::UnicodeUTF8));
        arm->setTabText(arm->indexOf(armPage1), QApplication::translate("MainWindow", "Arm", 0, QApplication::UnicodeUTF8));
        arm->setTabToolTip(arm->indexOf(armPage1), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">This tab enables the user to moves the robot arm thanks to his mouse. You can move the end effector on the drawing by clicking on it and moving your mouse.</span></p></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioButton->setToolTip(QApplication::translate("MainWindow", "Open the gripper", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton->setText(QApplication::translate("MainWindow", "Open", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        frame_7->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Here you can see the state of the Gripper</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_5->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButton_2->setToolTip(QApplication::translate("MainWindow", "close the gripper", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton_2->setText(QApplication::translate("MainWindow", "Close", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/gripper.jpg\" /></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        frame_8->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Representation of the orientation of the wrist. You can click with your mouse on the long red line and move your mouse to move the line to change the orientation.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_11->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/wrist.jpg\" /></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioButton_4->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the wrist angle in radian</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton_4->setText(QApplication::translate("MainWindow", "radian", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioButton_3->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the wrist angle in degree</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioButton_3->setText(QApplication::translate("MainWindow", "degree", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "Wrist angle:", 0, QApplication::UnicodeUTF8));
        label_8->setText(QString());
        arm->setTabText(arm->indexOf(tab_11), QApplication::translate("MainWindow", "Wrist and Gripper", 0, QApplication::UnicodeUTF8));
        arm->setTabToolTip(arm->indexOf(tab_11), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This tab elables the user to see and modify the orientation of the wrist and the state of the gripper(open/close).</p></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        drive->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">keyboard</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<table style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/keyboard.png\" /></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        label_2->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Joystick</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<table style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/mouse.jpg\" /></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        label_21->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">mouse</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_21->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/Joystick.png\" /></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
        drive->setTabText(drive->indexOf(drivePage1), QApplication::translate("MainWindow", "drive methods", 0, QApplication::UnicodeUTF8));
        drive->setTabToolTip(drive->indexOf(drivePage1), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display all the methods that permits to drive the robot</p></body></html>", 0, QApplication::UnicodeUTF8));
        speed_fast->setText(QApplication::translate("MainWindow", "Fast", 0, QApplication::UnicodeUTF8));
        speed_moderate->setText(QApplication::translate("MainWindow", "Moderate", 0, QApplication::UnicodeUTF8));
        speed_slow->setText(QApplication::translate("MainWindow", "Slow", 0, QApplication::UnicodeUTF8));
        Forward->setText(QApplication::translate("MainWindow", "F", 0, QApplication::UnicodeUTF8));
        TURNLEFT->setText(QApplication::translate("MainWindow", "L", 0, QApplication::UnicodeUTF8));
        STOP->setText(QApplication::translate("MainWindow", "STOP", 0, QApplication::UnicodeUTF8));
        TURNRIGHT->setText(QApplication::translate("MainWindow", "R", 0, QApplication::UnicodeUTF8));
        BACKWARD->setText(QApplication::translate("MainWindow", "B", 0, QApplication::UnicodeUTF8));
        drive->setTabText(drive->indexOf(tab_25), QApplication::translate("MainWindow", "Robot Drive", 0, QApplication::UnicodeUTF8));
        MotorControlToggle->setText(QApplication::translate("MainWindow", "Motor Control Disable", 0, QApplication::UnicodeUTF8));
        drive->setTabText(drive->indexOf(tab_3), QApplication::translate("MainWindow", "macro control", 0, QApplication::UnicodeUTF8));
        label_13->setText(QString());
        label_14->setText(QApplication::translate("MainWindow", "Voltage:", 0, QApplication::UnicodeUTF8));
        label_36->setText(QApplication::translate("MainWindow", "V", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        progressBar->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Percentage of battery left</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_12), QApplication::translate("MainWindow", "Battery", 0, QApplication::UnicodeUTF8));
        tabWidget_2->setTabToolTip(tabWidget_2->indexOf(tab_12), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display some battery information</p></body></html>", 0, QApplication::UnicodeUTF8));
        label_29->setText(QApplication::translate("MainWindow", "Acceleration", 0, QApplication::UnicodeUTF8));
        label_30->setText(QApplication::translate("MainWindow", "Angular Rate", 0, QApplication::UnicodeUTF8));
        label_31->setText(QApplication::translate("MainWindow", "Magnetic", 0, QApplication::UnicodeUTF8));
        label_27->setText(QApplication::translate("MainWindow", "IR 01", 0, QApplication::UnicodeUTF8));
        label_28->setText(QApplication::translate("MainWindow", "IR 02", 0, QApplication::UnicodeUTF8));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_14), QApplication::translate("MainWindow", "Misc Data", 0, QApplication::UnicodeUTF8));
        tabWidget_2->setTabToolTip(tabWidget_2->indexOf(tab_14), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display some sensors value</p></body></html>", 0, QApplication::UnicodeUTF8));
        label_24->setText(QApplication::translate("MainWindow", "          Bumper2", 0, QApplication::UnicodeUTF8));
        label_26->setText(QApplication::translate("MainWindow", "        Bumper3", 0, QApplication::UnicodeUTF8));
        label_40->setText(QApplication::translate("MainWindow", "         Bumper4", 0, QApplication::UnicodeUTF8));
        label_19->setText(QApplication::translate("MainWindow", "         Bumper1", 0, QApplication::UnicodeUTF8));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_10), QApplication::translate("MainWindow", "Bumpers", 0, QApplication::UnicodeUTF8));
        PanTilt_Reset->setText(QApplication::translate("MainWindow", "Reset", 0, QApplication::UnicodeUTF8));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_23), QApplication::translate("MainWindow", "Pan/Tilt Control", 0, QApplication::UnicodeUTF8));
        label_42->setText(QApplication::translate("MainWindow", "Linear speed", 0, QApplication::UnicodeUTF8));
        label_43->setText(QApplication::translate("MainWindow", "Angular speed", 0, QApplication::UnicodeUTF8));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_26), QApplication::translate("MainWindow", "Odometry Data", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        camera->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        graphicsView->setToolTip(QApplication::translate("MainWindow", "Display the robot's hd camera ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        camera->setTabText(camera->indexOf(cameraPage1), QApplication::translate("MainWindow", "Front Camera", 0, QApplication::UnicodeUTF8));
        camera->setTabToolTip(camera->indexOf(cameraPage1), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the robot's hd camera </p></body></html>", 0, QApplication::UnicodeUTF8));
        camera->setTabText(camera->indexOf(tab_16), QApplication::translate("MainWindow", "Rear Camera", 0, QApplication::UnicodeUTF8));
        showLRF->setText(QApplication::translate("MainWindow", "Show LRF", 0, QApplication::UnicodeUTF8));
        ShowLRFlines->setText(QApplication::translate("MainWindow", "Show Lines", 0, QApplication::UnicodeUTF8));
        ShowIRdata->setText(QApplication::translate("MainWindow", "Show IR data", 0, QApplication::UnicodeUTF8));
        label_22->setText(QApplication::translate("MainWindow", "1 meter", 0, QApplication::UnicodeUTF8));
        camera->setTabText(camera->indexOf(tab_28), QApplication::translate("MainWindow", "Rangefinding", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        graphicsView_2->setToolTip(QApplication::translate("MainWindow", "Display the robot's hd camera ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        camera->setTabText(camera->indexOf(tab_4), QApplication::translate("MainWindow", "Depth Sensor - RGB", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        graphicsView_3->setToolTip(QApplication::translate("MainWindow", "Display the robot's hd camera ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        camera->setTabText(camera->indexOf(tab_6), QApplication::translate("MainWindow", "Depth Sensor - Depth", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "engineering control", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabToolTip(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This tab enables the user to control the robot and to access a lot of information regarding the robot : sensors value, camera... and</p></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        drive_2->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        speed_fast_2->setText(QApplication::translate("MainWindow", "Fast", 0, QApplication::UnicodeUTF8));
        speed_moderate_2->setText(QApplication::translate("MainWindow", "Moderate", 0, QApplication::UnicodeUTF8));
        speed_slow_2->setText(QApplication::translate("MainWindow", "Slow", 0, QApplication::UnicodeUTF8));
        Forward_2->setText(QApplication::translate("MainWindow", "F", 0, QApplication::UnicodeUTF8));
        TURNLEFT_2->setText(QApplication::translate("MainWindow", "L", 0, QApplication::UnicodeUTF8));
        STOP_2->setText(QApplication::translate("MainWindow", "STOP", 0, QApplication::UnicodeUTF8));
        TURNRIGHT_2->setText(QApplication::translate("MainWindow", "R", 0, QApplication::UnicodeUTF8));
        BACKWARD_2->setText(QApplication::translate("MainWindow", "B", 0, QApplication::UnicodeUTF8));
        drive_2->setTabText(drive_2->indexOf(tab_27), QApplication::translate("MainWindow", "Robot Drive", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        frontView->setToolTip(QApplication::translate("MainWindow", "Display the robot's hd camera ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        mapView->setToolTip(QApplication::translate("MainWindow", "Display the robot's hd camera ", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QApplication::translate("MainWindow", "easy control", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        webView->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Map that show the position of the robot and the path of the selected itinerary.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        listWidget->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">List of all loaded itineraries.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        label_16->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the longitude position of the robot</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_16->setText(QApplication::translate("MainWindow", "                      Longitude: ", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lcdNumber_4->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the longitude position of the robot</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        label_17->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the latitude position of the robot</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_17->setText(QApplication::translate("MainWindow", "                         Latitude:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lcdNumber_5->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Display the latitude position of the robot</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        label_9->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Select the sampling frequency value of the gps data</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_9->setText(QApplication::translate("MainWindow", "        sampling frequency:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        spinBox->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Select the sampling frequency value of the gps data</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        label_37->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Select the map type</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_37->setText(QApplication::translate("MainWindow", "                        map type:", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "roadmap", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "satellite", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "terrain", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "hybrid", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_TOOLTIP
        comboBox->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Select the map type</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        label_38->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">choose the value of the zoom in the map. 0 is an automatic zoom.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_38->setText(QApplication::translate("MainWindow", "                             zoom:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        spinBox_2->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">choose the value of the zoom in the map. 0 is an automatic zoom.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        start_gps->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Start a new itinierary. The path will be shown on the map. It will delete the last itinerary made if unsaved.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        start_gps->setText(QApplication::translate("MainWindow", "Start Itinenary ", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        sto_gps->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Stop the running itinerary</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        sto_gps->setText(QApplication::translate("MainWindow", "Stop Itinerary", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        load_gps->setToolTip(QApplication::translate("MainWindow", "Load from the hard drive an itinerary previously saved.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        load_gps->setText(QApplication::translate("MainWindow", "Load Itinerary", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        save_gps->setToolTip(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Save on the hard drive the last itinerary stoped. </p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        save_gps->setText(QApplication::translate("MainWindow", "Save Itinerary", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "GPS", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabToolTip(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This tab shows the GPS position of the robot and show a map of it. You can also save and view the itinerary of the robot</p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
