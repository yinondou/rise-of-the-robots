# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_pantilt/src/dynctrl.c" "/home/yinon/dev/ros_corobot/build/corobot/corobot_pantilt/CMakeFiles/corobot_pantilt.dir/src/dynctrl.c.o"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_pantilt/src/utils.c" "/home/yinon/dev/ros_corobot/build/corobot/corobot_pantilt/CMakeFiles/corobot_pantilt.dir/src/utils.c.o"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_pantilt/src/v4l2uvc.c" "/home/yinon/dev/ros_corobot/build/corobot/corobot_pantilt/CMakeFiles/corobot_pantilt.dir/src/v4l2uvc.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"corobot_pantilt\""
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_pantilt/include"
  "/home/yinon/dev/ros_corobot/devel/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_diagnostics/include"
  "/opt/ros/kinetic/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_pantilt/src/pantilt.cpp" "/home/yinon/dev/ros_corobot/build/corobot/corobot_pantilt/CMakeFiles/corobot_pantilt.dir/src/pantilt.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"corobot_pantilt\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_pantilt/include"
  "/home/yinon/dev/ros_corobot/devel/include"
  "/home/yinon/dev/ros_corobot/src/corobot/corobot_diagnostics/include"
  "/opt/ros/kinetic/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
