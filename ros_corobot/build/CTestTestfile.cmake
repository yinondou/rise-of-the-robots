# CMake generated Testfile for 
# Source directory: /home/yinon/dev/ros_corobot/src
# Build directory: /home/yinon/dev/ros_corobot/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(corobot/corobot)
subdirs(corobot/corobot_launch)
subdirs(corobot/corobot_urdf)
subdirs(corobot/corobot_msgs)
subdirs(corobot/corobot_arm)
subdirs(corobot/corobot_diagnostics)
subdirs(corobot/corobot_gps)
subdirs(corobot/corobot_ssc32)
subdirs(corobot/phidget_motor)
subdirs(corobot/phidget_servo)
subdirs(corobot/phidget_stepper)
subdirs(corobot/corobot_camera)
subdirs(corobot/corobot_joystick)
subdirs(corobot/corobot_pantilt)
subdirs(corobot/corobot_gazebo)
subdirs(corobot/corobot_phidget_ik)
subdirs(corobot/corobot_state_tf)
subdirs(corobot/corobot_teleop)
subdirs(corobot/map_to_jpeg)
