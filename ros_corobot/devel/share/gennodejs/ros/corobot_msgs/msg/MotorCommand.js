// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class MotorCommand {
  constructor() {
    this.leftSpeed = 0;
    this.rightSpeed = 0;
    this.secondsDuration = 0;
    this.acceleration = 0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type MotorCommand
    // Serialize message field [leftSpeed]
    bufferInfo = _serializer.int16(obj.leftSpeed, bufferInfo);
    // Serialize message field [rightSpeed]
    bufferInfo = _serializer.int16(obj.rightSpeed, bufferInfo);
    // Serialize message field [secondsDuration]
    bufferInfo = _serializer.uint16(obj.secondsDuration, bufferInfo);
    // Serialize message field [acceleration]
    bufferInfo = _serializer.uint16(obj.acceleration, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type MotorCommand
    let tmp;
    let len;
    let data = new MotorCommand();
    // Deserialize message field [leftSpeed]
    tmp = _deserializer.int16(buffer);
    data.leftSpeed = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [rightSpeed]
    tmp = _deserializer.int16(buffer);
    data.rightSpeed = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [secondsDuration]
    tmp = _deserializer.uint16(buffer);
    data.secondsDuration = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [acceleration]
    tmp = _deserializer.uint16(buffer);
    data.acceleration = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/MotorCommand';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '451c087bf7cd0b5ee063409877e152e9';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # Bill Mania <bmania@coroware.com>
    #
    # Service used with the CoroWare CoroBot to move the robot
    # base. Originally developed for the Phidget HC Motor Controller
    #
    # leftSpeed and rightSpeed are signed integers which specify the
    # combined speed and direction to rotate the wheels on each side
    # of the robot. 0 indicates full stop. positive integers indicate
    # a forward rotation and larger integers indicate a higher
    # rotational speed.
    #
    int16 leftSpeed
    int16 rightSpeed
    #
    # when at least one of leftSpeed or rightSpeed is non-zero,
    # secondsDuration indicates for how many seconds to rotate those
    # wheels at the requested speed. when secondsDuration is zero,
    # the wheels will be rotated at the requested speed, in the
    # requested direction until another request is received.
    #
    uint16 secondsDuration
    #
    # the number of seconds over which to effect the change in
    # rotational speed from the current speed to the speed requested.
    # a value of zero indicates "as fast as possible". the greater
    # this value, the greater amount of time taken to change the
    # speed.
    uint16 acceleration
    
    `;
  }

};

module.exports = MotorCommand;
