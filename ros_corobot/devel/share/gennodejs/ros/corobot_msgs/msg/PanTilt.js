// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class PanTilt {
  constructor() {
    this.pan = 0;
    this.tilt = 0;
    this.zoom = 0;
    this.reset = false;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type PanTilt
    // Serialize message field [pan]
    bufferInfo = _serializer.int32(obj.pan, bufferInfo);
    // Serialize message field [tilt]
    bufferInfo = _serializer.int32(obj.tilt, bufferInfo);
    // Serialize message field [zoom]
    bufferInfo = _serializer.int32(obj.zoom, bufferInfo);
    // Serialize message field [reset]
    bufferInfo = _serializer.bool(obj.reset, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type PanTilt
    let tmp;
    let len;
    let data = new PanTilt();
    // Deserialize message field [pan]
    tmp = _deserializer.int32(buffer);
    data.pan = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [tilt]
    tmp = _deserializer.int32(buffer);
    data.tilt = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [zoom]
    tmp = _deserializer.int32(buffer);
    data.zoom = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [reset]
    tmp = _deserializer.bool(buffer);
    data.reset = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/PanTilt';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f2fe3951598b512c28e9462a0ece165b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 pan
    int32 tilt
    int32 zoom
    bool reset
    
    `;
  }

};

module.exports = PanTilt;
