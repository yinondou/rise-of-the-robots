// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class ServoType {
  constructor() {
    this.index = 0;
    this.type = 0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type ServoType
    // Serialize message field [index]
    bufferInfo = _serializer.int8(obj.index, bufferInfo);
    // Serialize message field [type]
    bufferInfo = _serializer.int8(obj.type, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type ServoType
    let tmp;
    let len;
    let data = new ServoType();
    // Deserialize message field [index]
    tmp = _deserializer.int8(buffer);
    data.index = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [type]
    tmp = _deserializer.int8(buffer);
    data.type = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/ServoType';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '1838b6e393c9a582039957a5d550ab79';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #
    # Morgan Cormier <mcormier@coroware.com>
    #
    # Service used to set the type of servo motor connected at the specified index on the phidget servo controller
    #
    # Index is the index of the servo motor connected to the phidget device. 
    # The maximum value of index depends on the Phidget device and how many connections it accepts
    #
    # type is the type of motor. See enum CPhidget_ServoType of the Phidget library for the list of type.
    
    int8 index
    
    int8 type
    
    `;
  }

};

module.exports = ServoType;
