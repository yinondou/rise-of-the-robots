// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class PosMsg {
  constructor() {
    this.header = new std_msgs.msg.Header();
    this.px = 0;
    this.py = 0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type PosMsg
    // Serialize message field [header]
    bufferInfo = std_msgs.msg.Header.serialize(obj.header, bufferInfo);
    // Serialize message field [px]
    bufferInfo = _serializer.int32(obj.px, bufferInfo);
    // Serialize message field [py]
    bufferInfo = _serializer.int32(obj.py, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type PosMsg
    let tmp;
    let len;
    let data = new PosMsg();
    // Deserialize message field [header]
    tmp = std_msgs.msg.Header.deserialize(buffer);
    data.header = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [px]
    tmp = _deserializer.int32(buffer);
    data.px = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [py]
    tmp = _deserializer.int32(buffer);
    data.py = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/PosMsg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd4e99badba42a86feac27c2c6f7119ef';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    int32 px
    int32 py
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

};

module.exports = PosMsg;
