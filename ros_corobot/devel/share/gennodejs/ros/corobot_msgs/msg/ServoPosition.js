// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class ServoPosition {
  constructor() {
    this.index = 0;
    this.position = 0.0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type ServoPosition
    // Serialize message field [index]
    bufferInfo = _serializer.int8(obj.index, bufferInfo);
    // Serialize message field [position]
    bufferInfo = _serializer.float32(obj.position, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type ServoPosition
    let tmp;
    let len;
    let data = new ServoPosition();
    // Deserialize message field [index]
    tmp = _deserializer.int8(buffer);
    data.index = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [position]
    tmp = _deserializer.float32(buffer);
    data.position = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/ServoPosition';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '04e7b072b763b2e3ee692e3faf7e9c94';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #
    # Morgan Cormier <mcormier@coroware.com>
    #
    # Message used to set or get the position of a servo motor connected to a Phidget servo controller.
    #
    # Index is the index of the servo motor connected to the phidget device. 
    # The maximum value of index depends on the Phidget device and how many connections it accepts
    
    int8 index
    
    # position is the angle in degree set for the motor selected with the index value. 
    # position has a minimum and maximum value possible, that can be retrieve through two services
    float32 position
    
    `;
  }

};

module.exports = ServoPosition;
