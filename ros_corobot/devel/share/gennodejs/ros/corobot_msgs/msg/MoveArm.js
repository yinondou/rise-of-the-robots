// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class MoveArm {
  constructor() {
    this.index = 0;
    this.position = 0.0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type MoveArm
    // Serialize message field [index]
    bufferInfo = _serializer.int8(obj.index, bufferInfo);
    // Serialize message field [position]
    bufferInfo = _serializer.float32(obj.position, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type MoveArm
    let tmp;
    let len;
    let data = new MoveArm();
    // Deserialize message field [index]
    tmp = _deserializer.int8(buffer);
    data.index = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [position]
    tmp = _deserializer.float32(buffer);
    data.position = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/MoveArm';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '0880c120739bb732d35453e85c86d543';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #
    # Morgan Cormier <mcormier@coroware.com>
    #
    # Message used to set or get the position of a servo motor connected to a Phidget servo controller.
    #
    
    ####### Possible Indexes #########
    uint8 BASE_ROTATION = 0
    uint8 SHOULDER = 1
    uint8 ELBOW = 2
    uint8 WRIST_FLEX = 3
    uint8 WRIST_ROTATION = 3
    uint8 GRIPPER = 4
    uint8 LEFT_WHEEL = 5 # only used in case the left wheel is a stepper motor
    uint8 RIGHT_WHEEL = 6 # only used in case the right wheel is a stepper motor
    # Index is the index from the list of possible index. 
    int8 index
    
    # position is the angle in degree set for the servo motor selected with index
    float32 position
    
    `;
  }

};

// Constants for message
MoveArm.Constants = {
  BASE_ROTATION: 0,
  SHOULDER: 1,
  ELBOW: 2,
  WRIST_FLEX: 3,
  WRIST_ROTATION: 3,
  GRIPPER: 4,
  LEFT_WHEEL: 5,
  RIGHT_WHEEL: 6,
}

module.exports = MoveArm;
