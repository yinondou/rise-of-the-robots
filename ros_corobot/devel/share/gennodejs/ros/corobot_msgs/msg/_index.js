
"use strict";

let MoveArm = require('./MoveArm.js');
let PanTilt = require('./PanTilt.js');
let state = require('./state.js');
let SensorMsg = require('./SensorMsg.js');
let PowerMsg = require('./PowerMsg.js');
let ServoType = require('./ServoType.js');
let GPSStatus = require('./GPSStatus.js');
let MotorCommand = require('./MotorCommand.js');
let videomode = require('./videomode.js');
let ServoPosition = require('./ServoPosition.js');
let GPSFix = require('./GPSFix.js');
let PosMsg = require('./PosMsg.js');
let takepic = require('./takepic.js');

module.exports = {
  MoveArm: MoveArm,
  PanTilt: PanTilt,
  state: state,
  SensorMsg: SensorMsg,
  PowerMsg: PowerMsg,
  ServoType: ServoType,
  GPSStatus: GPSStatus,
  MotorCommand: MotorCommand,
  videomode: videomode,
  ServoPosition: ServoPosition,
  GPSFix: GPSFix,
  PosMsg: PosMsg,
  takepic: takepic,
};
