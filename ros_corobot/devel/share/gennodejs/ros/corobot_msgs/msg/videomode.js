// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class videomode {
  constructor() {
    this.header = new std_msgs.msg.Header();
    this.immediately = false;
    this.width = 0;
    this.height = 0;
    this.fps = 0;
    this.auto_exposure = false;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type videomode
    // Serialize message field [header]
    bufferInfo = std_msgs.msg.Header.serialize(obj.header, bufferInfo);
    // Serialize message field [immediately]
    bufferInfo = _serializer.bool(obj.immediately, bufferInfo);
    // Serialize message field [width]
    bufferInfo = _serializer.uint16(obj.width, bufferInfo);
    // Serialize message field [height]
    bufferInfo = _serializer.uint16(obj.height, bufferInfo);
    // Serialize message field [fps]
    bufferInfo = _serializer.uint8(obj.fps, bufferInfo);
    // Serialize message field [auto_exposure]
    bufferInfo = _serializer.bool(obj.auto_exposure, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type videomode
    let tmp;
    let len;
    let data = new videomode();
    // Deserialize message field [header]
    tmp = std_msgs.msg.Header.deserialize(buffer);
    data.header = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [immediately]
    tmp = _deserializer.bool(buffer);
    data.immediately = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [width]
    tmp = _deserializer.uint16(buffer);
    data.width = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [height]
    tmp = _deserializer.uint16(buffer);
    data.height = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [fps]
    tmp = _deserializer.uint8(buffer);
    data.fps = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [auto_exposure]
    tmp = _deserializer.bool(buffer);
    data.auto_exposure = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/videomode';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5bf6aeb9e3ba1dd691c5c50529d7e824';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header  header
    bool    immediately
    uint16  width
    uint16  height
    uint8   fps
    bool    auto_exposure
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

};

module.exports = videomode;
