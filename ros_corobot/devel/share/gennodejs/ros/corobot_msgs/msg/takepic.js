// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class takepic {
  constructor() {
    this.camera_index = 0;
    this.note = 0;
    this.take = false;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type takepic
    // Serialize message field [camera_index]
    bufferInfo = _serializer.int32(obj.camera_index, bufferInfo);
    // Serialize message field [note]
    bufferInfo = _serializer.int32(obj.note, bufferInfo);
    // Serialize message field [take]
    bufferInfo = _serializer.bool(obj.take, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type takepic
    let tmp;
    let len;
    let data = new takepic();
    // Deserialize message field [camera_index]
    tmp = _deserializer.int32(buffer);
    data.camera_index = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [note]
    tmp = _deserializer.int32(buffer);
    data.note = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [take]
    tmp = _deserializer.bool(buffer);
    data.take = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/takepic';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '802817317afdd58e6e9fd4be92bf0d57';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 camera_index
    int32 note
    bool take
    
    `;
  }

};

module.exports = takepic;
