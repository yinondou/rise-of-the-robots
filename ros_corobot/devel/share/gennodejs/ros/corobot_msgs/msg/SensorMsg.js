// Auto-generated. Do not edit!

// (in-package corobot_msgs.msg)


"use strict";

let _serializer = require('../base_serialize.js');
let _deserializer = require('../base_deserialize.js');
let _finder = require('../find.js');

//-----------------------------------------------------------

class SensorMsg {
  constructor() {
    this.type = 0;
    this.index = 0;
    this.value = 0.0;
  }

  static serialize(obj, bufferInfo) {
    // Serializes a message object of type SensorMsg
    // Serialize message field [type]
    bufferInfo = _serializer.int8(obj.type, bufferInfo);
    // Serialize message field [index]
    bufferInfo = _serializer.int8(obj.index, bufferInfo);
    // Serialize message field [value]
    bufferInfo = _serializer.float64(obj.value, bufferInfo);
    return bufferInfo;
  }

  static deserialize(buffer) {
    //deserializes a message object of type SensorMsg
    let tmp;
    let len;
    let data = new SensorMsg();
    // Deserialize message field [type]
    tmp = _deserializer.int8(buffer);
    data.type = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [index]
    tmp = _deserializer.int8(buffer);
    data.index = tmp.data;
    buffer = tmp.buffer;
    // Deserialize message field [value]
    tmp = _deserializer.float64(buffer);
    data.value = tmp.data;
    buffer = tmp.buffer;
    return {
      data: data,
      buffer: buffer
    }
  }

  static datatype() {
    // Returns string type for a message object
    return 'corobot_msgs/SensorMsg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f2f2ad707f7136c4cb90a14479ae8687';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # This message can be used to send some simple sensor data that require only one value. The type of the sensor can be unkown.
    
    #define sensor type
    int8 BUMPER = 0
    int8 OTHER = 1
    uint8 ULTRASOUND=2
    uint8 INFRARED_FRONT=3
    uint8 INFRARED_REAR=4
    
    # type of sensor, defined above
    int8 type
    
    # index of the sensor, for example index on the Phidget Interface Kit. This can be used to filter data when the sensor type is unknown, but the user know the data type
    int8 index
    
    # Value of the sensor, it can be digital in which case the value will be 0.0 or 1.0, or analog in Volts. If the type of the sensor is known, for example ultrasound or infrared, the value is in the corresponding standard format, for example meters. 
    float64 value
    
    `;
  }

};

// Constants for message
SensorMsg.Constants = {
  BUMPER: 0,
  OTHER: 1,
  ULTRASOUND: 2,
  INFRARED_FRONT: 3,
  INFRARED_REAR: 4,
}

module.exports = SensorMsg;
