# generated from catkin/cmake/template/pkgConfig.cmake.in

# append elements to a list and remove existing duplicates from the list
# copied from catkin/cmake/list_append_deduplicate.cmake to keep pkgConfig
# self contained
macro(_list_append_deduplicate listname)
  if(NOT "${ARGN}" STREQUAL "")
    if(${listname})
      list(REMOVE_ITEM ${listname} ${ARGN})
    endif()
    list(APPEND ${listname} ${ARGN})
  endif()
endmacro()

# append elements to a list if they are not already in the list
# copied from catkin/cmake/list_append_unique.cmake to keep pkgConfig
# self contained
macro(_list_append_unique listname)
  foreach(_item ${ARGN})
    list(FIND ${listname} ${_item} _index)
    if(_index EQUAL -1)
      list(APPEND ${listname} ${_item})
    endif()
  endforeach()
endmacro()

# pack a list of libraries with optional build configuration keywords
# copied from catkin/cmake/catkin_libraries.cmake to keep pkgConfig
# self contained
macro(_pack_libraries_with_build_configuration VAR)
  set(${VAR} "")
  set(_argn ${ARGN})
  list(LENGTH _argn _count)
  set(_index 0)
  while(${_index} LESS ${_count})
    list(GET _argn ${_index} lib)
    if("${lib}" MATCHES "^(debug|optimized|general)$")
      math(EXPR _index "${_index} + 1")
      if(${_index} EQUAL ${_count})
        message(FATAL_ERROR "_pack_libraries_with_build_configuration() the list of libraries '${ARGN}' ends with '${lib}' which is a build configuration keyword and must be followed by a library")
      endif()
      list(GET _argn ${_index} library)
      list(APPEND ${VAR} "${lib}${CATKIN_BUILD_CONFIGURATION_KEYWORD_SEPARATOR}${library}")
    else()
      list(APPEND ${VAR} "${lib}")
    endif()
    math(EXPR _index "${_index} + 1")
  endwhile()
endmacro()

# unpack a list of libraries with optional build configuration keyword prefixes
# copied from catkin/cmake/catkin_libraries.cmake to keep pkgConfig
# self contained
macro(_unpack_libraries_with_build_configuration VAR)
  set(${VAR} "")
  foreach(lib ${ARGN})
    string(REGEX REPLACE "^(debug|optimized|general)${CATKIN_BUILD_CONFIGURATION_KEYWORD_SEPARATOR}(.+)$" "\\1;\\2" lib "${lib}")
    list(APPEND ${VAR} "${lib}")
  endforeach()
endmacro()


if(map_to_jpeg_CONFIG_INCLUDED)
  return()
endif()
set(map_to_jpeg_CONFIG_INCLUDED TRUE)

# set variables for source/devel/install prefixes
if("TRUE" STREQUAL "TRUE")
  set(map_to_jpeg_SOURCE_PREFIX /home/yinon/dev/ros_corobot/src/corobot/map_to_jpeg)
  set(map_to_jpeg_DEVEL_PREFIX /home/yinon/dev/ros_corobot/devel)
  set(map_to_jpeg_INSTALL_PREFIX "")
  set(map_to_jpeg_PREFIX ${map_to_jpeg_DEVEL_PREFIX})
else()
  set(map_to_jpeg_SOURCE_PREFIX "")
  set(map_to_jpeg_DEVEL_PREFIX "")
  set(map_to_jpeg_INSTALL_PREFIX /home/yinon/dev/ros_corobot/install)
  set(map_to_jpeg_PREFIX ${map_to_jpeg_INSTALL_PREFIX})
endif()

# warn when using a deprecated package
if(NOT "" STREQUAL "")
  set(_msg "WARNING: package 'map_to_jpeg' is deprecated")
  # append custom deprecation text if available
  if(NOT "" STREQUAL "TRUE")
    set(_msg "${_msg} ()")
  endif()
  message("${_msg}")
endif()

# flag project as catkin-based to distinguish if a find_package()-ed project is a catkin project
set(map_to_jpeg_FOUND_CATKIN_PROJECT TRUE)

if(NOT "/home/yinon/dev/ros_corobot/src/corobot/map_to_jpeg/include;/usr/include/eigen3;/opt/ros/kinetic/include/opencv-3.1.0-dev/opencv;/opt/ros/kinetic/include/opencv-3.1.0-dev " STREQUAL " ")
  set(map_to_jpeg_INCLUDE_DIRS "")
  set(_include_dirs "/home/yinon/dev/ros_corobot/src/corobot/map_to_jpeg/include;/usr/include/eigen3;/opt/ros/kinetic/include/opencv-3.1.0-dev/opencv;/opt/ros/kinetic/include/opencv-3.1.0-dev")
  foreach(idir ${_include_dirs})
    if(IS_ABSOLUTE ${idir} AND IS_DIRECTORY ${idir})
      set(include ${idir})
    elseif("${idir} " STREQUAL "include ")
      get_filename_component(include "${map_to_jpeg_DIR}/../../../include" ABSOLUTE)
      if(NOT IS_DIRECTORY ${include})
        message(FATAL_ERROR "Project 'map_to_jpeg' specifies '${idir}' as an include dir, which is not found.  It does not exist in '${include}'.  Ask the maintainer 'Morgan Cormier <mcormier@coroware.com>' to fix it.")
      endif()
    else()
      message(FATAL_ERROR "Project 'map_to_jpeg' specifies '${idir}' as an include dir, which is not found.  It does neither exist as an absolute directory nor in '/home/yinon/dev/ros_corobot/src/corobot/map_to_jpeg/${idir}'.  Ask the maintainer 'Morgan Cormier <mcormier@coroware.com>' to fix it.")
    endif()
    _list_append_unique(map_to_jpeg_INCLUDE_DIRS ${include})
  endforeach()
endif()

set(libraries "map_to_jpeg;/opt/ros/kinetic/lib/libopencv_xphoto3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_xobjdetect3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_ximgproc3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_xfeatures2d3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_tracking3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_text3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_surface_matching3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_structured_light3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_stereo3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_saliency3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_rgbd3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_reg3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_plot3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_optflow3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_line_descriptor3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_hdf3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_fuzzy3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_face3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_dpm3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_dnn3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_datasets3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_cvv3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_ccalib3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_bioinspired3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_bgsegm3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_aruco3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_viz3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_videostab3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_videoio3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_video3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_superres3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_stitching3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_shape3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_photo3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_objdetect3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_ml3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_imgproc3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_imgcodecs3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_highgui3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_flann3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_features2d3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_core3.so.3.1.0;/opt/ros/kinetic/lib/libopencv_calib3d3.so.3.1.0")
foreach(library ${libraries})
  # keep build configuration keywords, target names and absolute libraries as-is
  if("${library}" MATCHES "^(debug|optimized|general)$")
    list(APPEND map_to_jpeg_LIBRARIES ${library})
  elseif(TARGET ${library})
    list(APPEND map_to_jpeg_LIBRARIES ${library})
  elseif(IS_ABSOLUTE ${library})
    list(APPEND map_to_jpeg_LIBRARIES ${library})
  else()
    set(lib_path "")
    set(lib "${library}-NOTFOUND")
    # since the path where the library is found is returned we have to iterate over the paths manually
    foreach(path /home/yinon/dev/ros_corobot/devel/lib;/home/yinon/dev/ros_corobot/devel/lib;/opt/ros/kinetic/lib)
      find_library(lib ${library}
        PATHS ${path}
        NO_DEFAULT_PATH NO_CMAKE_FIND_ROOT_PATH)
      if(lib)
        set(lib_path ${path})
        break()
      endif()
    endforeach()
    if(lib)
      _list_append_unique(map_to_jpeg_LIBRARY_DIRS ${lib_path})
      list(APPEND map_to_jpeg_LIBRARIES ${lib})
    else()
      # as a fall back for non-catkin libraries try to search globally
      find_library(lib ${library})
      if(NOT lib)
        message(FATAL_ERROR "Project '${PROJECT_NAME}' tried to find library '${library}'.  The library is neither a target nor built/installed properly.  Did you compile project 'map_to_jpeg'?  Did you find_package() it before the subdirectory containing its code is included?")
      endif()
      list(APPEND map_to_jpeg_LIBRARIES ${lib})
    endif()
  endif()
endforeach()

set(map_to_jpeg_EXPORTED_TARGETS "")
# create dummy targets for exported code generation targets to make life of users easier
foreach(t ${map_to_jpeg_EXPORTED_TARGETS})
  if(NOT TARGET ${t})
    add_custom_target(${t})
  endif()
endforeach()

set(depends "roscpp")
foreach(depend ${depends})
  string(REPLACE " " ";" depend_list ${depend})
  # the package name of the dependency must be kept in a unique variable so that it is not overwritten in recursive calls
  list(GET depend_list 0 map_to_jpeg_dep)
  list(LENGTH depend_list count)
  if(${count} EQUAL 1)
    # simple dependencies must only be find_package()-ed once
    if(NOT ${map_to_jpeg_dep}_FOUND)
      find_package(${map_to_jpeg_dep} REQUIRED NO_MODULE)
    endif()
  else()
    # dependencies with components must be find_package()-ed again
    list(REMOVE_AT depend_list 0)
    find_package(${map_to_jpeg_dep} REQUIRED NO_MODULE ${depend_list})
  endif()
  _list_append_unique(map_to_jpeg_INCLUDE_DIRS ${${map_to_jpeg_dep}_INCLUDE_DIRS})

  # merge build configuration keywords with library names to correctly deduplicate
  _pack_libraries_with_build_configuration(map_to_jpeg_LIBRARIES ${map_to_jpeg_LIBRARIES})
  _pack_libraries_with_build_configuration(_libraries ${${map_to_jpeg_dep}_LIBRARIES})
  _list_append_deduplicate(map_to_jpeg_LIBRARIES ${_libraries})
  # undo build configuration keyword merging after deduplication
  _unpack_libraries_with_build_configuration(map_to_jpeg_LIBRARIES ${map_to_jpeg_LIBRARIES})

  _list_append_unique(map_to_jpeg_LIBRARY_DIRS ${${map_to_jpeg_dep}_LIBRARY_DIRS})
  list(APPEND map_to_jpeg_EXPORTED_TARGETS ${${map_to_jpeg_dep}_EXPORTED_TARGETS})
endforeach()

set(pkg_cfg_extras "")
foreach(extra ${pkg_cfg_extras})
  if(NOT IS_ABSOLUTE ${extra})
    set(extra ${map_to_jpeg_DIR}/${extra})
  endif()
  include(${extra})
endforeach()
