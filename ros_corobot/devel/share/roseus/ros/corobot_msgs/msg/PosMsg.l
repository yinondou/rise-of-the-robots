;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::PosMsg)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'PosMsg (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::POSMSG")
  (make-package "COROBOT_MSGS::POSMSG"))

(in-package "ROS")
;;//! \htmlinclude PosMsg.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass corobot_msgs::PosMsg
  :super ros::object
  :slots (_header _px _py ))

(defmethod corobot_msgs::PosMsg
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:px __px) 0)
    ((:py __py) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _px (round __px))
   (setq _py (round __py))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:px
   (&optional __px)
   (if __px (setq _px __px)) _px)
  (:py
   (&optional __py)
   (if __py (setq _py __py)) _py)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; int32 _px
    4
    ;; int32 _py
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; int32 _px
       (write-long _px s)
     ;; int32 _py
       (write-long _py s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; int32 _px
     (setq _px (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _py
     (setq _py (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get corobot_msgs::PosMsg :md5sum-) "d4e99badba42a86feac27c2c6f7119ef")
(setf (get corobot_msgs::PosMsg :datatype-) "corobot_msgs/PosMsg")
(setf (get corobot_msgs::PosMsg :definition-)
      "Header header
int32 px
int32 py

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :corobot_msgs/PosMsg "d4e99badba42a86feac27c2c6f7119ef")


