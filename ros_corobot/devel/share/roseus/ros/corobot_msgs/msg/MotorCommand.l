;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::MotorCommand)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'MotorCommand (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::MOTORCOMMAND")
  (make-package "COROBOT_MSGS::MOTORCOMMAND"))

(in-package "ROS")
;;//! \htmlinclude MotorCommand.msg.html


(defclass corobot_msgs::MotorCommand
  :super ros::object
  :slots (_leftSpeed _rightSpeed _secondsDuration _acceleration ))

(defmethod corobot_msgs::MotorCommand
  (:init
   (&key
    ((:leftSpeed __leftSpeed) 0)
    ((:rightSpeed __rightSpeed) 0)
    ((:secondsDuration __secondsDuration) 0)
    ((:acceleration __acceleration) 0)
    )
   (send-super :init)
   (setq _leftSpeed (round __leftSpeed))
   (setq _rightSpeed (round __rightSpeed))
   (setq _secondsDuration (round __secondsDuration))
   (setq _acceleration (round __acceleration))
   self)
  (:leftSpeed
   (&optional __leftSpeed)
   (if __leftSpeed (setq _leftSpeed __leftSpeed)) _leftSpeed)
  (:rightSpeed
   (&optional __rightSpeed)
   (if __rightSpeed (setq _rightSpeed __rightSpeed)) _rightSpeed)
  (:secondsDuration
   (&optional __secondsDuration)
   (if __secondsDuration (setq _secondsDuration __secondsDuration)) _secondsDuration)
  (:acceleration
   (&optional __acceleration)
   (if __acceleration (setq _acceleration __acceleration)) _acceleration)
  (:serialization-length
   ()
   (+
    ;; int16 _leftSpeed
    2
    ;; int16 _rightSpeed
    2
    ;; uint16 _secondsDuration
    2
    ;; uint16 _acceleration
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int16 _leftSpeed
       (write-word _leftSpeed s)
     ;; int16 _rightSpeed
       (write-word _rightSpeed s)
     ;; uint16 _secondsDuration
       (write-word _secondsDuration s)
     ;; uint16 _acceleration
       (write-word _acceleration s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int16 _leftSpeed
     (setq _leftSpeed (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _rightSpeed
     (setq _rightSpeed (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _secondsDuration
     (setq _secondsDuration (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _acceleration
     (setq _acceleration (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get corobot_msgs::MotorCommand :md5sum-) "451c087bf7cd0b5ee063409877e152e9")
(setf (get corobot_msgs::MotorCommand :datatype-) "corobot_msgs/MotorCommand")
(setf (get corobot_msgs::MotorCommand :definition-)
      "# Bill Mania <bmania@coroware.com>
#
# Service used with the CoroWare CoroBot to move the robot
# base. Originally developed for the Phidget HC Motor Controller
#
# leftSpeed and rightSpeed are signed integers which specify the
# combined speed and direction to rotate the wheels on each side
# of the robot. 0 indicates full stop. positive integers indicate
# a forward rotation and larger integers indicate a higher
# rotational speed.
#
int16 leftSpeed
int16 rightSpeed
#
# when at least one of leftSpeed or rightSpeed is non-zero,
# secondsDuration indicates for how many seconds to rotate those
# wheels at the requested speed. when secondsDuration is zero,
# the wheels will be rotated at the requested speed, in the
# requested direction until another request is received.
#
uint16 secondsDuration
#
# the number of seconds over which to effect the change in
# rotational speed from the current speed to the speed requested.
# a value of zero indicates \"as fast as possible\". the greater
# this value, the greater amount of time taken to change the
# speed.
uint16 acceleration

")



(provide :corobot_msgs/MotorCommand "451c087bf7cd0b5ee063409877e152e9")


