;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::MoveArm)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'MoveArm (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::MOVEARM")
  (make-package "COROBOT_MSGS::MOVEARM"))

(in-package "ROS")
;;//! \htmlinclude MoveArm.msg.html


(intern "*BASE_ROTATION*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*BASE_ROTATION* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*BASE_ROTATION* 0)
(intern "*SHOULDER*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*SHOULDER* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*SHOULDER* 1)
(intern "*ELBOW*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*ELBOW* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*ELBOW* 2)
(intern "*WRIST_FLEX*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*WRIST_FLEX* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*WRIST_FLEX* 3)
(intern "*WRIST_ROTATION*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*WRIST_ROTATION* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*WRIST_ROTATION* 3)
(intern "*GRIPPER*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*GRIPPER* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*GRIPPER* 4)
(intern "*LEFT_WHEEL*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*LEFT_WHEEL* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*LEFT_WHEEL* 5)
(intern "*RIGHT_WHEEL*" (find-package "COROBOT_MSGS::MOVEARM"))
(shadow '*RIGHT_WHEEL* (find-package "COROBOT_MSGS::MOVEARM"))
(defconstant corobot_msgs::MoveArm::*RIGHT_WHEEL* 6)
(defclass corobot_msgs::MoveArm
  :super ros::object
  :slots (_index _position ))

(defmethod corobot_msgs::MoveArm
  (:init
   (&key
    ((:index __index) 0)
    ((:position __position) 0.0)
    )
   (send-super :init)
   (setq _index (round __index))
   (setq _position (float __position))
   self)
  (:index
   (&optional __index)
   (if __index (setq _index __index)) _index)
  (:position
   (&optional __position)
   (if __position (setq _position __position)) _position)
  (:serialization-length
   ()
   (+
    ;; int8 _index
    1
    ;; float32 _position
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8 _index
       (write-byte _index s)
     ;; float32 _position
       (sys::poke _position (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8 _index
     (setq _index (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _index 127) (setq _index (- _index 256)))
   ;; float32 _position
     (setq _position (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get corobot_msgs::MoveArm :md5sum-) "0880c120739bb732d35453e85c86d543")
(setf (get corobot_msgs::MoveArm :datatype-) "corobot_msgs/MoveArm")
(setf (get corobot_msgs::MoveArm :definition-)
      "#
# Morgan Cormier <mcormier@coroware.com>
#
# Message used to set or get the position of a servo motor connected to a Phidget servo controller.
#

####### Possible Indexes #########
uint8 BASE_ROTATION = 0
uint8 SHOULDER = 1
uint8 ELBOW = 2
uint8 WRIST_FLEX = 3
uint8 WRIST_ROTATION = 3
uint8 GRIPPER = 4
uint8 LEFT_WHEEL = 5 # only used in case the left wheel is a stepper motor
uint8 RIGHT_WHEEL = 6 # only used in case the right wheel is a stepper motor
# Index is the index from the list of possible index. 
int8 index

# position is the angle in degree set for the servo motor selected with index
float32 position

")



(provide :corobot_msgs/MoveArm "0880c120739bb732d35453e85c86d543")


