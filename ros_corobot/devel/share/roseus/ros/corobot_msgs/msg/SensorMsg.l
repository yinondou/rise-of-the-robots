;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::SensorMsg)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'SensorMsg (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::SENSORMSG")
  (make-package "COROBOT_MSGS::SENSORMSG"))

(in-package "ROS")
;;//! \htmlinclude SensorMsg.msg.html


(intern "*BUMPER*" (find-package "COROBOT_MSGS::SENSORMSG"))
(shadow '*BUMPER* (find-package "COROBOT_MSGS::SENSORMSG"))
(defconstant corobot_msgs::SensorMsg::*BUMPER* 0)
(intern "*OTHER*" (find-package "COROBOT_MSGS::SENSORMSG"))
(shadow '*OTHER* (find-package "COROBOT_MSGS::SENSORMSG"))
(defconstant corobot_msgs::SensorMsg::*OTHER* 1)
(intern "*ULTRASOUND*" (find-package "COROBOT_MSGS::SENSORMSG"))
(shadow '*ULTRASOUND* (find-package "COROBOT_MSGS::SENSORMSG"))
(defconstant corobot_msgs::SensorMsg::*ULTRASOUND* 2)
(intern "*INFRARED_FRONT*" (find-package "COROBOT_MSGS::SENSORMSG"))
(shadow '*INFRARED_FRONT* (find-package "COROBOT_MSGS::SENSORMSG"))
(defconstant corobot_msgs::SensorMsg::*INFRARED_FRONT* 3)
(intern "*INFRARED_REAR*" (find-package "COROBOT_MSGS::SENSORMSG"))
(shadow '*INFRARED_REAR* (find-package "COROBOT_MSGS::SENSORMSG"))
(defconstant corobot_msgs::SensorMsg::*INFRARED_REAR* 4)
(defclass corobot_msgs::SensorMsg
  :super ros::object
  :slots (_type _index _value ))

(defmethod corobot_msgs::SensorMsg
  (:init
   (&key
    ((:type __type) 0)
    ((:index __index) 0)
    ((:value __value) 0.0)
    )
   (send-super :init)
   (setq _type (round __type))
   (setq _index (round __index))
   (setq _value (float __value))
   self)
  (:type
   (&optional __type)
   (if __type (setq _type __type)) _type)
  (:index
   (&optional __index)
   (if __index (setq _index __index)) _index)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; int8 _type
    1
    ;; int8 _index
    1
    ;; float64 _value
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8 _type
       (write-byte _type s)
     ;; int8 _index
       (write-byte _index s)
     ;; float64 _value
       (sys::poke _value (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8 _type
     (setq _type (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _type 127) (setq _type (- _type 256)))
   ;; int8 _index
     (setq _index (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _index 127) (setq _index (- _index 256)))
   ;; float64 _value
     (setq _value (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get corobot_msgs::SensorMsg :md5sum-) "f2f2ad707f7136c4cb90a14479ae8687")
(setf (get corobot_msgs::SensorMsg :datatype-) "corobot_msgs/SensorMsg")
(setf (get corobot_msgs::SensorMsg :definition-)
      "# This message can be used to send some simple sensor data that require only one value. The type of the sensor can be unkown.

#define sensor type
int8 BUMPER = 0
int8 OTHER = 1
uint8 ULTRASOUND=2
uint8 INFRARED_FRONT=3
uint8 INFRARED_REAR=4

# type of sensor, defined above
int8 type

# index of the sensor, for example index on the Phidget Interface Kit. This can be used to filter data when the sensor type is unknown, but the user know the data type
int8 index

# Value of the sensor, it can be digital in which case the value will be 0.0 or 1.0, or analog in Volts. If the type of the sensor is known, for example ultrasound or infrared, the value is in the corresponding standard format, for example meters. 
float64 value

")



(provide :corobot_msgs/SensorMsg "f2f2ad707f7136c4cb90a14479ae8687")


