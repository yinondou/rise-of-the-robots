;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::takepic)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'takepic (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::TAKEPIC")
  (make-package "COROBOT_MSGS::TAKEPIC"))

(in-package "ROS")
;;//! \htmlinclude takepic.msg.html


(defclass corobot_msgs::takepic
  :super ros::object
  :slots (_camera_index _note _take ))

(defmethod corobot_msgs::takepic
  (:init
   (&key
    ((:camera_index __camera_index) 0)
    ((:note __note) 0)
    ((:take __take) nil)
    )
   (send-super :init)
   (setq _camera_index (round __camera_index))
   (setq _note (round __note))
   (setq _take __take)
   self)
  (:camera_index
   (&optional __camera_index)
   (if __camera_index (setq _camera_index __camera_index)) _camera_index)
  (:note
   (&optional __note)
   (if __note (setq _note __note)) _note)
  (:take
   (&optional __take)
   (if __take (setq _take __take)) _take)
  (:serialization-length
   ()
   (+
    ;; int32 _camera_index
    4
    ;; int32 _note
    4
    ;; bool _take
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _camera_index
       (write-long _camera_index s)
     ;; int32 _note
       (write-long _note s)
     ;; bool _take
       (if _take (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _camera_index
     (setq _camera_index (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _note
     (setq _note (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; bool _take
     (setq _take (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get corobot_msgs::takepic :md5sum-) "802817317afdd58e6e9fd4be92bf0d57")
(setf (get corobot_msgs::takepic :datatype-) "corobot_msgs/takepic")
(setf (get corobot_msgs::takepic :definition-)
      "int32 camera_index
int32 note
bool take

")



(provide :corobot_msgs/takepic "802817317afdd58e6e9fd4be92bf0d57")


