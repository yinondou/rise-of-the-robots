;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::videomode)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'videomode (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::VIDEOMODE")
  (make-package "COROBOT_MSGS::VIDEOMODE"))

(in-package "ROS")
;;//! \htmlinclude videomode.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass corobot_msgs::videomode
  :super ros::object
  :slots (_header _immediately _width _height _fps _auto_exposure ))

(defmethod corobot_msgs::videomode
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:immediately __immediately) nil)
    ((:width __width) 0)
    ((:height __height) 0)
    ((:fps __fps) 0)
    ((:auto_exposure __auto_exposure) nil)
    )
   (send-super :init)
   (setq _header __header)
   (setq _immediately __immediately)
   (setq _width (round __width))
   (setq _height (round __height))
   (setq _fps (round __fps))
   (setq _auto_exposure __auto_exposure)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:immediately
   (&optional __immediately)
   (if __immediately (setq _immediately __immediately)) _immediately)
  (:width
   (&optional __width)
   (if __width (setq _width __width)) _width)
  (:height
   (&optional __height)
   (if __height (setq _height __height)) _height)
  (:fps
   (&optional __fps)
   (if __fps (setq _fps __fps)) _fps)
  (:auto_exposure
   (&optional __auto_exposure)
   (if __auto_exposure (setq _auto_exposure __auto_exposure)) _auto_exposure)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _immediately
    1
    ;; uint16 _width
    2
    ;; uint16 _height
    2
    ;; uint8 _fps
    1
    ;; bool _auto_exposure
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _immediately
       (if _immediately (write-byte -1 s) (write-byte 0 s))
     ;; uint16 _width
       (write-word _width s)
     ;; uint16 _height
       (write-word _height s)
     ;; uint8 _fps
       (write-byte _fps s)
     ;; bool _auto_exposure
       (if _auto_exposure (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _immediately
     (setq _immediately (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint16 _width
     (setq _width (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _height
     (setq _height (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint8 _fps
     (setq _fps (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; bool _auto_exposure
     (setq _auto_exposure (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get corobot_msgs::videomode :md5sum-) "5bf6aeb9e3ba1dd691c5c50529d7e824")
(setf (get corobot_msgs::videomode :datatype-) "corobot_msgs/videomode")
(setf (get corobot_msgs::videomode :definition-)
      "Header  header
bool    immediately
uint16  width
uint16  height
uint8   fps
bool    auto_exposure

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :corobot_msgs/videomode "5bf6aeb9e3ba1dd691c5c50529d7e824")


