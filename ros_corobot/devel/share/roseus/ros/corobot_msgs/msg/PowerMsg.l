;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::PowerMsg)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'PowerMsg (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::POWERMSG")
  (make-package "COROBOT_MSGS::POWERMSG"))

(in-package "ROS")
;;//! \htmlinclude PowerMsg.msg.html


(defclass corobot_msgs::PowerMsg
  :super ros::object
  :slots (_volts _min_volt _max_volt ))

(defmethod corobot_msgs::PowerMsg
  (:init
   (&key
    ((:volts __volts) 0.0)
    ((:min_volt __min_volt) 0.0)
    ((:max_volt __max_volt) 0.0)
    )
   (send-super :init)
   (setq _volts (float __volts))
   (setq _min_volt (float __min_volt))
   (setq _max_volt (float __max_volt))
   self)
  (:volts
   (&optional __volts)
   (if __volts (setq _volts __volts)) _volts)
  (:min_volt
   (&optional __min_volt)
   (if __min_volt (setq _min_volt __min_volt)) _min_volt)
  (:max_volt
   (&optional __max_volt)
   (if __max_volt (setq _max_volt __max_volt)) _max_volt)
  (:serialization-length
   ()
   (+
    ;; float32 _volts
    4
    ;; float32 _min_volt
    4
    ;; float32 _max_volt
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _volts
       (sys::poke _volts (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _min_volt
       (sys::poke _min_volt (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _max_volt
       (sys::poke _max_volt (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _volts
     (setq _volts (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _min_volt
     (setq _min_volt (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _max_volt
     (setq _max_volt (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get corobot_msgs::PowerMsg :md5sum-) "a65f7d50ca34b41b593a2bed414b49e0")
(setf (get corobot_msgs::PowerMsg :datatype-) "corobot_msgs/PowerMsg")
(setf (get corobot_msgs::PowerMsg :definition-)
      "float32 volts
float32 min_volt
float32 max_volt

")



(provide :corobot_msgs/PowerMsg "a65f7d50ca34b41b593a2bed414b49e0")


