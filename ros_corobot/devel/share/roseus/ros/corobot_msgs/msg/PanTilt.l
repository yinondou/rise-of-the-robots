;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::PanTilt)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'PanTilt (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::PANTILT")
  (make-package "COROBOT_MSGS::PANTILT"))

(in-package "ROS")
;;//! \htmlinclude PanTilt.msg.html


(defclass corobot_msgs::PanTilt
  :super ros::object
  :slots (_pan _tilt _zoom _reset ))

(defmethod corobot_msgs::PanTilt
  (:init
   (&key
    ((:pan __pan) 0)
    ((:tilt __tilt) 0)
    ((:zoom __zoom) 0)
    ((:reset __reset) nil)
    )
   (send-super :init)
   (setq _pan (round __pan))
   (setq _tilt (round __tilt))
   (setq _zoom (round __zoom))
   (setq _reset __reset)
   self)
  (:pan
   (&optional __pan)
   (if __pan (setq _pan __pan)) _pan)
  (:tilt
   (&optional __tilt)
   (if __tilt (setq _tilt __tilt)) _tilt)
  (:zoom
   (&optional __zoom)
   (if __zoom (setq _zoom __zoom)) _zoom)
  (:reset
   (&optional __reset)
   (if __reset (setq _reset __reset)) _reset)
  (:serialization-length
   ()
   (+
    ;; int32 _pan
    4
    ;; int32 _tilt
    4
    ;; int32 _zoom
    4
    ;; bool _reset
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _pan
       (write-long _pan s)
     ;; int32 _tilt
       (write-long _tilt s)
     ;; int32 _zoom
       (write-long _zoom s)
     ;; bool _reset
       (if _reset (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _pan
     (setq _pan (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _tilt
     (setq _tilt (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _zoom
     (setq _zoom (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; bool _reset
     (setq _reset (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get corobot_msgs::PanTilt :md5sum-) "f2fe3951598b512c28e9462a0ece165b")
(setf (get corobot_msgs::PanTilt :datatype-) "corobot_msgs/PanTilt")
(setf (get corobot_msgs::PanTilt :definition-)
      "int32 pan
int32 tilt
int32 zoom
bool reset

")



(provide :corobot_msgs/PanTilt "f2fe3951598b512c28e9462a0ece165b")


