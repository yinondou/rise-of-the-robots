;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::ServoType)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'ServoType (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::SERVOTYPE")
  (make-package "COROBOT_MSGS::SERVOTYPE"))

(in-package "ROS")
;;//! \htmlinclude ServoType.msg.html


(defclass corobot_msgs::ServoType
  :super ros::object
  :slots (_index _type ))

(defmethod corobot_msgs::ServoType
  (:init
   (&key
    ((:index __index) 0)
    ((:type __type) 0)
    )
   (send-super :init)
   (setq _index (round __index))
   (setq _type (round __type))
   self)
  (:index
   (&optional __index)
   (if __index (setq _index __index)) _index)
  (:type
   (&optional __type)
   (if __type (setq _type __type)) _type)
  (:serialization-length
   ()
   (+
    ;; int8 _index
    1
    ;; int8 _type
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8 _index
       (write-byte _index s)
     ;; int8 _type
       (write-byte _type s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8 _index
     (setq _index (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _index 127) (setq _index (- _index 256)))
   ;; int8 _type
     (setq _type (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _type 127) (setq _type (- _type 256)))
   ;;
   self)
  )

(setf (get corobot_msgs::ServoType :md5sum-) "1838b6e393c9a582039957a5d550ab79")
(setf (get corobot_msgs::ServoType :datatype-) "corobot_msgs/ServoType")
(setf (get corobot_msgs::ServoType :definition-)
      "#
# Morgan Cormier <mcormier@coroware.com>
#
# Service used to set the type of servo motor connected at the specified index on the phidget servo controller
#
# Index is the index of the servo motor connected to the phidget device. 
# The maximum value of index depends on the Phidget device and how many connections it accepts
#
# type is the type of motor. See enum CPhidget_ServoType of the Phidget library for the list of type.

int8 index

int8 type

")



(provide :corobot_msgs/ServoType "1838b6e393c9a582039957a5d550ab79")


