;; Auto-generated. Do not edit!


(when (boundp 'corobot_msgs::ServoPosition)
  (if (not (find-package "COROBOT_MSGS"))
    (make-package "COROBOT_MSGS"))
  (shadow 'ServoPosition (find-package "COROBOT_MSGS")))
(unless (find-package "COROBOT_MSGS::SERVOPOSITION")
  (make-package "COROBOT_MSGS::SERVOPOSITION"))

(in-package "ROS")
;;//! \htmlinclude ServoPosition.msg.html


(defclass corobot_msgs::ServoPosition
  :super ros::object
  :slots (_index _position ))

(defmethod corobot_msgs::ServoPosition
  (:init
   (&key
    ((:index __index) 0)
    ((:position __position) 0.0)
    )
   (send-super :init)
   (setq _index (round __index))
   (setq _position (float __position))
   self)
  (:index
   (&optional __index)
   (if __index (setq _index __index)) _index)
  (:position
   (&optional __position)
   (if __position (setq _position __position)) _position)
  (:serialization-length
   ()
   (+
    ;; int8 _index
    1
    ;; float32 _position
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8 _index
       (write-byte _index s)
     ;; float32 _position
       (sys::poke _position (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8 _index
     (setq _index (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _index 127) (setq _index (- _index 256)))
   ;; float32 _position
     (setq _position (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get corobot_msgs::ServoPosition :md5sum-) "04e7b072b763b2e3ee692e3faf7e9c94")
(setf (get corobot_msgs::ServoPosition :datatype-) "corobot_msgs/ServoPosition")
(setf (get corobot_msgs::ServoPosition :definition-)
      "#
# Morgan Cormier <mcormier@coroware.com>
#
# Message used to set or get the position of a servo motor connected to a Phidget servo controller.
#
# Index is the index of the servo motor connected to the phidget device. 
# The maximum value of index depends on the Phidget device and how many connections it accepts

int8 index

# position is the angle in degree set for the motor selected with the index value. 
# position has a minimum and maximum value possible, that can be retrieve through two services
float32 position

")



(provide :corobot_msgs/ServoPosition "04e7b072b763b2e3ee692e3faf7e9c94")


