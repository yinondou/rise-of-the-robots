; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude MotorCommand.msg.html

(cl:defclass <MotorCommand> (roslisp-msg-protocol:ros-message)
  ((leftSpeed
    :reader leftSpeed
    :initarg :leftSpeed
    :type cl:fixnum
    :initform 0)
   (rightSpeed
    :reader rightSpeed
    :initarg :rightSpeed
    :type cl:fixnum
    :initform 0)
   (secondsDuration
    :reader secondsDuration
    :initarg :secondsDuration
    :type cl:fixnum
    :initform 0)
   (acceleration
    :reader acceleration
    :initarg :acceleration
    :type cl:fixnum
    :initform 0))
)

(cl:defclass MotorCommand (<MotorCommand>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MotorCommand>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MotorCommand)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<MotorCommand> is deprecated: use corobot_msgs-msg:MotorCommand instead.")))

(cl:ensure-generic-function 'leftSpeed-val :lambda-list '(m))
(cl:defmethod leftSpeed-val ((m <MotorCommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:leftSpeed-val is deprecated.  Use corobot_msgs-msg:leftSpeed instead.")
  (leftSpeed m))

(cl:ensure-generic-function 'rightSpeed-val :lambda-list '(m))
(cl:defmethod rightSpeed-val ((m <MotorCommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:rightSpeed-val is deprecated.  Use corobot_msgs-msg:rightSpeed instead.")
  (rightSpeed m))

(cl:ensure-generic-function 'secondsDuration-val :lambda-list '(m))
(cl:defmethod secondsDuration-val ((m <MotorCommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:secondsDuration-val is deprecated.  Use corobot_msgs-msg:secondsDuration instead.")
  (secondsDuration m))

(cl:ensure-generic-function 'acceleration-val :lambda-list '(m))
(cl:defmethod acceleration-val ((m <MotorCommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:acceleration-val is deprecated.  Use corobot_msgs-msg:acceleration instead.")
  (acceleration m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MotorCommand>) ostream)
  "Serializes a message object of type '<MotorCommand>"
  (cl:let* ((signed (cl:slot-value msg 'leftSpeed)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'rightSpeed)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'secondsDuration)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'secondsDuration)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'acceleration)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'acceleration)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MotorCommand>) istream)
  "Deserializes a message object of type '<MotorCommand>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'leftSpeed) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'rightSpeed) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'secondsDuration)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'secondsDuration)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'acceleration)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'acceleration)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MotorCommand>)))
  "Returns string type for a message object of type '<MotorCommand>"
  "corobot_msgs/MotorCommand")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MotorCommand)))
  "Returns string type for a message object of type 'MotorCommand"
  "corobot_msgs/MotorCommand")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MotorCommand>)))
  "Returns md5sum for a message object of type '<MotorCommand>"
  "451c087bf7cd0b5ee063409877e152e9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MotorCommand)))
  "Returns md5sum for a message object of type 'MotorCommand"
  "451c087bf7cd0b5ee063409877e152e9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MotorCommand>)))
  "Returns full string definition for message of type '<MotorCommand>"
  (cl:format cl:nil "# Bill Mania <bmania@coroware.com>~%#~%# Service used with the CoroWare CoroBot to move the robot~%# base. Originally developed for the Phidget HC Motor Controller~%#~%# leftSpeed and rightSpeed are signed integers which specify the~%# combined speed and direction to rotate the wheels on each side~%# of the robot. 0 indicates full stop. positive integers indicate~%# a forward rotation and larger integers indicate a higher~%# rotational speed.~%#~%int16 leftSpeed~%int16 rightSpeed~%#~%# when at least one of leftSpeed or rightSpeed is non-zero,~%# secondsDuration indicates for how many seconds to rotate those~%# wheels at the requested speed. when secondsDuration is zero,~%# the wheels will be rotated at the requested speed, in the~%# requested direction until another request is received.~%#~%uint16 secondsDuration~%#~%# the number of seconds over which to effect the change in~%# rotational speed from the current speed to the speed requested.~%# a value of zero indicates \"as fast as possible\". the greater~%# this value, the greater amount of time taken to change the~%# speed.~%uint16 acceleration~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MotorCommand)))
  "Returns full string definition for message of type 'MotorCommand"
  (cl:format cl:nil "# Bill Mania <bmania@coroware.com>~%#~%# Service used with the CoroWare CoroBot to move the robot~%# base. Originally developed for the Phidget HC Motor Controller~%#~%# leftSpeed and rightSpeed are signed integers which specify the~%# combined speed and direction to rotate the wheels on each side~%# of the robot. 0 indicates full stop. positive integers indicate~%# a forward rotation and larger integers indicate a higher~%# rotational speed.~%#~%int16 leftSpeed~%int16 rightSpeed~%#~%# when at least one of leftSpeed or rightSpeed is non-zero,~%# secondsDuration indicates for how many seconds to rotate those~%# wheels at the requested speed. when secondsDuration is zero,~%# the wheels will be rotated at the requested speed, in the~%# requested direction until another request is received.~%#~%uint16 secondsDuration~%#~%# the number of seconds over which to effect the change in~%# rotational speed from the current speed to the speed requested.~%# a value of zero indicates \"as fast as possible\". the greater~%# this value, the greater amount of time taken to change the~%# speed.~%uint16 acceleration~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MotorCommand>))
  (cl:+ 0
     2
     2
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MotorCommand>))
  "Converts a ROS message object to a list"
  (cl:list 'MotorCommand
    (cl:cons ':leftSpeed (leftSpeed msg))
    (cl:cons ':rightSpeed (rightSpeed msg))
    (cl:cons ':secondsDuration (secondsDuration msg))
    (cl:cons ':acceleration (acceleration msg))
))
