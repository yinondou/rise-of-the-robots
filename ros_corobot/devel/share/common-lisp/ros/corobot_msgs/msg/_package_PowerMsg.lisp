(cl:in-package corobot_msgs-msg)
(cl:export '(VOLTS-VAL
          VOLTS
          MIN_VOLT-VAL
          MIN_VOLT
          MAX_VOLT-VAL
          MAX_VOLT
))