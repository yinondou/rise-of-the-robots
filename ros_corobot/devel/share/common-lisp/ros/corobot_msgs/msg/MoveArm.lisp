; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude MoveArm.msg.html

(cl:defclass <MoveArm> (roslisp-msg-protocol:ros-message)
  ((index
    :reader index
    :initarg :index
    :type cl:fixnum
    :initform 0)
   (position
    :reader position
    :initarg :position
    :type cl:float
    :initform 0.0))
)

(cl:defclass MoveArm (<MoveArm>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MoveArm>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MoveArm)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<MoveArm> is deprecated: use corobot_msgs-msg:MoveArm instead.")))

(cl:ensure-generic-function 'index-val :lambda-list '(m))
(cl:defmethod index-val ((m <MoveArm>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:index-val is deprecated.  Use corobot_msgs-msg:index instead.")
  (index m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <MoveArm>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:position-val is deprecated.  Use corobot_msgs-msg:position instead.")
  (position m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<MoveArm>)))
    "Constants for message type '<MoveArm>"
  '((:BASE_ROTATION . 0)
    (:SHOULDER . 1)
    (:ELBOW . 2)
    (:WRIST_FLEX . 3)
    (:WRIST_ROTATION . 3)
    (:GRIPPER . 4)
    (:LEFT_WHEEL . 5)
    (:RIGHT_WHEEL . 6))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'MoveArm)))
    "Constants for message type 'MoveArm"
  '((:BASE_ROTATION . 0)
    (:SHOULDER . 1)
    (:ELBOW . 2)
    (:WRIST_FLEX . 3)
    (:WRIST_ROTATION . 3)
    (:GRIPPER . 4)
    (:LEFT_WHEEL . 5)
    (:RIGHT_WHEEL . 6))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MoveArm>) ostream)
  "Serializes a message object of type '<MoveArm>"
  (cl:let* ((signed (cl:slot-value msg 'index)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'position))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MoveArm>) istream)
  "Deserializes a message object of type '<MoveArm>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'index) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'position) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MoveArm>)))
  "Returns string type for a message object of type '<MoveArm>"
  "corobot_msgs/MoveArm")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MoveArm)))
  "Returns string type for a message object of type 'MoveArm"
  "corobot_msgs/MoveArm")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MoveArm>)))
  "Returns md5sum for a message object of type '<MoveArm>"
  "0880c120739bb732d35453e85c86d543")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MoveArm)))
  "Returns md5sum for a message object of type 'MoveArm"
  "0880c120739bb732d35453e85c86d543")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MoveArm>)))
  "Returns full string definition for message of type '<MoveArm>"
  (cl:format cl:nil "#~%# Morgan Cormier <mcormier@coroware.com>~%#~%# Message used to set or get the position of a servo motor connected to a Phidget servo controller.~%#~%~%####### Possible Indexes #########~%uint8 BASE_ROTATION = 0~%uint8 SHOULDER = 1~%uint8 ELBOW = 2~%uint8 WRIST_FLEX = 3~%uint8 WRIST_ROTATION = 3~%uint8 GRIPPER = 4~%uint8 LEFT_WHEEL = 5 # only used in case the left wheel is a stepper motor~%uint8 RIGHT_WHEEL = 6 # only used in case the right wheel is a stepper motor~%# Index is the index from the list of possible index. ~%int8 index~%~%# position is the angle in degree set for the servo motor selected with index~%float32 position~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MoveArm)))
  "Returns full string definition for message of type 'MoveArm"
  (cl:format cl:nil "#~%# Morgan Cormier <mcormier@coroware.com>~%#~%# Message used to set or get the position of a servo motor connected to a Phidget servo controller.~%#~%~%####### Possible Indexes #########~%uint8 BASE_ROTATION = 0~%uint8 SHOULDER = 1~%uint8 ELBOW = 2~%uint8 WRIST_FLEX = 3~%uint8 WRIST_ROTATION = 3~%uint8 GRIPPER = 4~%uint8 LEFT_WHEEL = 5 # only used in case the left wheel is a stepper motor~%uint8 RIGHT_WHEEL = 6 # only used in case the right wheel is a stepper motor~%# Index is the index from the list of possible index. ~%int8 index~%~%# position is the angle in degree set for the servo motor selected with index~%float32 position~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MoveArm>))
  (cl:+ 0
     1
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MoveArm>))
  "Converts a ROS message object to a list"
  (cl:list 'MoveArm
    (cl:cons ':index (index msg))
    (cl:cons ':position (position msg))
))
