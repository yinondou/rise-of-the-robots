; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude videomode.msg.html

(cl:defclass <videomode> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (immediately
    :reader immediately
    :initarg :immediately
    :type cl:boolean
    :initform cl:nil)
   (width
    :reader width
    :initarg :width
    :type cl:fixnum
    :initform 0)
   (height
    :reader height
    :initarg :height
    :type cl:fixnum
    :initform 0)
   (fps
    :reader fps
    :initarg :fps
    :type cl:fixnum
    :initform 0)
   (auto_exposure
    :reader auto_exposure
    :initarg :auto_exposure
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass videomode (<videomode>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <videomode>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'videomode)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<videomode> is deprecated: use corobot_msgs-msg:videomode instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <videomode>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:header-val is deprecated.  Use corobot_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'immediately-val :lambda-list '(m))
(cl:defmethod immediately-val ((m <videomode>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:immediately-val is deprecated.  Use corobot_msgs-msg:immediately instead.")
  (immediately m))

(cl:ensure-generic-function 'width-val :lambda-list '(m))
(cl:defmethod width-val ((m <videomode>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:width-val is deprecated.  Use corobot_msgs-msg:width instead.")
  (width m))

(cl:ensure-generic-function 'height-val :lambda-list '(m))
(cl:defmethod height-val ((m <videomode>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:height-val is deprecated.  Use corobot_msgs-msg:height instead.")
  (height m))

(cl:ensure-generic-function 'fps-val :lambda-list '(m))
(cl:defmethod fps-val ((m <videomode>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:fps-val is deprecated.  Use corobot_msgs-msg:fps instead.")
  (fps m))

(cl:ensure-generic-function 'auto_exposure-val :lambda-list '(m))
(cl:defmethod auto_exposure-val ((m <videomode>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:auto_exposure-val is deprecated.  Use corobot_msgs-msg:auto_exposure instead.")
  (auto_exposure m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <videomode>) ostream)
  "Serializes a message object of type '<videomode>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'immediately) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'width)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'width)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'height)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'height)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'fps)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'auto_exposure) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <videomode>) istream)
  "Deserializes a message object of type '<videomode>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'immediately) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'width)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'width)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'height)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'height)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'fps)) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'auto_exposure) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<videomode>)))
  "Returns string type for a message object of type '<videomode>"
  "corobot_msgs/videomode")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'videomode)))
  "Returns string type for a message object of type 'videomode"
  "corobot_msgs/videomode")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<videomode>)))
  "Returns md5sum for a message object of type '<videomode>"
  "5bf6aeb9e3ba1dd691c5c50529d7e824")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'videomode)))
  "Returns md5sum for a message object of type 'videomode"
  "5bf6aeb9e3ba1dd691c5c50529d7e824")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<videomode>)))
  "Returns full string definition for message of type '<videomode>"
  (cl:format cl:nil "Header  header~%bool    immediately~%uint16  width~%uint16  height~%uint8   fps~%bool    auto_exposure~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'videomode)))
  "Returns full string definition for message of type 'videomode"
  (cl:format cl:nil "Header  header~%bool    immediately~%uint16  width~%uint16  height~%uint8   fps~%bool    auto_exposure~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <videomode>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     2
     2
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <videomode>))
  "Converts a ROS message object to a list"
  (cl:list 'videomode
    (cl:cons ':header (header msg))
    (cl:cons ':immediately (immediately msg))
    (cl:cons ':width (width msg))
    (cl:cons ':height (height msg))
    (cl:cons ':fps (fps msg))
    (cl:cons ':auto_exposure (auto_exposure msg))
))
