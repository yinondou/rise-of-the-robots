; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude SensorMsg.msg.html

(cl:defclass <SensorMsg> (roslisp-msg-protocol:ros-message)
  ((type
    :reader type
    :initarg :type
    :type cl:fixnum
    :initform 0)
   (index
    :reader index
    :initarg :index
    :type cl:fixnum
    :initform 0)
   (value
    :reader value
    :initarg :value
    :type cl:float
    :initform 0.0))
)

(cl:defclass SensorMsg (<SensorMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SensorMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SensorMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<SensorMsg> is deprecated: use corobot_msgs-msg:SensorMsg instead.")))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <SensorMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:type-val is deprecated.  Use corobot_msgs-msg:type instead.")
  (type m))

(cl:ensure-generic-function 'index-val :lambda-list '(m))
(cl:defmethod index-val ((m <SensorMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:index-val is deprecated.  Use corobot_msgs-msg:index instead.")
  (index m))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <SensorMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:value-val is deprecated.  Use corobot_msgs-msg:value instead.")
  (value m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<SensorMsg>)))
    "Constants for message type '<SensorMsg>"
  '((:BUMPER . 0)
    (:OTHER . 1)
    (:ULTRASOUND . 2)
    (:INFRARED_FRONT . 3)
    (:INFRARED_REAR . 4))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'SensorMsg)))
    "Constants for message type 'SensorMsg"
  '((:BUMPER . 0)
    (:OTHER . 1)
    (:ULTRASOUND . 2)
    (:INFRARED_FRONT . 3)
    (:INFRARED_REAR . 4))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SensorMsg>) ostream)
  "Serializes a message object of type '<SensorMsg>"
  (cl:let* ((signed (cl:slot-value msg 'type)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'index)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'value))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SensorMsg>) istream)
  "Deserializes a message object of type '<SensorMsg>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'type) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'index) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'value) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SensorMsg>)))
  "Returns string type for a message object of type '<SensorMsg>"
  "corobot_msgs/SensorMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SensorMsg)))
  "Returns string type for a message object of type 'SensorMsg"
  "corobot_msgs/SensorMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SensorMsg>)))
  "Returns md5sum for a message object of type '<SensorMsg>"
  "f2f2ad707f7136c4cb90a14479ae8687")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SensorMsg)))
  "Returns md5sum for a message object of type 'SensorMsg"
  "f2f2ad707f7136c4cb90a14479ae8687")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SensorMsg>)))
  "Returns full string definition for message of type '<SensorMsg>"
  (cl:format cl:nil "# This message can be used to send some simple sensor data that require only one value. The type of the sensor can be unkown.~%~%#define sensor type~%int8 BUMPER = 0~%int8 OTHER = 1~%uint8 ULTRASOUND=2~%uint8 INFRARED_FRONT=3~%uint8 INFRARED_REAR=4~%~%# type of sensor, defined above~%int8 type~%~%# index of the sensor, for example index on the Phidget Interface Kit. This can be used to filter data when the sensor type is unknown, but the user know the data type~%int8 index~%~%# Value of the sensor, it can be digital in which case the value will be 0.0 or 1.0, or analog in Volts. If the type of the sensor is known, for example ultrasound or infrared, the value is in the corresponding standard format, for example meters. ~%float64 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SensorMsg)))
  "Returns full string definition for message of type 'SensorMsg"
  (cl:format cl:nil "# This message can be used to send some simple sensor data that require only one value. The type of the sensor can be unkown.~%~%#define sensor type~%int8 BUMPER = 0~%int8 OTHER = 1~%uint8 ULTRASOUND=2~%uint8 INFRARED_FRONT=3~%uint8 INFRARED_REAR=4~%~%# type of sensor, defined above~%int8 type~%~%# index of the sensor, for example index on the Phidget Interface Kit. This can be used to filter data when the sensor type is unknown, but the user know the data type~%int8 index~%~%# Value of the sensor, it can be digital in which case the value will be 0.0 or 1.0, or analog in Volts. If the type of the sensor is known, for example ultrasound or infrared, the value is in the corresponding standard format, for example meters. ~%float64 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SensorMsg>))
  (cl:+ 0
     1
     1
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SensorMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'SensorMsg
    (cl:cons ':type (type msg))
    (cl:cons ':index (index msg))
    (cl:cons ':value (value msg))
))
