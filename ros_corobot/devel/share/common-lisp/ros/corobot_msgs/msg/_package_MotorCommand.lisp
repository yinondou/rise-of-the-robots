(cl:in-package corobot_msgs-msg)
(cl:export '(LEFTSPEED-VAL
          LEFTSPEED
          RIGHTSPEED-VAL
          RIGHTSPEED
          SECONDSDURATION-VAL
          SECONDSDURATION
          ACCELERATION-VAL
          ACCELERATION
))