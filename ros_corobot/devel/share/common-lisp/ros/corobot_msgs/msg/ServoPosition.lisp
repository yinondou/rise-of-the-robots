; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude ServoPosition.msg.html

(cl:defclass <ServoPosition> (roslisp-msg-protocol:ros-message)
  ((index
    :reader index
    :initarg :index
    :type cl:fixnum
    :initform 0)
   (position
    :reader position
    :initarg :position
    :type cl:float
    :initform 0.0))
)

(cl:defclass ServoPosition (<ServoPosition>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ServoPosition>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ServoPosition)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<ServoPosition> is deprecated: use corobot_msgs-msg:ServoPosition instead.")))

(cl:ensure-generic-function 'index-val :lambda-list '(m))
(cl:defmethod index-val ((m <ServoPosition>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:index-val is deprecated.  Use corobot_msgs-msg:index instead.")
  (index m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <ServoPosition>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:position-val is deprecated.  Use corobot_msgs-msg:position instead.")
  (position m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ServoPosition>) ostream)
  "Serializes a message object of type '<ServoPosition>"
  (cl:let* ((signed (cl:slot-value msg 'index)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'position))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ServoPosition>) istream)
  "Deserializes a message object of type '<ServoPosition>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'index) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'position) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ServoPosition>)))
  "Returns string type for a message object of type '<ServoPosition>"
  "corobot_msgs/ServoPosition")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ServoPosition)))
  "Returns string type for a message object of type 'ServoPosition"
  "corobot_msgs/ServoPosition")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ServoPosition>)))
  "Returns md5sum for a message object of type '<ServoPosition>"
  "04e7b072b763b2e3ee692e3faf7e9c94")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ServoPosition)))
  "Returns md5sum for a message object of type 'ServoPosition"
  "04e7b072b763b2e3ee692e3faf7e9c94")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ServoPosition>)))
  "Returns full string definition for message of type '<ServoPosition>"
  (cl:format cl:nil "#~%# Morgan Cormier <mcormier@coroware.com>~%#~%# Message used to set or get the position of a servo motor connected to a Phidget servo controller.~%#~%# Index is the index of the servo motor connected to the phidget device. ~%# The maximum value of index depends on the Phidget device and how many connections it accepts~%~%int8 index~%~%# position is the angle in degree set for the motor selected with the index value. ~%# position has a minimum and maximum value possible, that can be retrieve through two services~%float32 position~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ServoPosition)))
  "Returns full string definition for message of type 'ServoPosition"
  (cl:format cl:nil "#~%# Morgan Cormier <mcormier@coroware.com>~%#~%# Message used to set or get the position of a servo motor connected to a Phidget servo controller.~%#~%# Index is the index of the servo motor connected to the phidget device. ~%# The maximum value of index depends on the Phidget device and how many connections it accepts~%~%int8 index~%~%# position is the angle in degree set for the motor selected with the index value. ~%# position has a minimum and maximum value possible, that can be retrieve through two services~%float32 position~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ServoPosition>))
  (cl:+ 0
     1
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ServoPosition>))
  "Converts a ROS message object to a list"
  (cl:list 'ServoPosition
    (cl:cons ':index (index msg))
    (cl:cons ':position (position msg))
))
