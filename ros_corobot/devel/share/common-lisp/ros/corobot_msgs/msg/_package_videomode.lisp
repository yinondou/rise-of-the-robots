(cl:in-package corobot_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          IMMEDIATELY-VAL
          IMMEDIATELY
          WIDTH-VAL
          WIDTH
          HEIGHT-VAL
          HEIGHT
          FPS-VAL
          FPS
          AUTO_EXPOSURE-VAL
          AUTO_EXPOSURE
))