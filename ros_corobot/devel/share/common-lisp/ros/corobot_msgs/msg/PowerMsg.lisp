; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude PowerMsg.msg.html

(cl:defclass <PowerMsg> (roslisp-msg-protocol:ros-message)
  ((volts
    :reader volts
    :initarg :volts
    :type cl:float
    :initform 0.0)
   (min_volt
    :reader min_volt
    :initarg :min_volt
    :type cl:float
    :initform 0.0)
   (max_volt
    :reader max_volt
    :initarg :max_volt
    :type cl:float
    :initform 0.0))
)

(cl:defclass PowerMsg (<PowerMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PowerMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PowerMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<PowerMsg> is deprecated: use corobot_msgs-msg:PowerMsg instead.")))

(cl:ensure-generic-function 'volts-val :lambda-list '(m))
(cl:defmethod volts-val ((m <PowerMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:volts-val is deprecated.  Use corobot_msgs-msg:volts instead.")
  (volts m))

(cl:ensure-generic-function 'min_volt-val :lambda-list '(m))
(cl:defmethod min_volt-val ((m <PowerMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:min_volt-val is deprecated.  Use corobot_msgs-msg:min_volt instead.")
  (min_volt m))

(cl:ensure-generic-function 'max_volt-val :lambda-list '(m))
(cl:defmethod max_volt-val ((m <PowerMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:max_volt-val is deprecated.  Use corobot_msgs-msg:max_volt instead.")
  (max_volt m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PowerMsg>) ostream)
  "Serializes a message object of type '<PowerMsg>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'volts))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'min_volt))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'max_volt))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PowerMsg>) istream)
  "Deserializes a message object of type '<PowerMsg>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'volts) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'min_volt) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'max_volt) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PowerMsg>)))
  "Returns string type for a message object of type '<PowerMsg>"
  "corobot_msgs/PowerMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PowerMsg)))
  "Returns string type for a message object of type 'PowerMsg"
  "corobot_msgs/PowerMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PowerMsg>)))
  "Returns md5sum for a message object of type '<PowerMsg>"
  "a65f7d50ca34b41b593a2bed414b49e0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PowerMsg)))
  "Returns md5sum for a message object of type 'PowerMsg"
  "a65f7d50ca34b41b593a2bed414b49e0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PowerMsg>)))
  "Returns full string definition for message of type '<PowerMsg>"
  (cl:format cl:nil "float32 volts~%float32 min_volt~%float32 max_volt~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PowerMsg)))
  "Returns full string definition for message of type 'PowerMsg"
  (cl:format cl:nil "float32 volts~%float32 min_volt~%float32 max_volt~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PowerMsg>))
  (cl:+ 0
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PowerMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'PowerMsg
    (cl:cons ':volts (volts msg))
    (cl:cons ':min_volt (min_volt msg))
    (cl:cons ':max_volt (max_volt msg))
))
