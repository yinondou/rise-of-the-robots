; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude ServoType.msg.html

(cl:defclass <ServoType> (roslisp-msg-protocol:ros-message)
  ((index
    :reader index
    :initarg :index
    :type cl:fixnum
    :initform 0)
   (type
    :reader type
    :initarg :type
    :type cl:fixnum
    :initform 0))
)

(cl:defclass ServoType (<ServoType>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ServoType>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ServoType)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<ServoType> is deprecated: use corobot_msgs-msg:ServoType instead.")))

(cl:ensure-generic-function 'index-val :lambda-list '(m))
(cl:defmethod index-val ((m <ServoType>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:index-val is deprecated.  Use corobot_msgs-msg:index instead.")
  (index m))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <ServoType>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:type-val is deprecated.  Use corobot_msgs-msg:type instead.")
  (type m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ServoType>) ostream)
  "Serializes a message object of type '<ServoType>"
  (cl:let* ((signed (cl:slot-value msg 'index)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'type)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ServoType>) istream)
  "Deserializes a message object of type '<ServoType>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'index) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'type) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ServoType>)))
  "Returns string type for a message object of type '<ServoType>"
  "corobot_msgs/ServoType")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ServoType)))
  "Returns string type for a message object of type 'ServoType"
  "corobot_msgs/ServoType")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ServoType>)))
  "Returns md5sum for a message object of type '<ServoType>"
  "1838b6e393c9a582039957a5d550ab79")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ServoType)))
  "Returns md5sum for a message object of type 'ServoType"
  "1838b6e393c9a582039957a5d550ab79")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ServoType>)))
  "Returns full string definition for message of type '<ServoType>"
  (cl:format cl:nil "#~%# Morgan Cormier <mcormier@coroware.com>~%#~%# Service used to set the type of servo motor connected at the specified index on the phidget servo controller~%#~%# Index is the index of the servo motor connected to the phidget device. ~%# The maximum value of index depends on the Phidget device and how many connections it accepts~%#~%# type is the type of motor. See enum CPhidget_ServoType of the Phidget library for the list of type.~%~%int8 index~%~%int8 type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ServoType)))
  "Returns full string definition for message of type 'ServoType"
  (cl:format cl:nil "#~%# Morgan Cormier <mcormier@coroware.com>~%#~%# Service used to set the type of servo motor connected at the specified index on the phidget servo controller~%#~%# Index is the index of the servo motor connected to the phidget device. ~%# The maximum value of index depends on the Phidget device and how many connections it accepts~%#~%# type is the type of motor. See enum CPhidget_ServoType of the Phidget library for the list of type.~%~%int8 index~%~%int8 type~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ServoType>))
  (cl:+ 0
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ServoType>))
  "Converts a ROS message object to a list"
  (cl:list 'ServoType
    (cl:cons ':index (index msg))
    (cl:cons ':type (type msg))
))
