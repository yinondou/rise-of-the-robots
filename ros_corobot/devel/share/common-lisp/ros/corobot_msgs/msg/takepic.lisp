; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude takepic.msg.html

(cl:defclass <takepic> (roslisp-msg-protocol:ros-message)
  ((camera_index
    :reader camera_index
    :initarg :camera_index
    :type cl:integer
    :initform 0)
   (note
    :reader note
    :initarg :note
    :type cl:integer
    :initform 0)
   (take
    :reader take
    :initarg :take
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass takepic (<takepic>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <takepic>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'takepic)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<takepic> is deprecated: use corobot_msgs-msg:takepic instead.")))

(cl:ensure-generic-function 'camera_index-val :lambda-list '(m))
(cl:defmethod camera_index-val ((m <takepic>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:camera_index-val is deprecated.  Use corobot_msgs-msg:camera_index instead.")
  (camera_index m))

(cl:ensure-generic-function 'note-val :lambda-list '(m))
(cl:defmethod note-val ((m <takepic>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:note-val is deprecated.  Use corobot_msgs-msg:note instead.")
  (note m))

(cl:ensure-generic-function 'take-val :lambda-list '(m))
(cl:defmethod take-val ((m <takepic>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:take-val is deprecated.  Use corobot_msgs-msg:take instead.")
  (take m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <takepic>) ostream)
  "Serializes a message object of type '<takepic>"
  (cl:let* ((signed (cl:slot-value msg 'camera_index)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'note)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'take) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <takepic>) istream)
  "Deserializes a message object of type '<takepic>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'camera_index) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'note) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:setf (cl:slot-value msg 'take) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<takepic>)))
  "Returns string type for a message object of type '<takepic>"
  "corobot_msgs/takepic")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'takepic)))
  "Returns string type for a message object of type 'takepic"
  "corobot_msgs/takepic")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<takepic>)))
  "Returns md5sum for a message object of type '<takepic>"
  "802817317afdd58e6e9fd4be92bf0d57")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'takepic)))
  "Returns md5sum for a message object of type 'takepic"
  "802817317afdd58e6e9fd4be92bf0d57")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<takepic>)))
  "Returns full string definition for message of type '<takepic>"
  (cl:format cl:nil "int32 camera_index~%int32 note~%bool take~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'takepic)))
  "Returns full string definition for message of type 'takepic"
  (cl:format cl:nil "int32 camera_index~%int32 note~%bool take~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <takepic>))
  (cl:+ 0
     4
     4
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <takepic>))
  "Converts a ROS message object to a list"
  (cl:list 'takepic
    (cl:cons ':camera_index (camera_index msg))
    (cl:cons ':note (note msg))
    (cl:cons ':take (take msg))
))
