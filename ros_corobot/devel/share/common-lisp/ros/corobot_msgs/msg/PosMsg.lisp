; Auto-generated. Do not edit!


(cl:in-package corobot_msgs-msg)


;//! \htmlinclude PosMsg.msg.html

(cl:defclass <PosMsg> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (px
    :reader px
    :initarg :px
    :type cl:integer
    :initform 0)
   (py
    :reader py
    :initarg :py
    :type cl:integer
    :initform 0))
)

(cl:defclass PosMsg (<PosMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PosMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PosMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name corobot_msgs-msg:<PosMsg> is deprecated: use corobot_msgs-msg:PosMsg instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PosMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:header-val is deprecated.  Use corobot_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'px-val :lambda-list '(m))
(cl:defmethod px-val ((m <PosMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:px-val is deprecated.  Use corobot_msgs-msg:px instead.")
  (px m))

(cl:ensure-generic-function 'py-val :lambda-list '(m))
(cl:defmethod py-val ((m <PosMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader corobot_msgs-msg:py-val is deprecated.  Use corobot_msgs-msg:py instead.")
  (py m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PosMsg>) ostream)
  "Serializes a message object of type '<PosMsg>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'px)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'py)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PosMsg>) istream)
  "Deserializes a message object of type '<PosMsg>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'px) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'py) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PosMsg>)))
  "Returns string type for a message object of type '<PosMsg>"
  "corobot_msgs/PosMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PosMsg)))
  "Returns string type for a message object of type 'PosMsg"
  "corobot_msgs/PosMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PosMsg>)))
  "Returns md5sum for a message object of type '<PosMsg>"
  "d4e99badba42a86feac27c2c6f7119ef")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PosMsg)))
  "Returns md5sum for a message object of type 'PosMsg"
  "d4e99badba42a86feac27c2c6f7119ef")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PosMsg>)))
  "Returns full string definition for message of type '<PosMsg>"
  (cl:format cl:nil "Header header~%int32 px~%int32 py~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PosMsg)))
  "Returns full string definition for message of type 'PosMsg"
  (cl:format cl:nil "Header header~%int32 px~%int32 py~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PosMsg>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PosMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'PosMsg
    (cl:cons ':header (header msg))
    (cl:cons ':px (px msg))
    (cl:cons ':py (py msg))
))
