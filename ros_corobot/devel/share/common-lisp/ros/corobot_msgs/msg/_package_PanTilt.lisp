(cl:in-package corobot_msgs-msg)
(cl:export '(PAN-VAL
          PAN
          TILT-VAL
          TILT
          ZOOM-VAL
          ZOOM
          RESET-VAL
          RESET
))