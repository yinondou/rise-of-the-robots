(cl:in-package corobot_gazebo-msg)
(cl:export '(NAME-VAL
          NAME
          POSE-VAL
          POSE
          TWIST-VAL
          TWIST
))