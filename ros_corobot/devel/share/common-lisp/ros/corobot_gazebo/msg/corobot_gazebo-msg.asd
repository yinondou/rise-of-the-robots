
(cl:in-package :asdf)

(defsystem "corobot_gazebo-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
)
  :components ((:file "_package")
    (:file "ModelStates" :depends-on ("_package_ModelStates"))
    (:file "_package_ModelStates" :depends-on ("_package"))
  ))